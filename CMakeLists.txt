# * ********************************************************************************************************* *
# *
# * Copyright 2019 NXP
# * Copyright 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

cmake_minimum_required(VERSION 3.10 FATAL_ERROR)

if(WIN32)
    set(APPLICATION_COMPILE_DEFS WIN32_LEAN_AND_MEAN)
    if(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
        set(APPLICATION_COMPILE_OPTS /MP)
    endif()
    set(APPLICATION_LIBS gdiplus)
elseif (UNIX AND NOT APPLE)
    set(APPLICATION_COMPILE_OPTS -Wall -Wextra)
    set(APPLICATION_LIBS -static-libgcc -static-libstdc++)
elseif (APPLE)
    find_library(_framework_cocoa Cocoa)
    set(APPLICATION_COMPILE_OPTS -Wno-dollar-in-identifier-extension -Wall -Wextra -x objective-c++)
    set(APPLICATION_LIBS ${_framework_cocoa})
endif ()

macro(APPLICATION_MAIN_CALLBACK target)
    target_include_directories(${target} PUBLIC ${APPLICATION_INCLUDES})

    target_compile_features(${target} PUBLIC cxx_std_17)
    target_compile_definitions(${target} PRIVATE ${APPLICATION_COMPILE_DEFS})
    target_compile_options(${target} PUBLIC ${APPLICATION_COMPILE_OPTS})

    target_link_libraries(${target} PUBLIC ${APPLICATION_LIBS})

    # reduce optimisation level because after magic sequence detection issue it somehow forget static fields for indexing in EAP build
    if (APPLE)
        set(CMAKE_CXX_FLAGS_RELEASE "-O1 -fdata-sections -ffunction-sections -DNDEBUG -Wsign-compare -Wparentheses ")
    endif()
endmacro()

macro(APPLICATION_TEST_CALLBACK target testTarget)
    if (UNIX AND NOT APPLE)
        target_link_libraries(${testTarget} PUBLIC pthread)
    endif()
endmacro()

macro(APPLICATION_OIDIS_DEPENDENCIES_CALLBACK oidisDep)
endmacro()

add_subdirectory(bin/resource/scripts)

option(INTEGRATION_TESTS OFF)
if (INTEGRATION_TESTS)
    message(STATUS "Adding integration tests into the build")

    add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/test/integration)
endif ()
