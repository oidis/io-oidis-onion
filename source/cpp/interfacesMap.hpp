/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef IO_OIDIS_ONION_FILESYSTEM_GUI_ENVIRONMENT_INTERFACESMAP_HPP_  // NOLINT
#define IO_OIDIS_ONION_FILESYSTEM_GUI_ENVIRONMENT_INTERFACESMAP_HPP_

namespace Io {
    namespace Oidis {
        namespace Onion {
            namespace Filesystem {
            }
            namespace Gui {
                namespace Environment {
                    class Handler;
                }
                namespace Image {
                    class ImageBase;
                    class ImageLinux;
                    class ImageMac;
                    class ImageWin;
                }
                namespace Label {
                    class LabelBase;
                    class LabelLinux;
                    class LabelMac;
                    class LabelWin;
                }
                namespace NotifyIcon {
                    class NotifyBalloonIcon;
                    class NotifyIconBase;
                    class NotifyIconContextMenuItem;
                    class NotifyIconLinux;
                    class NotifyIconMac;
                    class NotifyIconOptions;
                    class NotifyIconWin;
                }
                namespace Primitives {
                    class BaseGuiObject;
                }
                namespace ProgressBar {
                    class ProgressBarBase;
                    class ProgressBarLinux;
                    class ProgressBarMac;
                    class ProgressBarWin;
                }
                namespace SplashScreen {
                    class SplashScreenBase;
                    class SplashScreenCmd;
                    class SplashScreenLinux;
                    class SplashScreenMac;
                    class SplashScreenWin;
                }
                namespace Structures {
                    class ColorBase;
                    class ColorLinux;
                    class ColorMac;
                    class ColorWin;
                    class Font;
                }
                namespace TaskBar {
                    class TaskBarBase;
                    class TaskBarMac;
                    class TaskBarWin;
                }
            }
            namespace ResourceManager {
                class ResourceManager;
                namespace Core {
                    class Resource;
                }
                namespace Logger {
                    class Logger;
                    class LoggerConsole;
                    class LoggerDevNull;
                    class LogManager;
                }
                namespace Reader {
                    class ReaderSettings;
                    class ResourceReader;
                }
                namespace Utils {
                    class BinaryReader;
                    class FileReader;
                    class FileWriter;
                }
                namespace Writer {
                    class ResourceWriter;
                    class WriterSettings;
                }
            }
        }
    }
}

#endif  // IO_OIDIS_ONION_FILESYSTEM_GUI_ENVIRONMENT_INTERFACESMAP_HPP_  // NOLINT
