/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../reference.hpp"

namespace Io::Oidis::Onion::Filesystem {

    bool Exists(const std::string &$path) {
        const auto replaceAll = [](const std::string &$str, const std::string &$from, const std::string &$to) -> std::string {
            std::string tmp = $str;

            size_t start_pos = 0;
            while ((start_pos = tmp.find($from, start_pos)) != std::string::npos) {
                tmp.replace(start_pos, $from.length(), $to);
                start_pos += $to.length();
            }
            return tmp;
        };

        std::ifstream in;
        std::string p = replaceAll($path, "\\", "/");
        in.open(p);
        return in.is_open();
    }
}
