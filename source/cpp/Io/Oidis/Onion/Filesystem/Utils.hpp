/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_FILESYSTEM_UTILS_HPP_
#define IO_OIDIS_ONION_FILESYSTEM_UTILS_HPP_

#include <string>
#include <fstream>

namespace Io::Oidis::Onion::Filesystem {
    /**
     * Checks whether $path file exists.
     * Replacement for boost or std filesystem because of issues on Mac.
     * @param $path Path to check.
     * @return Return true if exists.
     */
    bool Exists(const std::string &$path);
}

#endif  // IO_OIDIS_ONION_FILESYSTEM_UTILS_HPP_
