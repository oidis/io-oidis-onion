/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_UTILS_FILEUTILS_HPP_
#define IO_OIDIS_ONION_RESOURCEMANAGER_UTILS_FILEUTILS_HPP_

#include <string>

namespace Io::Oidis::Onion::ResourceManager::Utils::FileUtils {
    void truncateFile(const std::string &$path, size_t $size);
}

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_UTILS_FILEUTILS_HPP_
