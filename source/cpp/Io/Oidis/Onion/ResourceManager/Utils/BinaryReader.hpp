/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_UTILS_BINARYREADER_HPP_
#define IO_OIDIS_ONION_RESOURCEMANAGER_UTILS_BINARYREADER_HPP_

#include <fstream>
#include <string>

#include "../Core/Protocol.hpp"

namespace Io::Oidis::Onion::ResourceManager::Utils {
    class BinaryReader {
     public:
        explicit BinaryReader(const std::string& $path);

        Core::Protocol::Types::Data getContents();

     private:
        std::ifstream stream;
    };
}

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_UTILS_BINARYREADER_HPP_
