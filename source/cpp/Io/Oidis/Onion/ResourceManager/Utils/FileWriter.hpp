/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_UTILS_FILEWRITER_HPP_
#define IO_OIDIS_ONION_RESOURCEMANAGER_UTILS_FILEWRITER_HPP_

#include <cstdint>
#include <fstream>

#include "../Core/Protocol.hpp"
#include "../Core/Resource.hpp"

namespace Io::Oidis::Onion::ResourceManager::Utils {
    class FileWriter {
     public:
        explicit FileWriter(const std::string &$path, Core::Protocol::Types::ContentOffset $contentOffset);

        void Write(Core::Resource &&$resource);

        void WriteProtocolVersion();

        void Close();

     private:
        void writeOffset();

        void writeMagicSequence();

        std::ofstream stream;
        Core::Protocol::Types::ContentOffset contentOffset = 0;
    };
}

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_UTILS_FILEWRITER_HPP_
