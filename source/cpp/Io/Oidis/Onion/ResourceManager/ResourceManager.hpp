/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_RESOURCEMANAGER_HPP_
#define IO_OIDIS_ONION_RESOURCEMANAGER_RESOURCEMANAGER_HPP_

#if __cplusplus < 201703L
#   ifdef __clang__
#       pragma clang diagnostic push
#       pragma clang diagnostic ignored "-Wc++17-extensions"
#   endif
#endif

#include <string>
#include <unordered_map>

using std::string;

#include "Core/Resource.hpp"
#include "Writer/ResourceWriter.hpp"
#include "Reader/ResourceReader.hpp"
#include "Logger/LoggerConsole.hpp"
#include "Logger/LoggerDevNull.hpp"
#include "Logger/LogManager.hpp"

namespace Io::Oidis::Onion::ResourceManager {
    class ResourceManager {
        typedef Io::Oidis::Onion::ResourceManager::Core::Resource Resource;
        typedef Io::Oidis::Onion::ResourceManager::Core::ResourceMap ResourceMap;

     public:
        /**
         * Constructs ResourceManager object. With this constructions the Initialize(<path>) method has to been used before Read/Write.
         */
        ResourceManager() = default;

        /**
         * Constructs ResourceManager object attached to binary specified by path.
         * @param $binPath Specify binary path.
         */
        explicit ResourceManager(string $binPath);

        /**
         * Initialize ResourceManager and attach to binary path.
         * @param $path Specify binary path.
         */
        void Initialize(const string &$path);

        /**
         * Explicitly initialize resources reader cache. Used internally in Read() method.
         */
        void ReaderInit();

        /**
         * Write resources map into binary file. If any of resource in map is not exists than it will be created. If any of resource in map
         * exists in binary file than resource will be updated.
         * @param $resources Specify resources map.
         */
        void Write(ResourceMap $resources);

        /**
         * Read resource from binary file. In the case of POSIX OS the very first read initializes resources cache to speed up next read
         * invocations. Any resource is identified by $group/$id identifier, on POSIX also only $group could be used.
         * @param $group Specify group name.
         * @param $id Specify resource ID.
         * @param $force Use true to force reload resources cache.
         * @return Returns resource object.
         */
        Resource Read(const std::string &$group, const std::string &$id, bool $force = false);

        /**
         * Destroy ResourceManager and free any internal caches.
         */
        void Destroy();

     private:
        Io::Oidis::Onion::ResourceManager::Reader::ResourceReader *resourceReader = nullptr;
        Io::Oidis::Onion::ResourceManager::Writer::ResourceWriter *resourceWriter = nullptr;
        bool isInitialized = false;
        bool isReaderInitialized = false;
        string binPath;
    };
}

#if __cplusplus < 201703L
#   ifdef __clang__
#       pragma clang diagnostic pop
#   endif
#endif

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_RESOURCEMANAGER_HPP_
