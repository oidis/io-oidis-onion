/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::ResourceManager::Writer {
    WriterSettings::WriterSettings(std::string $outputFilePath)
            : outputFilePath(std::move($outputFilePath)) {
    }

    std::string WriterSettings::getOutputFilePath() const {
        return this->outputFilePath;
    }

    std::ostream &operator<<(std::ostream &$stream, const WriterSettings &$params) {
        $stream << "output file path=" << $params.getOutputFilePath();

        return $stream;
    }
}
