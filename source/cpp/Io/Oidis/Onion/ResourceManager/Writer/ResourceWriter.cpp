/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

#ifdef WIN_PLATFORM

#include <Windows.h>

#endif

namespace Io::Oidis::Onion::ResourceManager::Writer {
    using Io::Oidis::Onion::ResourceManager::Utils::FileReader;
    using Io::Oidis::Onion::ResourceManager::Utils::FileWriter;
    using Io::Oidis::Onion::ResourceManager::Core::ResourceMap;
    using Io::Oidis::Onion::ResourceManager::Utils::ProtocolError;
    using Io::Oidis::Onion::ResourceManager::Core::Resource;

    ResourceWriter::ResourceWriter(const string &$path)
            : settings(WriterSettings($path)) {
    }

    ResourceWriter::ResourceWriter(WriterSettings $settings)
            : settings(std::move($settings)) {
    }

    void ResourceWriter::Write(ResourceMap $resources) {
        auto resourcesToUpdate = std::move($resources);

        if (!resourcesToUpdate.empty()) {
            Logger::LogManager::getLogger().Info("Updating " + std::to_string(resourcesToUpdate.size()) + " resources");

#ifndef WIN_PLATFORM
            Reader::ResourceReader resourceReader{this->settings.getOutputFilePath()};

            ResourceMap existingResources;
            decltype(existingResources) additionalResourcesToWrite;

            // Might happen that the file is untouched by the resource-writer
            try {
                existingResources = resourceReader.getResources();
            } catch (const ProtocolError &) {
                // dummy
            }

            Logger::LogManager::getLogger().Debug("Found " + std::to_string(existingResources.size()) + " existing resources");

            for (auto &item : resourcesToUpdate) {
                auto existingResource = std::find(std::begin(existingResources), std::end(existingResources), item);
                if (existingResource != std::end(existingResources)) {
                    Logger::LogManager::getLogger().Info("Updating resource " + existingResource->second.getName());
                    existingResource->second = std::move(item.second);
                }
            }

            Logger::LogManager::getLogger().Debug("Preparing rest of resources to write");

            for (auto &item : resourcesToUpdate) {
                if (!item.second.isEmpty()) {
                    additionalResourcesToWrite.emplace(std::make_pair(item.first, std::move(item.second)));
                }
            }

            Utils::CollectionUtils::ConcatenateHashMaps(existingResources, additionalResourcesToWrite);

            // TODO(nxf45876): This was already done by the ResourceReader during reading of resources, optimize
            FileReader fileReader{this->settings.getOutputFilePath(), false};
            fileReader.Close();

            Utils::FileUtils::truncateFile(this->settings.getOutputFilePath(), fileReader.getContentOffset());

            this->writeMap(std::move(existingResources));
#else
            HANDLE handle = BeginUpdateResource(this->settings.getOutputFilePath().c_str(), FALSE);
            if (handle != nullptr) {
                for (auto &item : resourcesToUpdate) {
                    Logger::LogManager::getLogger().Info("Updating resource " + item.second.getName());
                    string type = "DEFAULT";  // group
                    string name = "unknown";  // name
                    string id = item.first;
                    if (id.length() == 0) {
                        throw std::runtime_error("Resource ID/name has invalid format.");
                    }
                    auto index = id.find('/');
                    Resource &resource = item.second;
                    if (index != string::npos) {
                        type = id.substr(0, index);
                        name = id.substr(index + 1, string::npos);
                    } else {
                        name = id;
                    }
                    auto data = resource.getData();
                    if (UpdateResource(handle, type.c_str(), name.c_str(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), data.data(),
                                       data.size()) != TRUE) {
                        throw std::runtime_error("Resource write failed.");
                    }
                }
                if (EndUpdateResource(handle, FALSE) != TRUE) {
                    auto errorId = GetLastError();
                    char lpMsgBuf[512];
                    auto size = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                              NULL,
                                              errorId,
                                              MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                                              lpMsgBuf, 512, NULL);
                    throw std::runtime_error("Resource update failed in final phase: " + string(lpMsgBuf, size));
                }
            } else {
                throw std::runtime_error("Can not open file for resource update.");
            }
#endif
        }
    }

    void ResourceWriter::writeMap(ResourceMap $resources) {
#ifndef WIN_PLATFORM
        auto resourcesToWrite = std::move($resources);

        if (!resourcesToWrite.empty()) {
            Logger::LogManager::getLogger().Info("Writing " + std::to_string(resourcesToWrite.size()) + " resources");

            FileReader fileReader{this->settings.getOutputFilePath(), false};
            fileReader.Close();

            if (!fileReader.isVanillaFile()) {
                Logger::LogManager::getLogger().Debug("Will truncate the file");

                Utils::FileUtils::truncateFile(this->settings.getOutputFilePath(), fileReader.getContentEnd());
            }

            FileWriter fileWriter{this->settings.getOutputFilePath(), fileReader.getContentOffset()};

            if (fileReader.isVanillaFile()) {
                fileWriter.WriteProtocolVersion();
            }

            for (auto &item : resourcesToWrite) {
                item.second.setName(item.first);
                fileWriter.Write(std::move(item.second));
            }

            fileWriter.Close();
        }
#else
        (void)$resources;
        throw std::runtime_error("wirteMap() is not supported on windows.");
#endif
    }
}
