/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_WRITER_WRITERSETTINGS_HPP_
#define IO_OIDIS_ONION_RESOURCEMANAGER_WRITER_WRITERSETTINGS_HPP_

#include <iosfwd>
#include <string>
#include <vector>

namespace Io::Oidis::Onion::ResourceManager::Writer {
    class WriterSettings {
     public:
        explicit WriterSettings(std::string $outputFilePath);

        std::string getOutputFilePath() const;

     private:
        std::string outputFilePath;
    };

    std::ostream &operator<<(std::ostream &$stream, const WriterSettings &$params);
}

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_WRITER_WRITERSETTINGS_HPP_
