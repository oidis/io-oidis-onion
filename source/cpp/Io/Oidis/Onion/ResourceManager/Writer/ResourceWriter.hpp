/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_WRITER_RESOURCEWRITER_HPP_
#define IO_OIDIS_ONION_RESOURCEMANAGER_WRITER_RESOURCEWRITER_HPP_

#include "WriterSettings.hpp"

namespace Io::Oidis::Onion::ResourceManager::Writer {
    class ResourceWriter {
        typedef Io::Oidis::Onion::ResourceManager::Core::ResourceMap ResourceMap;

     public:
        explicit ResourceWriter(const string &$path);

        explicit ResourceWriter(WriterSettings $settings);

        void Write(ResourceMap $resources);

     protected:
        void writeMap(ResourceMap $resources);

     private:
        WriterSettings settings;
    };
}

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_WRITER_RESOURCEWRITER_HPP_
