/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "ResourceManager.hpp"

namespace Io::Oidis::Onion::ResourceManager {
    using Io::Oidis::Onion::ResourceManager::Reader::ResourceReader;
    using Io::Oidis::Onion::ResourceManager::Writer::ResourceWriter;
    using Io::Oidis::Onion::ResourceManager::Core::Resource;
    using Io::Oidis::Onion::ResourceManager::Core::ResourceMap;

    ResourceManager::ResourceManager(string $binPath)
            : binPath(std::move($binPath)) {}

    void ResourceManager::Initialize(const string &$path = "") {
        if (!$path.empty()) {
            this->binPath = $path;
        }
        if (this->binPath.empty()) {
            throw std::runtime_error("Resources manager could not be initialized without binary file path.");
        }

        this->resourceWriter = new ResourceWriter(this->binPath);
        this->resourceReader = new ResourceReader(this->binPath);
        this->isInitialized = true;
    }

    void ResourceManager::Write(ResourceMap $resources) {
        if (!this->isInitialized) {
            this->Initialize();
        }
        this->resourceWriter->Write(std::move($resources));
    }

    Resource ResourceManager::Read(const std::string &$group, const std::string &$id, bool $force) {
        if (!this->isInitialized) {
            this->Initialize();
        }
        if (!this->isReaderInitialized || $force) {
            this->ReaderInit();
        }
        return this->resourceReader->Read($group, $id);
    }

    void ResourceManager::Destroy() {
        this->isInitialized = false;
        this->isReaderInitialized = false;
        delete this->resourceReader;
        delete this->resourceWriter;
    }

    void ResourceManager::ReaderInit() {
        this->resourceReader->Initialize();
        this->isReaderInitialized = true;
    }
}
