/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_READER_RESOURCEREADER_HPP_
#define IO_OIDIS_ONION_RESOURCEMANAGER_READER_RESOURCEREADER_HPP_

#include "ReaderSettings.hpp"

#ifdef WIN_PLATFORM
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>

#endif

namespace Io::Oidis::Onion::ResourceManager::Reader {
    class ResourceReader {
        typedef Io::Oidis::Onion::ResourceManager::Core::Resource Resource;
        typedef Io::Oidis::Onion::ResourceManager::Core::ResourceMap ResourceMap;

     public:
        static ResourceMap &getCachedResources() { return ResourceReader::cachedResources; }

        static void ClearCachedResources() { ResourceReader::cachedResources.clear(); }

        explicit ResourceReader(const string &$path);

        explicit ResourceReader(ReaderSettings $settings);

        ResourceMap getResources(const size_t $numberOfBuckets = 0);

        Resource Read(const std::string &$group, const std::string &$id, size_t $numberOfBuckets = 0);

        bool HasEmbeddedResources();

        void Initialize();

        void Dispose();

     private:
        void CacheResources();

        ReaderSettings settings;
#ifdef WIN_PLATFORM
        HMODULE hmodule = nullptr;
#endif

        static ResourceMap cachedResources;
    };
}

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_READER_RESOURCEREADER_HPP_
