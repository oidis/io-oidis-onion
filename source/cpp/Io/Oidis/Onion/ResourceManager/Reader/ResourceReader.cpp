/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#if defined UNICODE && defined WIN_PLATFORM
#pragma push_macro("UNICODE")
#undef UNICODE
#endif

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::ResourceManager::Reader {
    using Io::Oidis::Onion::ResourceManager::Utils::FileReader;
    using Io::Oidis::Onion::ResourceManager::Core::Resource;
    using Io::Oidis::Onion::ResourceManager::Core::ResourceMap;

    ResourceMap ResourceReader::cachedResources = {};

    ResourceReader::ResourceReader(const string &$path)
            : settings(ReaderSettings($path)) {
    }

    ResourceReader::ResourceReader(ReaderSettings $settings)
            : settings(std::move($settings)) {
    }

    ResourceMap ResourceReader::getResources(const size_t $numberOfBuckets) {
#ifndef WIN_PLATFORM
        ResourceMap resourcesToReturn{$numberOfBuckets};

        FileReader reader{this->settings.getInputFilePath(), true};
        reader.ReadResources([&resourcesToReturn](Core::Resource &&$foundResource) {
            auto extractedResource = std::move($foundResource);

            Logger::LogManager::getLogger().Info("Inspecting resource " + extractedResource.getName());

            resourcesToReturn.emplace(std::make_pair(extractedResource.moveName(), std::move(extractedResource)));
        });

        return resourcesToReturn;
#else
        (void)$numberOfBuckets;
        throw std::runtime_error("Use ReadOne() on this platform.");
#endif
    }

    Resource ResourceReader::Read(const std::string &$group, const std::string &$id, size_t $numberOfBuckets) {
#ifdef WIN_PLATFORM
        (void)$numberOfBuckets;
        if (!this->settings.getInputFilePath().empty()) {
            this->hmodule = LoadLibrary(this->settings.getInputFilePath().c_str());
            if (this->hmodule == nullptr) {
                auto errorId = GetLastError();
                char lpMsgBuf[512];
                auto size = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                          NULL,
                                          errorId,
                                          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                                          lpMsgBuf, 512, NULL);
                throw std::runtime_error(
                        (string("Could not load exe \"") + this->settings.getInputFilePath() + "\": " + string(lpMsgBuf, size)).c_str());
            }
        }
        string id = $id;
        string group = $group;
        if (id.empty()) {
            id = group;
            group = "DEFAULT";
        }
        HRSRC hrsrc = FindResource(this->hmodule, id.c_str(), group.c_str());
        if (hrsrc != nullptr) {
            HGLOBAL hglobal = LoadResource(this->hmodule, hrsrc);
            if (hglobal != nullptr) {
                auto *bufPtr = static_cast<const char *>(LockResource(hglobal));
                if (bufPtr != nullptr) {
                    auto size = static_cast<unsigned int>(SizeofResource(this->hmodule, hrsrc));
                    Resource resource;
                    // TODO(nxa33894) why this is not set for POSIX?
                    // resource.setName(group + (id.empty() ? "" : "/" + id));
                    resource.setData(bufPtr, size);
                    FreeResource(hglobal);
                    FreeLibrary(this->hmodule);
                    return resource;
                }
                throw std::runtime_error("Locked resource points to NULL for resource");
            }
            throw std::runtime_error("Can not load resource");
        }
        throw std::runtime_error("Can not find resource");
#else
        (void)$numberOfBuckets;
        const auto resource = ResourceReader::getCachedResources().find($group + ($id.empty() ? "" : "/" + $id));
        if (resource != ResourceReader::getCachedResources().cend()) {
            return (resource->second);
        } else {
            throw std::runtime_error("Cached resource not found.");
        }
#endif
    }

    bool ResourceReader::HasEmbeddedResources() {
#ifndef WIN_PLATFORM
        const auto extractedResources = this->getResources();
        const std::string resourceId = "CONFIG/PACKAGE_MAP";
        return (extractedResources.find(resourceId) != std::cend(extractedResources));
#else
        if (!this->settings.getInputFilePath().empty()) {
            this->hmodule = LoadLibrary(this->settings.getInputFilePath().c_str());
            if (this->hmodule != nullptr) {
                HRSRC hrsrc = FindResource(this->hmodule, "PACKAGE_MAP", "CONFIG");
                if (hrsrc != nullptr) {
                    HGLOBAL hglobal = LoadResource(this->hmodule, hrsrc);
                    if (hglobal != nullptr) {
                        auto *bufPtr = static_cast<char *>(LockResource(hglobal));
                        if (bufPtr != nullptr) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
#endif
    }

    void ResourceReader::CacheResources() {
#ifndef WIN_PLATFORM
        ResourceReader::cachedResources = this->getResources();
#endif
    }

    void ResourceReader::Initialize() {
        this->CacheResources();
    }

    void ResourceReader::Dispose() {
        ResourceReader::ClearCachedResources();
    }
}

#if defined WIN_PLATFORM
#pragma pop_macro("UNICODE")
#endif
