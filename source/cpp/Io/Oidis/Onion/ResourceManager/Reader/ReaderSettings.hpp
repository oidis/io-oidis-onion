/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_READER_READERSETTINGS_HPP_
#define IO_OIDIS_ONION_RESOURCEMANAGER_READER_READERSETTINGS_HPP_

#include <iosfwd>
#include <string>

namespace Io::Oidis::Onion::ResourceManager::Reader {
    class ReaderSettings {
     public:
        explicit ReaderSettings(std::string $inputFilePath);

        std::string getInputFilePath() const;

     private:
        std::string inputFilePath;
    };

    std::ostream &operator<<(std::ostream &$stream, const ReaderSettings &$parser);
}

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_READER_READERSETTINGS_HPP_
