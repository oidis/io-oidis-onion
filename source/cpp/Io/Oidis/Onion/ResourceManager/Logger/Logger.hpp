/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_LOGGER_LOGGER_HPP_
#define IO_OIDIS_ONION_RESOURCEMANAGER_LOGGER_LOGGER_HPP_

#include <functional>
#include <iostream>

namespace Io::Oidis::Onion::ResourceManager::Logger {
    class Logger {
     public:
        using LogCallback = std::function<void(std::string &&$message)>;

        Logger() = default;

        Logger(LogCallback $errorCallback, LogCallback $infoCallback, LogCallback $debugCallback);

        void Error(std::string &&$message) const;

        void Info(std::string &&$message) const;

        void Debug(std::string &&$message) const;

     private:
        void invokeCallback(const LogCallback &$callback, std::string &&$message) const;

        LogCallback error = nullptr;
        LogCallback info = nullptr;
        LogCallback debug = nullptr;
    };
}

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_LOGGER_LOGGER_HPP_
