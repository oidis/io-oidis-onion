/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_LOGGER_LOGMANAGER_HPP_
#define IO_OIDIS_ONION_RESOURCEMANAGER_LOGGER_LOGMANAGER_HPP_

#include "Logger.hpp"

namespace Io::Oidis::Onion::ResourceManager::Logger {
    class LogManager {
     public:
        static Logger& getLogger();

        static void setLogger(const Logger &$logger);

     private:
        static Logger logger;
    };
}

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_LOGGER_LOGMANAGER_HPP_
