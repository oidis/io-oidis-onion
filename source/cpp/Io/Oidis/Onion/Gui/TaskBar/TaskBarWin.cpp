/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#ifdef UNICODE
#define OVERRIDE_UNICODE 1
#pragma push_macro("UNICODE")
#undef UNICODE
#endif

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::TaskBar {

    TaskBarWin::TaskBarWin(void *$owner)
            : TaskBarBase($owner) {
        CoInitialize(nullptr);
        CoCreateInstance(CLSID_TaskbarList, nullptr, CLSCTX_INPROC_SERVER, IID_ITaskbarList3,
                         reinterpret_cast<void **>(&this->taskBarList));
        if ($owner == nullptr) {
            this->createWindow();
        } else {
            this->window = static_cast<HWND>($owner);
        }
    }

    TaskBarWin::~TaskBarWin() {
        this->Clear();
    }

    void TaskBarWin::createWindow() {
        string className = "io-oidis-onion-taskbar-class";
        string windowName = "io-oidis-onion-taskbar-window";
        WNDCLASS wc = {};
        wc.lpfnWndProc = DefWindowProc;
        wc.hInstance = nullptr;
        wc.lpszClassName = className.c_str();
        RegisterClass(&wc);
        this->window = CreateWindowEx(
                WS_EX_OVERLAPPEDWINDOW,
                className.c_str(),
                windowName.c_str(),
                WS_POPUP,
                CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                nullptr,
                nullptr,
                nullptr,
                nullptr);
        if (this->window != nullptr) {
            ShowWindow(this->window, SW_SHOWNORMAL);
        }
    }

    bool TaskBarWin::setProgressState(TaskbarProgressState $state) {
        bool retVal = false;
        TBPFLAG flag = TBPF_NOPROGRESS;
        if ($state == TaskbarProgressState::INDETERMINATE) {
            flag = TBPF_INDETERMINATE;
        } else if ($state == TaskbarProgressState::NORMAL) {
            flag = TBPF_NORMAL;
        } else if ($state == TaskbarProgressState::ERROR_STATE) {
            flag = TBPF_ERROR;
        } else if ($state == TaskbarProgressState::PAUSED) {
            flag = TBPF_PAUSED;
        }

        if ((this->taskBarList != nullptr) && (this->window != nullptr)) {
            if (this->taskBarList->SetProgressState(this->window, flag) == S_OK) {
                retVal = true;
            }
        }

        return retVal;
    }

    bool TaskBarWin::setProgressValue(const int $completed, const int $total) {
        TaskBarBase::setProgressValue($completed, $total);
        bool retVal = false;

        if ((this->taskBarList != nullptr) && (this->window != nullptr)) {
            if (this->taskBarList->SetProgressValue(this->window, static_cast<ULONGLONG>($completed),
                                                    static_cast<ULONGLONG>($total)) == S_OK) {
                retVal = true;
            }
        }

        return retVal;
    }

    void TaskBarWin::Clear() {
        if (this->taskBarList != nullptr) {
            CoUninitialize();
        }
        if (this->window != nullptr) {
            DestroyWindow(this->window);
        }
    }
}

#ifdef OVERRIDE_UNICODE
#pragma pop_macro("UNICODE")
#endif

#endif  // WIN_PLATFORM
