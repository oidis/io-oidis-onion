/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_TASKBAR_TASKBARWIN_HPP_
#define IO_OIDIS_ONION_GUI_TASKBAR_TASKBARWIN_HPP_

#ifdef WIN_PLATFORM

#include <windows.h>
#include <shobjidl.h>

#include "TaskBarBase.hpp"

namespace Io::Oidis::Onion::Gui::TaskBar {
    class TaskBarWin : public TaskBarBase {
     public:
        /**
         * @param $owner Expected a valid HWND on Windows.
         */
        explicit TaskBarWin(void *$owner);

        ~TaskBarWin();

        bool setProgressState(TaskbarProgressState $state) override;

        bool setProgressValue(const int $completed, const int $total) override;

        void Clear() override;

     private:
        HWND window = nullptr;
        ITaskbarList3 *taskBarList = nullptr;

        void createWindow();
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_ONION_GUI_TASKBAR_TASKBARWIN_HPP_
