/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::TaskBar {

    TaskBarBase::TaskBarBase(void *$owner) {
        (void)$owner;
    }

    bool TaskBarBase::setProgressValue(const int $completed, const int $total) {
        if ($completed < 0 || $total < 0) {
            throw std::logic_error{"Progress in the TaskBar cannot be negative"};
        }

        if ($completed > $total) {
            throw std::logic_error{"Completed progress in the TaskBar " + std::to_string($completed) + " cannot be higher than the total"};
        }

        return false;
    }
}
