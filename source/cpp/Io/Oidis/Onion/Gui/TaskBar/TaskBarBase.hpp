/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_TASKBAR_TASKBARBASE_HPP_
#define IO_OIDIS_ONION_GUI_TASKBAR_TASKBARBASE_HPP_

namespace Io::Oidis::Onion::Gui::TaskBar {
    class TaskBarBase {
     public:
        // TODO(nxa33118) use WUI_DECLARE macro from xcppcommons?
        enum TaskbarProgressState : unsigned char {
            NOPROGRESS = 0,
            INDETERMINATE,
            NORMAL,
            ERROR_STATE,
            PAUSED
        };

        /**
         * Initialize TaskBar.
         * @param $owner If $owner is nullptr, a default window with tasbkar is created. If $owner is correct owner, it is used.
         */
        explicit TaskBarBase(void *$owner);

        /**
         * Set state in which the progress bar is.
         * @param $state State of the progress bar, one of TaskbarProgressState.
         * @return Return true if set was successful.
         */
        virtual bool setProgressState(TaskbarProgressState $state) = 0;

        /**
         * @param $completed Set $completed value out of $total.
         * @param $total Max value of progress.
         * @return Return true if set was successful.
         */
        virtual bool setProgressValue(const int $completed, const int $total) = 0;

        /**
         * Tear down TaskBar.
         */
        virtual void Clear() = 0;
    };
}

#endif  // IO_OIDIS_ONION_GUI_TASKBAR_TASKBARBASE_HPP_
