/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::TaskBar::TaskBarFactory {
    TaskBarBase *Create(void *$owner) {
#if defined(WIN_PLATFORM) || defined(_WIN32)
        return new TaskBarWin($owner);
#elif defined(MAC_PLATFORM) || defined(__APPLE__)
        return new TaskBarMac($owner);
#else
        (void)$owner;
        return nullptr;
#endif
    }
}
