/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_TASKBAR_TASKBARMAC_HPP_
#define IO_OIDIS_ONION_GUI_TASKBAR_TASKBARMAC_HPP_

#if defined(MAC_PLATFORM) || defined(__APPLE__)

#import <AppKit/NSImageView.h>
#import <Appkit/NSProgressIndicator.h>

@interface TaskBarWrapper : NSObject

@property(nonatomic, retain) NSImageView *dockView;

@property(nonatomic, retain) NSProgressIndicator *progressBar;

- (id)init;

- (void)ShowProgressBar;

- (void)HideProgressBar;

@end;

#include "TaskBarBase.hpp"

namespace Io::Oidis::Onion::Gui::TaskBar {
    class TaskBarMac : public TaskBarBase {
     public:
        explicit TaskBarMac(void *$owner);

        ~TaskBarMac();

        bool setProgressState(TaskbarProgressState $state) override;

        bool setProgressValue(const int $completed, const int $total) override;

        void Clear() override;

     private:
        TaskBarWrapper *taskBar = nil;
    };
}

#endif  // defined(MAC_PLATFORM) || defined(__APPLE__)

#endif  // IO_OIDIS_ONION_GUI_TASKBAR_TASKBARMAC_HPP_
