/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::Structures {
    using Io::Oidis::Onion::Gui::Structures::ColorBase;

    Font::Font() {
        this->color = Io::Oidis::Onion::Gui::Structures::ColorFactory::Create("#000000");
    }

#if defined WIN_PLATFORM || defined LINUX_PLATFORM

    Font::~Font() {
        delete this->color;
    }

#endif

    const std::string &Font::getName() const {
        return this->name;
    }

    void Font::setName(const std::string &$name) {
        this->name = $name;
    }

    int Font::getSize() const {
        return this->size;
    }

    void Font::setSize(int $size) {
        this->size = $size;
    }

    bool Font::isBold() const {
        return this->bold;
    }

    void Font::setBold(bool $bold) {
        this->bold = $bold;
    }

    const ColorBase *Font::getColor() const {
        return this->color;
    }

    void Font::setColor(const ColorBase *$color) {
        this->color = const_cast<ColorBase *>($color);
    }
}
