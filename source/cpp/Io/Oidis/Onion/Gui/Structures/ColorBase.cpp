/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

using std::string;

namespace Io::Oidis::Onion::Gui::Structures {

    ColorBase::ColorBase() {
        this->a = this->r = this->g = this->b = 0;
    }

    ColorBase::ColorBase(const string &$value)
            : ColorBase() {
        this->FromString($value);
    }

    void ColorBase::FromString(const string &$value) {
        string value = $value;

        if (!value.empty()) {
            if (value[0] == '#') {
                value = value.substr(1);
            }
        }

        if (value.size() >= 2) {
            if (value[0] == '0' && value[1] == 'x') {
                value = value.substr(2);
            }
        }

        string aStr;
        string rStr;
        string gStr;
        string bStr;

        if (value.size() == 6) {
            aStr = "ff";
            rStr = value.substr(0, 2);
            gStr = value.substr(2, 2);
            bStr = value.substr(4, 2);
        } else if (value.size() == 8) {
            aStr = value.substr(0, 2);
            rStr = value.substr(2, 2);
            gStr = value.substr(4, 2);
            bStr = value.substr(6, 2);
        } else {
            throw CastException("Color::FromString can not convert other than 6 or 8 long string to color.");
        }

        this->a = std::stoi(aStr, nullptr, 16);
        this->r = std::stoi(rStr, nullptr, 16);
        this->g = std::stoi(gStr, nullptr, 16);
        this->b = std::stoi(bStr, nullptr, 16);

        this->updateNative();
    }

    void ColorBase::updateNative() {
        // dummy: should be implemented in each platform dependent classes
    }

    std::ostream &ColorBase::write(std::ostream &$ios, int $value) const {
        return $ios << std::hex << std::noshowbase << std::setw(2) << std::setfill('0') << $value;
    }
}
