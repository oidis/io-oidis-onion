/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_STRUCTURES_COLORWIN_HPP_
#define IO_OIDIS_ONION_GUI_STRUCTURES_COLORWIN_HPP_

#ifdef WIN_PLATFORM

#include <string>
#include <fstream>
#include <sstream>

#include "ColorBase.hpp"

namespace Io::Oidis::Onion::Gui::Structures {
    class ColorWin : public ColorBase {
        using ColorBase::ColorBase;

     public:
        explicit ColorWin(const std::string &$value);

        void *getNative() const override;

        std::string ToString(const std::string &$prefix) const override;

     protected:
        void updateNative() override;

     private:
        std::shared_ptr<Gdiplus::Color> nativeColor = nullptr;
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_ONION_GUI_STRUCTURES_COLORWIN_HPP_
