/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_STRUCTURES_COLORBASE_HPP_
#define IO_OIDIS_ONION_GUI_STRUCTURES_COLORBASE_HPP_

namespace Io::Oidis::Onion::Gui::Structures {
    /**
     * This class defines object to store RGBA color model.
     */
    class ColorBase {
     public:
        /**
         * Exceptions which can be thrown when creating a color from string.
         */
        class CastException : std::exception {
         public:
            explicit CastException(const string &$message)
                    : message($message) {}

            const char *what() const throw() override {
                return message.c_str();
            }

         private:
            string message = "";
        };

        /**
         * Construct default color.
         */
        ColorBase();

        virtual ~ColorBase() = default;

        /**
         * Constructs color from color string. Check FromString for supported formats.
         * @param $value
         */
        explicit ColorBase(const std::string &$value);

        /**
         * Initialize color from color string.
         * Supported formats:
         *  - #(AA)RRGGBB
         *  - 0x(AA)RRGGBB
         *  - (AA)RRGGBB
         *  .
         *  Each values are in two-digits hexadecimal. Alpha channel is optional, if not explicitly defined 0xff is used by default.
         * @param $value Specify color string.
         */
        void FromString(const std::string &$value);

        /**
         * Converts color to color string.
         * @param $prefix Specify color string prefix.
         * @return Returns prefixed color string with alpha channel (AARRGGBB).
         */
        virtual std::string ToString(const std::string &$prefix = "#") const = 0;

        /**
         * @return Returns platform specific void pointer (void *) containing a pointer to data. For each platform
         * the underlying data type is different.
         */
        virtual void *getNative() const = 0;

     protected:
        int a;
        int r;
        int g;
        int b;

        virtual void updateNative();

        std::ostream &write(std::ostream &$ios, int $value) const;
    };
}

#endif  // IO_OIDIS_ONION_GUI_STRUCTURES_COLORBASE_HPP_
