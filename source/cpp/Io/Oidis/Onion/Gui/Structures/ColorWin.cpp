/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

#ifdef WIN_PLATFORM

namespace Io::Oidis::Onion::Gui::Structures {

    ColorWin::ColorWin(const std::string &$value)
            : ColorBase($value) {
        this->updateNative();
    }

    void *ColorWin::getNative() const {
        return static_cast<void *>(this->nativeColor.get());
    }

    std::string ColorWin::ToString(const std::string &$prefix) const {
        std::ostringstream oss;
        oss << $prefix;

        ColorBase::write(oss, static_cast<int>(this->nativeColor->GetA()));
        ColorBase::write(oss, static_cast<int>(this->nativeColor->GetRed()));
        ColorBase::write(oss, static_cast<int>(this->nativeColor->GetGreen()));
        ColorBase::write(oss, static_cast<int>(this->nativeColor->GetBlue()));
        return oss.str();
    }

    void ColorWin::updateNative() {
        this->nativeColor.reset(new Gdiplus::Color(
                static_cast<BYTE>(this->a),
                static_cast<BYTE>(this->r),
                static_cast<BYTE>(this->g),
                static_cast<BYTE>(this->b)));
    }
}

#endif  // WIN_PLATFORM
