/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_STRUCTURES_COLORFACTORY_HPP_
#define IO_OIDIS_ONION_GUI_STRUCTURES_COLORFACTORY_HPP_

namespace Io::Oidis::Onion::Gui::Structures::ColorFactory {
    ColorBase *Create(const string &$color);
}

#endif  // IO_OIDIS_ONION_GUI_STRUCTURES_COLORFACTORY_HPP_
