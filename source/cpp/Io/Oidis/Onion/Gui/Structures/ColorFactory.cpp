/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::Structures::ColorFactory {

    ColorBase *Create(const string &$color) {
#if defined(MAC_PLATFORM) || defined(__APPLE__)
        return new ColorMac($color);
#elif (defined(LINUX_PLATFORM) || defined(__linux__)) && !defined(ONION_NO_GUI)  // NOLINT(whitespace/parens)
        return new ColorLinux($color);
#elif defined(WIN_PLATFORM) || defined(_WIN32)
        return new ColorWin($color);
#else
        return nullptr;
#endif
    }

}
