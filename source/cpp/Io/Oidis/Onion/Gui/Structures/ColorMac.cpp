/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include <AppKit/NSColor.h>

#include <sstream>

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::Structures {

    ColorMac::ColorMac(const std::string &$value)
            : ColorBase($value) {
        this->updateNative();
    }

    void *ColorMac::getNative() const {
        return this->nativeColor;
    }

    std::string ColorMac::ToString(const std::string &$prefix) const {
        std::ostringstream oss;
        oss << $prefix;

        ColorBase::write(oss, static_cast<int>([this->nativeColor alphaComponent]*255));
        ColorBase::write(oss, static_cast<int>([this->nativeColor redComponent]*255));
        ColorBase::write(oss, static_cast<int>([this->nativeColor greenComponent]*255));
        ColorBase::write(oss, static_cast<int>([this->nativeColor blueComponent]*255));

        return oss.str();
    }

    void ColorMac::updateNative() {
        this->nativeColor = [NSColor colorWithRed:this->r/255.0 green:this->g/255.0 blue:this->b/255.0 alpha:this->a/255.0];
    }
}

#endif  // MAC_PLATFORM
