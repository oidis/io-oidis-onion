/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#if defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::Structures {

    void *ColorLinux::getNative() const {
        return static_cast<void *>(this->nativeColor);
    }

    std::string ColorLinux::ToString(const std::string &$prefix) const {
        std::ostringstream oss;
        oss << $prefix;

        ColorBase::write(oss, static_cast<int>(0xff));
        ColorBase::write(oss, static_cast<int>(this->nativeColor->red));
        ColorBase::write(oss, static_cast<int>(this->nativeColor->green));
        ColorBase::write(oss, static_cast<int>(this->nativeColor->blue));
        return oss.str();
    }

    ColorLinux::ColorLinux(const std::string &$value)
            : ColorBase($value) {
        this->updateNative();
    }

    void ColorLinux::updateNative() {
        this->nativeColor = new GdkColor();
        this->nativeColor->pixel = 0;
        this->nativeColor->red = this->r;
        this->nativeColor->green = this->g;
        this->nativeColor->blue = this->b;
    }
}

#endif  // LINUX_PLATFORM
