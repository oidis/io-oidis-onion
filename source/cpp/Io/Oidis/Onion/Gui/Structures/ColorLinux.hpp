/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_STRUCTURES_COLORLINUX_HPP_
#define IO_OIDIS_ONION_GUI_STRUCTURES_COLORLINUX_HPP_

#if defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#include <string>

#include "ColorBase.hpp"

namespace Io::Oidis::Onion::Gui::Structures {
    class ColorLinux : public ColorBase {
        using ColorBase::ColorBase;

     public:
        explicit ColorLinux(const std::string &$value);

        /**
         * On Mac obj-C uses garbage collection, thats why on Linux and Win there are destructors for internal data.
         */
        ~ColorLinux() override = default;

        void *getNative() const override;

        std::string ToString(const std::string &$prefix) const override;

     protected:
        void updateNative() override;

     private:
        GdkColor *nativeColor = nullptr;
    };
}

#endif  // LINUX_PLATFORM
#endif  // IO_OIDIS_ONION_GUI_STRUCTURES_COLORLINUX_HPP_
