/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_STRUCTURES_COLORMAC_HPP_
#define IO_OIDIS_ONION_GUI_STRUCTURES_COLORMAC_HPP_

#ifdef MAC_PLATFORM

#include "ColorBase.hpp"

@class NSColor;

namespace Io::Oidis::Onion::Gui::Structures {
    /**
     * This class defines object to store RGBA color model.
     */
    class ColorMac : public ColorBase {
     public:
        explicit ColorMac(const std::string &$value);

        void *getNative() const override;

        ~ColorMac() override = default;

        std::string ToString(const std::string &$prefix = "#") const override;

     protected:
        void updateNative() override;

     private:
        NSColor *nativeColor = nullptr;
    };
}

#endif  // MAC_PLATFORM
#endif  // IO_OIDIS_ONION_GUI_STRUCTURES_COLORMAC_HPP_
