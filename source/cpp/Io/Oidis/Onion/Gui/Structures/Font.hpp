/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_STRUCTURES_FONT_HPP_
#define IO_OIDIS_ONION_GUI_STRUCTURES_FONT_HPP_

#include "ColorBase.hpp"

namespace Io::Oidis::Onion::Gui::Structures {
    class Font {
     public:
        Font();

#if defined WIN_PLATFORM || defined LINUX_PLATFORM

        /**
         * On Mac obj-C uses garbage collection, thats why on Linux and Win there are destructors for internal data.
         */
        ~Font();

#endif

        const std::string &getName() const;

        void setName(const std::string &$name);

        int getSize() const;

        void setSize(int $size);

        bool isBold() const;

        void setBold(bool $bold);

        const Io::Oidis::Onion::Gui::Structures::ColorBase *getColor() const;

        void setColor(const Io::Oidis::Onion::Gui::Structures::ColorBase *$color);

     private:
        std::string name = "Georgia";
        int size = 12;
        bool bold = false;
        Io::Oidis::Onion::Gui::Structures::ColorBase *color;
    };
}

#endif  // IO_OIDIS_ONION_GUI_STRUCTURES_FONT_HPP_
