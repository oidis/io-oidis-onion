/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_LABEL_LABELLINUX_HPP_
#define IO_OIDIS_ONION_GUI_LABEL_LABELLINUX_HPP_

#if defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"

#include <gtk/gtk.h>

#pragma GCC diagnostic pop

#include "LabelBase.hpp"

namespace Io::Oidis::Onion::Gui::Label {
    class LabelLinux : public LabelBase {
     public:
        using LabelBase::LabelBase;

        /**
         * Draw callback for gtk framework. Draws text on specified position.
         * @param $widget Callback parameter, window to draw in.
         * @param $event Callback parameter, event which called the callback.
         * @param $data Custom data, pointer to LabelLinux (LabelLinux *) in this case.
         * @return Return TRUE beacause the event has benn handled.
         */
        static gboolean doDraw(GtkWidget *$widget, GdkEventExpose *$event, gpointer $data);

        ~LabelLinux();

        void *getNative() const override;

     protected:
        void draw(void *$args) override;
    };
}

#endif  // LINUX_PLATFORM

#endif  // IO_OIDIS_ONION_GUI_LABEL_LABELLINUX_HPP_
