/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_LABEL_LABELBASE_HPP_
#define IO_OIDIS_ONION_GUI_LABEL_LABELBASE_HPP_

#include "../Primitives/BaseGuiObject.hpp"
#include "../Structures/Font.hpp"

namespace Io::Oidis::Onion::Gui::Label {
    class LabelBase : public Gui::Primitives::BaseGuiObject {
     public:
        enum TextJustify {
            LEFT = 0,
            CENTER = 1,
            RIGHT = 2
        };

        struct LabelSettings {
         public:
            int x = 0;
            int y = 0;
            int width = 0;
            int height = 0;
            string font;
            int size = 0;
            TextJustify justify;
            string color = "#000000";
            bool visible = false;
            bool bold = false;
        };

        /**
         * Constructs default Label.
         */
        explicit LabelBase(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent);

        /**
         * Release internal data.
         */
        virtual ~LabelBase() = default;

        /**
         * Draw function which calls private draw() with platform-dependent data.
         * @param $args
         */
        virtual void Draw(void *$args) {
            this->draw($args);
        }

        virtual const Io::Oidis::Onion::Gui::Structures::Font *getFont();

        virtual void setFont(const Io::Oidis::Onion::Gui::Structures::Font *$font);

        virtual const string &getText();

        virtual void setText(const string &$text);

        virtual const TextJustify &getJustify();

        virtual void setJustify(const TextJustify &$justify);

     protected:
        virtual void draw(void *$args) = 0;

     private:
        string text;
        TextJustify justify;
        Io::Oidis::Onion::Gui::Structures::Font *font;
    };
}

#endif  // IO_OIDIS_ONION_GUI_LABEL_LABELBASE_HPP_
