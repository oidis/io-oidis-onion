/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include <Cocoa/Cocoa.h>

#include "../../../../../reference.hpp"

// @formatter:off
@interface LabelView : NSView {}

@property(atomic, strong) NSColor *color;
@property(atomic, strong) NSString *text;
@property(atomic, strong) NSFont *font;
@property(nonatomic, assign) int justify;
@property(nonatomic, assign) NSSize size;
@property(nonatomic, assign) NSPoint position;
@property(nonatomic, assign) bool visible;

- (void) Update;

@end

@implementation LabelView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

- (void)drawRect:(NSRect)$rect {
//    (void)$rect;
    if (self.visible == true) {
        NSGraphicsContext* theContext = [NSGraphicsContext currentContext];

        [theContext saveGraphicsState];

        NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        style.lineBreakMode = NSLineBreakByTruncatingTail;
        if (self.justify == 2) {
            // right
            style.alignment = NSTextAlignmentRight;
        } else if (self.justify == 1) {
            // center
            style.alignment = NSTextAlignmentCenter;
        } else {
            // left
            style.alignment = NSTextAlignmentLeft;
        }

        [self.text drawInRect:$rect withAttributes:@{
                                           NSFontAttributeName              : self.font,
                                           NSForegroundColorAttributeName   : self.color,
                                           NSParagraphStyleAttributeName    : style
                                           }];

        [theContext restoreGraphicsState];
    }
}

- (void) Update {
    if ([NSThread isMainThread]) {
        auto rect = [self frame];
        auto win = [[self window] frame];
        auto y = win.size.height - rect.size.height - self.position.y;

        [self setFrame:NSMakeRect(self.position.x, y, self.size.width, self.size.height)];
        [self setNeedsDisplay: YES];
    } else {
        [self performSelectorOnMainThread:@selector(Update) withObject:nil waitUntilDone:NO];
    }
}
@end
// @formatter:on

namespace Io::Oidis::Onion::Gui::Label {

    LabelMac::LabelMac(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent)
            : LabelBase($parent) {}

    void LabelMac::draw(void *$args) {
        (void)$args;

        // @formatter:off
        [this->nativeLabel Update];
        // @formatter:on
    }

    void *LabelMac::getNative() const {
        return this->nativeLabel;
    }

    void LabelMac::Create() {
        BaseGuiObject::Create();

        // @formatter:off
        NSRect rect = NSMakeRect(this->getX(), this->getY(), this->getWidth(), this->getHeight());
        this->nativeLabel = [[LabelView alloc] initWithFrame: rect];
//        this->nativeLabel = [[LabelView alloc] initWithFrame: reinterpret_cast<NSWindow*>(this->getParent()->getNative()).frame];
        NSString *empty = @"";
        [this->nativeLabel setText:empty];
        [this->nativeLabel setColor:reinterpret_cast<NSColor*>(this->getFont()->getColor()->getNative())];
        this->setFont(this->getFont());
        // @formatter:on
    }

    void LabelMac::setFont(const Io::Oidis::Onion::Gui::Structures::Font *$font) {
        LabelBase::setFont($font);
        // @formatter:off
        [this->nativeLabel setColor:reinterpret_cast<NSColor*>($font->getColor()->getNative())];
//        NSFontManager *fontManager = [NSFontManager sharedFontManager];
//        NSLog(@"%@",[fontManager availableFonts]);
        NSFont *ft = [NSFont fontWithName:
                      [NSString stringWithUTF8String:this->getFont()->getName().c_str()] size:this->getFont()->getSize()];
        if (ft != nil) {
            [this->nativeLabel setFont:ft];
        } else {
            throw std::runtime_error{(string("Specified font \"") + this->getFont()->getName() + "\" can not be constructed.").c_str()};
        }
        [this->nativeLabel setFont:ft];
        [this->nativeLabel Update];
        // @formatter:off
    }

    void LabelMac::setText(const string &$text) {
        LabelBase::setText($text);
        // @formatter:off
        [this->nativeLabel setText:[NSString stringWithUTF8String:$text.c_str()]];
        [this->nativeLabel Update];
        // @formatter:off
    }

    void LabelMac::setJustify(const LabelBase::TextJustify &$justify) {
        LabelBase::setJustify($justify);
        // @formatter:off
        [this->nativeLabel setJustify:$justify];
        [this->nativeLabel Update];
        // @formatter:on
    }

    void LabelMac::setSize(int $width, int $height) {
        BaseGuiObject::setSize($width, $height);
        // @formatter:off
        [this->nativeLabel setSize: NSMakeSize($width, $height)];
        [this->nativeLabel Update];
        // @formatter:on
    }

    void LabelMac::setPosition(int $x, int $y) {
        BaseGuiObject::setPosition($x, $y);
        // @formatter:off
        [this->nativeLabel setPosition:NSMakePoint($x, $y)];
        [this->nativeLabel Update];
        // @formatter:on
    }

    void LabelMac::setSizeAndPosition(int $x, int $y, int $width, int $height) {
        BaseGuiObject::setSizeAndPosition($x, $y, $width, $height);
        this->setPosition($x, $y);
        this->setSize($width, $height);
    }

    void LabelMac::setVisible(bool $visible) {
        BaseGuiObject::setVisible($visible);
        // @formatter:off
        [this->nativeLabel setVisible:$visible];
        [this->nativeLabel Update];
        // @formatter:on
    }
}

#endif  // MAC_PLATFORM
