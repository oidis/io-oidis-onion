/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_LABEL_LABELWIN_HPP_
#define IO_OIDIS_ONION_GUI_LABEL_LABELWIN_HPP_

#ifdef WIN_PLATFORM

#include "LabelBase.hpp"

namespace Io::Oidis::Onion::Gui::Label {
    class LabelWin : public LabelBase {
     public:
        using LabelBase::LabelBase;

        ~LabelWin();

        void *getNative() const override;

     protected:
        void draw(void *$args) override;
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_ONION_GUI_LABEL_LABELWIN_HPP_
