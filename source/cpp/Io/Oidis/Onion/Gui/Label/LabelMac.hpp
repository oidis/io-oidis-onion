/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_LABEL_LABELMAC_HPP_
#define IO_OIDIS_ONION_GUI_LABEL_LABELMAC_HPP_

#ifdef MAC_PLATFORM

#include "LabelBase.hpp"

@class LabelView;

namespace Io::Oidis::Onion::Gui::Label {
    class LabelMac : public LabelBase {
     public:
        explicit LabelMac(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent);

        void* getNative() const override;

        void Create() override;

        void setFont(const Io::Oidis::Onion::Gui::Structures::Font *$font) override;

        void setText(const string &$text) override;

        void setJustify(const TextJustify &$justify) override;

        void setSize(int $width, int $height) override;

        void setPosition(int $x, int $y) override;

        void setSizeAndPosition(int $x, int $y, int $width, int $height) override;

        void setVisible(bool $visible) override;

     protected:
        void draw(void *$args) override;

     private:
        LabelView *nativeLabel;
    };
}

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_ONION_GUI_LABEL_LABELMAC_HPP_
