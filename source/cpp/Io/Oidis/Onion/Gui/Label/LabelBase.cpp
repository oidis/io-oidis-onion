/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::Label {
    using Io::Oidis::Onion::Gui::Structures::Font;

    LabelBase::LabelBase(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent)
            : BaseGuiObject($parent) {
        this->font = new Font();
        this->setVisible(false);
        this->justify = TextJustify::LEFT;
    }

    const Io::Oidis::Onion::Gui::Structures::Font *LabelBase::getFont() {
        return this->font;
    }

    void LabelBase::setFont(const Io::Oidis::Onion::Gui::Structures::Font *$font) {
        this->font = const_cast<Structures::Font *>($font);
    }

    const string &LabelBase::getText() {
        return this->text;
    }

    void LabelBase::setText(const string &$text) {
        this->text = $text;
    }

    const LabelBase::TextJustify &LabelBase::getJustify() {
        return this->justify;
    }

    void LabelBase::setJustify(const LabelBase::TextJustify &$justify) {
        this->justify = $justify;
    }
}
