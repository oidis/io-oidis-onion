/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::Label {

    LabelWin::~LabelWin() {
        delete this->getFont();
    }

    void LabelWin::draw(void *$args) {
        auto graphics = static_cast<Gdiplus::Graphics *>($args);
        if (graphics == nullptr) {
            return;
        }

        std::wstring fontWstring(this->getFont()->getName().begin(), this->getFont()->getName().end());
        Gdiplus::FontFamily fontFamily(fontWstring.c_str());
        Gdiplus::Font font(&fontFamily, this->getFont()->getSize());

        auto color = static_cast<Gdiplus::Color *>(this->getFont()->getColor()->getNative());
        if (color == nullptr) {
            return;
        }

        Gdiplus::SolidBrush brush(*color);
        std::wstring text(this->getText().begin(), this->getText().end());
        graphics->DrawString(text.c_str(), -1, &font, Gdiplus::PointF(this->getX(), this->getY()), &brush);
    }

    void *LabelWin::getNative() const {
        return nullptr;
    }
}

#endif  // WIN_PLATFORM
