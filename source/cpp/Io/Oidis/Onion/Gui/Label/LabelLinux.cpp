/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#if defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::Label {

    gboolean LabelLinux::doDraw(GtkWidget *$widget, GdkEventExpose *$event, gpointer $data) {
        (void)$event;
        auto self = reinterpret_cast<LabelLinux *>($data);
        if (self) {
            if (self->isVisible()) {
                cairo_t *cr = gdk_cairo_create(gtk_widget_get_window(reinterpret_cast<GtkWidget *>($widget)));
                if (cr) {
                    auto font = self->getFont();
                    if (!font) {
                        return TRUE;
                    }

                    auto *nativeColor = static_cast<GdkColor *>(font->getColor()->getNative());
                    if (nativeColor) {
                        cairo_set_source_rgb(cr, static_cast<double>(nativeColor->red) / 255,
                                             static_cast<double>(nativeColor->green) / 255,
                                             static_cast<double>(nativeColor->blue) / 255);

                        cairo_select_font_face(cr, font->getName().c_str(),
                                               CAIRO_FONT_SLANT_NORMAL,
                                               CAIRO_FONT_WEIGHT_NORMAL);

                        cairo_set_font_size(cr, font->getSize());

                        cairo_move_to(cr, self->getX(), self->getY());
                        cairo_show_text(cr, self->getText().c_str());
                    }
                    cairo_destroy(cr);
                }
            }
        }
        return TRUE;
    }

    LabelLinux::~LabelLinux() {
        delete this->getFont();
    }

    void LabelLinux::draw(void *$args) {
        (void)$args;
    }

    void *LabelLinux::getNative() const {
        return nullptr;
    }
}

#endif  // LINUX_PLATFORM
