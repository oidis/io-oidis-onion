/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::Primitives {

    BaseGuiObject::BaseGuiObject(const BaseGuiObject *$parent)
            : x(0),
              y(0),
              width(0),
              height(0),
              visible(false),
              parent($parent) {}

    void BaseGuiObject::setSize(int $width, int $height) {
        this->width = $width;
        this->height = $height;
    }

    void BaseGuiObject::setPosition(int $x, int $y) {
        this->x = $x;
        this->y = $y;
    }

    void BaseGuiObject::setVisible(bool $visible) {
        this->visible = $visible;
    }

    void BaseGuiObject::setSizeAndPosition(int $x, int $y, int $width, int $height) {
        this->setPosition($x, $y);
        this->setSize($width, $height);
    }

    const BaseGuiObject *BaseGuiObject::getParent() const {
        return this->parent;
    }

    void *BaseGuiObject::getNative() const {
        return nullptr;
    }

    void BaseGuiObject::Create() {}
}
