/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_PRIMITIVES_BASEGUIOBJECT_HPP_
#define IO_OIDIS_ONION_GUI_PRIMITIVES_BASEGUIOBJECT_HPP_

namespace Io::Oidis::Onion::Gui::Primitives {
    /**
     * This class defines base class for each user controls classes.
     */
    class BaseGuiObject {
     public:
        /**
         * Initialize BaseGuiObject.
         * @param $parent Parent object.
         */
        explicit BaseGuiObject(const BaseGuiObject* $parent);

        int getWidth() const { return this->width; }

        int getHeight() const { return this->height; }

        virtual void setSize(int $width, int $height);

        int getX() const { return this->x; }

        virtual int getY() const { return this->y; }

        virtual void setPosition(int $x, int $y);

        virtual void setSizeAndPosition(int $x, int $y, int $width, int $height);

        bool isVisible() const { return this->visible; }

        virtual void setVisible(bool $visible);

        const BaseGuiObject* getParent() const;

        virtual void* getNative() const;

        virtual void Create();

     private:
        int x;
        int y;
        int width;
        int height;
        bool visible = true;
        const BaseGuiObject *parent;
    };
}

#endif  // IO_OIDIS_ONION_GUI_PRIMITIVES_BASEGUIOBJECT_HPP_
