/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <future>

#include "../../../../../reference.hpp"

#ifdef MAC_PLATFORM

#include <AppKit/NSApplication.h>
#include <Foundation/NSAutoreleasePool.h>

#elif defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"

#include <gtk/gtk.h>

#pragma GCC diagnostic pop

#include <X11/Xlib.h>

#elif WIN_PLATFORM

#include <windows.h>
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4458)  // declaration of 'xxx' hides class member
#endif
#include <gdiplus.h>
#ifdef _MSC_VER
#pragma warning(pop)
#endif
#endif

namespace Io::Oidis::Onion::Gui::Environment {
    void *Handler::platformDependentData = nullptr;

    void Handler::Init() {
#ifdef MAC_PLATFORM
        [NSApplication sharedApplication];
#elif defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)
        Handler::platformDependentData = new bool;
        bool initSuccessful = true;
        if (!gtk_init_check(nullptr, nullptr)) {
            initSuccessful = false;
        } else {
            XOpenDisplay(nullptr);
        }
        std::memcpy(Handler::platformDependentData, &initSuccessful, sizeof(bool));
#elif WIN_PLATFORM
        Handler::platformDependentData = new ULONG_PTR;
        Gdiplus::GdiplusStartupInput gdiplusStartupInput;
        GdiplusStartup(reinterpret_cast<ULONG_PTR *>(Handler::platformDependentData), &gdiplusStartupInput, nullptr);
#endif
    }

    int Handler::Start(const std::function<int()> &$action) {
        std::future<int> future;
        if ($action != nullptr) {
            future = std::async(std::launch::async, $action);
        }

#ifdef MAC_PLATFORM
        [NSApp run];
#endif
        if ($action != nullptr) {
            return future.get();
        }
        return 0;
    }

    void Handler::Stop() {
#ifdef MAC_PLATFORM
        [NSApp stop:nil];
#elif defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)
        if (gtk_main_level() > 0) {
            gtk_main_quit();
        }
        delete static_cast<bool *>(Handler::platformDependentData);
#elif WIN_PLATFORM
        Gdiplus::GdiplusShutdown(*(reinterpret_cast<ULONG_PTR *>(Handler::platformDependentData)));
#endif
    }

#ifdef LINUX_PLATFORM

    bool Handler::IsInitSuccessful() {
        if (!Handler::platformDependentData) {
            return false;
        }
        bool tmp = false;
        std::memcpy(&tmp, Handler::platformDependentData, sizeof(bool));
        return tmp;
    }

#endif
}
