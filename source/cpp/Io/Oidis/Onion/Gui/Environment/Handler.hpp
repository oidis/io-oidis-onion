/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_ENVIRONMENT_HANDLER_HPP_
#define IO_OIDIS_ONION_GUI_ENVIRONMENT_HANDLER_HPP_

#include <functional>

namespace Io::Oidis::Onion::Gui::Environment {
    class Handler {
     public:
        /**
         * Initialize Environment in which GUI elements are supposed to run.
         */
        static void Init();

        /**
         * Start environment and execute user-action function.
         * @param $action Action to execute after environment is ready.
         * @return True if successful.
         */
        static int Start(const std::function<int()> &$action = nullptr);

        /**
         * Stop and teardown environment.
         */
        static void Stop();

#ifdef LINUX_PLATFORM

        static bool IsInitSuccessful();

#endif

     private:
        static void *platformDependentData;
    };
}

#endif  // IO_OIDIS_ONION_GUI_ENVIRONMENT_HANDLER_HPP_
