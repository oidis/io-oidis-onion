/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#if defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#include "../../../../../reference.hpp"

using std::string;

namespace Io::Oidis::Onion::Gui::ProgressBar {

    gboolean ProgressBarLinux::doDraw(GtkWidget *$widget, GdkEventExpose *$event, gpointer $data) {
        (void)$event;
        auto self = reinterpret_cast<ProgressBarLinux *>($data);

        if (self) {
            if (self->isVisible()) {
                if (!(self->bgImage && self->bgImage->isVisible() && self->fgImage && self->fgImage->isVisible())) {
                    auto *nativeBgColor = static_cast<GdkColor *>(self->bgColor->getNative());
                    auto *nativeFgColor = static_cast<GdkColor *>(self->fgColor->getNative());

                    if (nativeBgColor && nativeFgColor) {
                        cairo_t *cr = gdk_cairo_create(gtk_widget_get_window($widget));
                        if (!cr) {
                            return TRUE;
                        }

                        cairo_set_source_rgb(cr, static_cast<double>(nativeBgColor->red) / 255,
                                             static_cast<double>(nativeBgColor->green) / 255,
                                             static_cast<double>(nativeBgColor->blue) / 255);
                        cairo_rectangle(cr, self->getX(), self->getY(),
                                        self->getWidth(), self->getHeight());
                        cairo_fill(cr);

                        int tmpWidth = self->getRatio() * self->getWidth();

                        cairo_set_source_rgb(cr, static_cast<double>(nativeFgColor->red) / 255,
                                             static_cast<double>(nativeFgColor->green) / 255,
                                             static_cast<double>(nativeFgColor->blue) / 255);
                        cairo_rectangle(cr, self->getX(), self->getY(),
                                        tmpWidth, self->getHeight());
                        cairo_fill(cr);

                        cairo_destroy(cr);
                    }
                }
            }
        }
        return TRUE;
    }

    void ProgressBarLinux::draw(void *$gtkWidgetWindow) {
        (void)$gtkWidgetWindow;
    }

    void ProgressBarLinux::setBackground(const std::string &$pathOrColor) {
        try {
            ProgressBarBase::setBackground($pathOrColor);
        } catch (Gui::Structures::ColorBase::CastException &ex) {
            // parameter is not a color, on linux progress bar image is not supported, so override with default color
            this->bgColor = Io::Oidis::Onion::Gui::Structures::ColorFactory::Create("#000000");
        } catch (std::invalid_argument &ex) {
            this->bgColor = Io::Oidis::Onion::Gui::Structures::ColorFactory::Create("#000000");
        } catch (std::out_of_range &ex) {
            this->bgColor = Io::Oidis::Onion::Gui::Structures::ColorFactory::Create("#000000");
        }
    }

    void ProgressBarLinux::setForeground(const std::string &$pathOrColor) {
        try {
            ProgressBarBase::setForeground($pathOrColor);
        } catch (Gui::Structures::ColorBase::CastException &ex) {
            // parameter is not a color, on linux progress bar image is not supported, so override with default color
            this->fgColor = Io::Oidis::Onion::Gui::Structures::ColorFactory::Create("#ff007f7f");
        } catch (std::invalid_argument &ex) {
            this->fgColor = Io::Oidis::Onion::Gui::Structures::ColorFactory::Create("#ff007f7f");
        } catch (std::out_of_range &ex) {
            this->fgColor = Io::Oidis::Onion::Gui::Structures::ColorFactory::Create("#ff007f7f");
        }
    }

    Image::ImageBase *ProgressBarLinux::getBgImage() const {
        return this->bgImage;
    }

    Image::ImageBase *ProgressBarLinux::getFgImage() const {
        return this->fgImage;
    }
}

#endif  // LINUX_PLATFORM
