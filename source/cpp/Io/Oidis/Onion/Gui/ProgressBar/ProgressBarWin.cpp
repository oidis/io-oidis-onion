/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../../../../../reference.hpp"

using std::string;

namespace Io::Oidis::Onion::Gui::ProgressBar {

    ProgressBarWin::ProgressBarWin(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent)
            : ProgressBarBase($parent) {}

    void ProgressBarWin::draw(void *$gdiGraphics) {
        auto graphics = static_cast<Gdiplus::Graphics *>($gdiGraphics);

        if (this->isVisible() && (graphics != nullptr)) {
            if ((this->bgImage != nullptr) && this->bgImage->isVisible()) {
                this->bgImage->Draw(graphics);
            } else if (this->bgColor != nullptr) {
                auto color = static_cast<Gdiplus::Color *>(this->bgColor->getNative());
                if (color != nullptr) {
                    Gdiplus::SolidBrush brush(*color);
                    graphics->FillRectangle(&brush, this->getX(), this->getY(), this->getWidth(), this->getHeight());
                }
            }

            int tmpWidth = static_cast<int>(ProgressBarBase::getRatio() * static_cast<float>(this->getWidth()));

            if ((this->fgImage != nullptr) && this->fgImage->isVisible()) {
                if (!this->marquee) {
                    this->fgImage->setSize(tmpWidth, this->fgImage->getHeight());
                }
                this->fgImage->Draw(graphics);
            } else if (this->fgColor != nullptr) {
                auto color = static_cast<Gdiplus::Color *>(this->fgColor->getNative());
                if (color != nullptr) {
                    Gdiplus::SolidBrush brush(*color);
                    graphics->FillRectangle(&brush, this->getX(), this->getY(), tmpWidth, this->getHeight());
                }
            }
        }
    }

    void ProgressBarWin::setBackground(const std::string &$pathOrColor) {
        ProgressBarBase::setBackground($pathOrColor);
        // if bg image is not yet set, try to set bg color
        if (this->bgImage == nullptr) {
            this->bgColor = new Structures::ColorWin();
            this->bgColor->FromString(this->backgroundPathOrColor);
        }
    }

    void ProgressBarWin::setForeground(const std::string &$pathOrColor) {
        ProgressBarBase::setForeground($pathOrColor);
        // if fg image is not yet set, try to set bg color
        if (this->fgImage == nullptr) {
            this->fgColor = new Structures::ColorWin();
            this->fgColor->FromString(this->foregroundPathOrColor);
        }
    }

    void ProgressBarWin::Create() {
        ProgressBarBase::Create();

        if (this->bgColor == nullptr) {
            this->bgImage->setVisible(true);
        }

        if (this->fgColor == nullptr) {
            this->fgImage->setVisible(true);
        }
    }
}

#endif
