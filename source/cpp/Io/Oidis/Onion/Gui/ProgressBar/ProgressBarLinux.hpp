/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARLINUX_HPP_
#define IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARLINUX_HPP_

#if defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"

#include <gtk/gtk.h>

#pragma GCC diagnostic pop

#include <string>

#include "ProgressBarBase.hpp"
#include "../Image/ImageBase.hpp"
#include "../Structures/ColorBase.hpp"

namespace Io::Oidis::Onion::Gui::ProgressBar {
    class ProgressBarLinux : public ProgressBarBase {
     public:
        using ProgressBarBase::ProgressBarBase;

        static gboolean doDraw(GtkWidget *$widget, GdkEventExpose *$event, gpointer $data);

        void setBackground(const std::string &$pathOrColor) override;

        void setForeground(const std::string &$pathOrColor) override;

        Image::ImageBase *getFgImage() const;

        Image::ImageBase *getBgImage() const;

     protected:
        void draw(void *$gtkWidgetWindow) override;
    };
}

#endif  // LINUX_PLATFORM
#endif  // IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARLINUX_HPP_
