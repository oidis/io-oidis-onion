/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARWIN_HPP_
#define IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARWIN_HPP_

#ifdef WIN_PLATFORM

#include <objidl.h>
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4458)  // declaration of 'xxx' hides class member
#endif
#include <gdiplus.h>
#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <string>

#include "ProgressBarBase.hpp"
#include "../Image/ImageBase.hpp"

namespace Io::Oidis::Onion::Gui::ProgressBar {
    /**
     * ProgressBarBase class provides GUI controls which shows horizontal progress with support of marquee style.
     */
    class ProgressBarWin : public ProgressBarBase {
     public:
        explicit ProgressBarWin(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent);

        void setBackground(const std::string &$pathOrColor) override;

        void setForeground(const std::string &$pathOrColor) override;

        void Create() override;

     private:
        void draw(void *$gdiGraphics) override;
    };
}

#endif  // WIN_PLATFORM
#endif  // IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARWIN_HPP_
