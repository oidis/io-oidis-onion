/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARBASE_HPP_
#define IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARBASE_HPP_

#include "../Image/ImageBase.hpp"
#include "../Primitives/BaseGuiObject.hpp"
#include "../Structures/ColorBase.hpp"

namespace Io::Oidis::Onion::Gui::ProgressBar {
    /**
     * ProgressBar class provides GUI controls which shows horizontal progress with support of marquee style.
     */
    class ProgressBarBase : public Gui::Primitives::BaseGuiObject {
     public:
        struct ProgressBarSettings {
         public:
            int x = 0;
            int y = 0;
            int width = 0;
            int height = 0;
            string foreground = "#000000";
            string background = "#ffffff";
            bool visible = false;
            bool isMarquee = false;
        };

        /**
         * Constructs default ProgressBar.
         */
        explicit ProgressBarBase(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent);

        virtual ~ProgressBarBase();

        virtual const std::string &getBackground() const {
            return this->backgroundPathOrColor;
        }

        virtual void setBackground(const std::string &$pathOrColor);

        virtual const std::string &getForeground() const {
            return this->foregroundPathOrColor;
        }

        virtual void setForeground(const std::string &$pathOrColor);

        /**
         * @return Returns true if marquee progress style is selected, false otherwise.
         */
        virtual bool isMarquee() const;

        /**
         * @param $marquee Specify true to select marquee progress style, false otherwise.
         */
        virtual void setMarquee(bool $marquee);

        /**
         * @return Returns progress minimal value.
         */
        virtual int getMin() const;

        /**
         * @param $min Specify progress minimal value.
         */
        virtual void setMin(int $min);

        /**
         * @return Returns progress maximal value.
         */
        virtual int getMax() const;

        /**
         * @param $max Specify progress maximal value.
         */
        virtual void setMax(int $max);

        /**
         * @return Returns actual progress value.
         */
        virtual int getValue() const;

        /**
         * @param $value Specify actual progress value.
         */
        virtual void setValue(int $value);

        /**
         * @return Returns true if tray-progress is attached to ProgressBar, false otherwise.
         */
        virtual bool isTrayProgress() const;

        /**
         * @param $trayProgress Specify true to attach tray-progress to ProgressBar, false otherwise.
         */
        virtual void setTrayProgress(bool $trayProgress);

        /**
         * Calls platform dependent draw function, with different data for different platform.
         * @param $args
         */
        virtual void Draw(void *$args) {
            this->draw($args);
        }

        virtual float getRatio();

     protected:
        virtual void draw(void *$args) = 0;

        std::string backgroundPathOrColor;
        Gui::Structures::ColorBase *bgColor = nullptr;
        Gui::Image::ImageBase *bgImage = nullptr;

        std::string foregroundPathOrColor;
        Gui::Structures::ColorBase *fgColor = nullptr;
        Gui::Image::ImageBase *fgImage = nullptr;

        bool marquee = false;
        bool trayProgress = false;
        int min = 0;
        int max = 100;
        int value = 0;
    };
}

#endif  // IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARBASE_HPP_
