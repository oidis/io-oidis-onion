/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::ProgressBar {

    ProgressBarBase::ProgressBarBase(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent)
            : BaseGuiObject($parent) {
        this->bgImage = Image::ImageFactory::Create(this);
        this->fgImage = Image::ImageFactory::Create(this);
    }

    ProgressBarBase::~ProgressBarBase() {
        delete this->bgImage;
        delete this->fgImage;
        delete this->bgColor;
        delete this->fgColor;
    }

    bool ProgressBarBase::isMarquee() const {
        return this->marquee;
    }

    void ProgressBarBase::setMarquee(bool $marquee) {
        this->marquee = $marquee;
    }

    int ProgressBarBase::getMin() const {
        return this->min;
    }

    void ProgressBarBase::setMin(int $min) {
        this->min = $min;
    }

    int ProgressBarBase::getMax() const {
        return this->max;
    }

    void ProgressBarBase::setMax(int $max) {
        this->max = $max;
    }

    int ProgressBarBase::getValue() const {
        return this->value;
    }

    void ProgressBarBase::setValue(int $value) {
        this->value = $value;
    }

    bool ProgressBarBase::isTrayProgress() const {
        return this->trayProgress;
    }

    void ProgressBarBase::setTrayProgress(bool $trayProgress) {
        this->trayProgress = $trayProgress;
    }

    void ProgressBarBase::setBackground(const std::string &$pathOrColor) {
        this->bgColor = nullptr;

        this->backgroundPathOrColor = $pathOrColor;
        if (!((!$pathOrColor.empty() && $pathOrColor[0] == '#') ||
              ($pathOrColor.size() >= 2 && $pathOrColor[0] == '0' && $pathOrColor[1] == 'x'))) {
            if (Onion::Filesystem::Exists(this->backgroundPathOrColor)) {
                this->bgImage->Load(this->backgroundPathOrColor);
                this->bgImage->setPosition(this->getX(), this->getY());
                this->bgImage->setSize(this->getWidth(), this->getHeight());
                this->bgImage->setVisible(this->isVisible());
                return;
            } else {
                this->backgroundPathOrColor = "0x00ffffff";
                this->bgImage->setVisible(false);
            }
        } else {
            this->bgImage->setVisible(false);
        }
        this->bgColor = Io::Oidis::Onion::Gui::Structures::ColorFactory::Create(this->backgroundPathOrColor);
    }

    void ProgressBarBase::setForeground(const std::string &$pathOrColor) {
        this->fgColor = nullptr;

        this->foregroundPathOrColor = $pathOrColor;
        if (!((!$pathOrColor.empty() && $pathOrColor[0] == '#') ||
              ($pathOrColor.size() >= 2 && $pathOrColor[0] == '0' && $pathOrColor[1] == 'x'))) {
            if (Onion::Filesystem::Exists(this->foregroundPathOrColor)) {
                this->fgImage->Load(this->foregroundPathOrColor);
                this->fgImage->setPosition(this->getX(), this->getY());
                this->fgImage->setSize(this->getWidth(), this->getHeight());
                this->fgImage->setVisible(this->isVisible());
                return;
            } else {
                this->foregroundPathOrColor = "0x00ffffff";
                this->fgImage->setVisible(false);
            }
        } else {
            this->fgImage->setVisible(false);
        }
        this->fgColor = Io::Oidis::Onion::Gui::Structures::ColorFactory::Create(this->foregroundPathOrColor);
    }

    float ProgressBarBase::getRatio() {
        return static_cast<float>(this->getValue()) / static_cast<float>(this->getMax() - this->getMin());
    }
}
