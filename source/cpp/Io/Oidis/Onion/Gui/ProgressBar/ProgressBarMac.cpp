/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include <Cocoa/Cocoa.h>

#include "../../../../../reference.hpp"
#include "ProgressBarMac.hpp"

// @formatter:off
@interface ProgressBarView : NSView {}

@property(atomic, strong) NSColor *bgColor;
@property(atomic, strong) NSColor *fgColor;
@property(atomic, assign) NSPoint position;
@property(atomic, assign) bool visible;
@property(atomic, assign) bool visibleBg;
@property(atomic, assign) bool visibleFg;
@property(atomic, assign) bool isMarquee;
@property(atomic, assign) float value;

- (void) Update;

@end

@implementation ProgressBarView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    [self setValue:0.0];
    return self;
}

- (void)drawRect:(NSRect)$rect {
    if (self.visible == true) {
        NSGraphicsContext* theContext = [NSGraphicsContext currentContext];

        [theContext saveGraphicsState];

        if (self.visibleBg == true) {
            [self.bgColor set];
            NSRectFill($rect);
        }

        if (self.visibleFg == true) {
            [self.fgColor set];
            NSRectFill(NSMakeRect($rect.origin.x, $rect.origin.y, $rect.size.width * self.value, $rect.size.height));
        }

        [theContext restoreGraphicsState];
    }
}

- (void) Update {
    if ([NSThread isMainThread]) {
        [self setNeedsDisplay: YES];
    } else {
        [self performSelectorOnMainThread:@selector(Update) withObject:nil waitUntilDone:NO];
    }
}
@end
// @formatter:on

namespace Io::Oidis::Onion::Gui::ProgressBar {

    ProgressBarMac::ProgressBarMac(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent)
            : ProgressBarBase($parent) {}

    void *ProgressBarMac::getNative() const {
        return this->nativeBar;
    }

    void ProgressBarMac::setSize(int $width, int $height) {
        BaseGuiObject::setSize($width, $height);
        this->bgImage->setSize($width, $height);
        this->fgImage->setSize($width, $height);
        // @formatter:off
        [this->nativeBar setFrameSize:NSMakeSize($width, $height)];
        [this->nativeBar Update];
        // @formatter:on
    }

    void ProgressBarMac::setPosition(int $x, int $y) {
        BaseGuiObject::setPosition($x, $y);

        this->bgImage->setPosition($x, this->getY());
        this->fgImage->setPosition($x, this->getY());
        // @formatter:off
        [this->nativeBar setFrameOrigin:NSMakePoint($x, this->getY())];
        [this->nativeBar setPosition:NSMakePoint($x, this->getY())];
        [this->nativeBar Update];
        // @formatter:on
    }

    void ProgressBarMac::setSizeAndPosition(int $x, int $y, int $width, int $height) {
        BaseGuiObject::setSizeAndPosition($x, $y, $width, $height);
        this->setPosition($x, $y);
        this->setSize($width, $height);
    }

    void ProgressBarMac::setVisible(bool $visible) {
        BaseGuiObject::setVisible($visible);
        // @formatter:off
        [this->nativeBar setVisible:$visible];
        [this->nativeBar Update];
        // @formatter:on
        this->bgImage->setVisible($visible);
        this->fgImage->setVisible($visible);
    }

    void ProgressBarMac::setValue(int $value) {
        ProgressBarBase::setValue($value);
        // @formatter:off
        [this->nativeBar setValue: this->getRatio()];
        [this->nativeBar Update];
        // @formatter:on
        this->fgImage->setSize(static_cast<int>(static_cast<float>(this->getWidth()) * this->getRatio()), this->getHeight());
    }

    void ProgressBarMac::setBackground(const std::string &$pathOrColor) {
        ProgressBarBase::setBackground($pathOrColor);
        if (this->bgColor != nullptr) {
            this->bgImage->setVisible(false);
            // @formatter:off
            [this->nativeBar setVisibleBg:true];
            [this->nativeBar setBgColor:reinterpret_cast<NSColor*>(this->bgColor->getNative())];
            [this->nativeBar Update];
            // @formatter:on
        } else {
            this->bgImage->setVisible(true);
            // @formatter:off
            [this->nativeBar setVisibleBg:false];
            [this->nativeBar Update];
            // @formatter:on
        }
    }

    void ProgressBarMac::setForeground(const std::string &$pathOrColor) {
        ProgressBarBase::setForeground($pathOrColor);
        if (this->fgColor != nullptr) {
            this->fgImage->setVisible(false);
            // @formatter:off
            [this->nativeBar setVisibleFg:true];
            [this->nativeBar setFgColor:reinterpret_cast<NSColor*>(this->fgColor->getNative())];
            [this->nativeBar Update];
            // @formatter:on
        } else {
            this->fgImage->setVisible(true);
            // @formatter:off
            [this->nativeBar setVisibleFg:false];
            [this->nativeBar Update];
            // @formatter:on
        }
    }

    void ProgressBarMac::Create() {
        BaseGuiObject::Create();

        NSRect rect = NSMakeRect(this->getX(), this->getY(), this->getWidth(), this->getHeight());
        // @formatter:off
        this->nativeBar = [[ProgressBarView alloc] initWithFrame: rect];
        // @formatter:on

        this->bgImage->Create();
        this->fgImage->Create();
        this->fgImage->setSize(0, 0);

        // @formatter:off
        [reinterpret_cast<NSWindow *>(this->getParent()->getNative()).contentView
            addSubview:reinterpret_cast<NSView *>(this->bgImage->getNative())];
        [reinterpret_cast<NSWindow *>(this->getParent()->getNative()).contentView
            addSubview:reinterpret_cast<NSView *>(this->fgImage->getNative())];
        // @formatter:on

        this->setBackground("#000000");
        this->setForeground("#007f7f");
    }

    void ProgressBarMac::draw(void *$args) {
        (void)$args;

        // @formatter:off
        [this->nativeBar Update];
        // @formatter:on

        this->bgImage->Draw(nullptr);
        this->fgImage->Draw(nullptr);
    }

    int ProgressBarMac::getY() const {
        int oldY = BaseGuiObject::getY();

        auto win = [[this->nativeBar window] frame];
        auto y = win.size.height - this->getHeight() - oldY;
        return y;
    }
}

#endif  // MAC_PLATFORM
