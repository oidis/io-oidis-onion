/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::ProgressBar::ProgressBarFactory {

    ProgressBarBase *Create(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent) {
#if defined(MAC_PLATFORM) || defined(__APPLE__)
        return new ProgressBarMac($parent);
#elif (defined(LINUX_PLATFORM) || defined(__linux__)) && !defined(ONION_NO_GUI)  // NOLINT(whitespace/parens)
        return new ProgressBarLinux($parent);
#elif defined(WIN_PLATFORM) || defined(_WIN32)
        return new ProgressBarWin($parent);
#else
        return nullptr;
#endif
    }
}
