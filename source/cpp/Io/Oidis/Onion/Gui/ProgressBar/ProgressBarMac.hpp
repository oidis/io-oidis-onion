/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARMAC_HPP_
#define IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARMAC_HPP_

#ifdef MAC_PLATFORM

#include "ProgressBarBase.hpp"
#include "../Image/ImageBase.hpp"

@class ProgressBarView;

namespace Io::Oidis::Onion::Gui::ProgressBar {
    class ProgressBarMac : public ProgressBarBase {
     public:
        explicit ProgressBarMac(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent);

        void setSize(int $width, int $height) override;

        void setPosition(int $x, int $y) override;

        void setSizeAndPosition(int $x, int $y, int $width, int $height) override;

        void setVisible(bool $visible) override;

        void Create() override;

        void setBackground(const std::string &$pathOrColor) override;

        void setForeground(const std::string &$pathOrColor) override;

        void *getNative() const override;

        void setValue(int $value) override;

        int getY() const override;

     protected:
        void draw(void *$args) override;

     private:
        ProgressBarView *nativeBar = nullptr;
    };
}

#endif  // MAC_PLATFORM
#endif  // IO_OIDIS_ONION_GUI_PROGRESSBAR_PROGRESSBARMAC_HPP_
