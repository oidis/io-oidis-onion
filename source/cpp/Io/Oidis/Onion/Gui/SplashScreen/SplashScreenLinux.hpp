/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENLINUX_HPP_
#define IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENLINUX_HPP_

#if (defined(LINUX_PLATFORM) || defined(__linux__)) && !defined(ONION_NO_GUI)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"

#include <gtk/gtk.h>

#pragma GCC diagnostic pop

#include "SplashScreenBase.hpp"

namespace Io::Oidis::Onion::Gui::SplashScreen {
    class SplashScreenLinux : public SplashScreenBase {
     public:
        explicit SplashScreenLinux(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent,
                                   const SplashScreenBase::SplashScreenSettings &$settings);

        ~SplashScreenLinux();

        void Show() override;

        void Hide() override;

        void Update() override;

        void Create() override;

     private:
        GtkWidget *window = nullptr;
        GtkWidget *img = nullptr;
        bool callbackAdded = false;
    };
}

#endif  // defined(LINUX_PLATFORM) || defined(__linux__)

#endif  // IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENLINUX_HPP_
