/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENBASE_HPP_
#define IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENBASE_HPP_

#include "../ProgressBar/ProgressBarBase.hpp"
#include "../Image/ImageBase.hpp"
#include "../Primitives/BaseGuiObject.hpp"
#include "../Label/LabelBase.hpp"

namespace Io::Oidis::Onion::Gui::SplashScreen {
    class SplashScreenBase : public Gui::Primitives::BaseGuiObject {
     public:
        struct SplashScreenSettings {
            int x = 0;
            int y = 0;
            int width = 0;
            int height = 0;
            std::string imagePath;
        };

        /**
         * Initialize base class for SplashScreen.
         * @param $parent Parent object of splashscreen.
         * @param $settings Settings of splashscreen.
         */
        explicit SplashScreenBase(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent, const SplashScreenSettings &$settings);

        /**
         * Release internal data.
         */
        virtual ~SplashScreenBase();

        /**
         * Intended for override on platform level. Each platform will use different way of showing the image.
         */
        virtual void Show() = 0;

        /**
         * Intended for override on platform level. Each platform will use different way of hiding the image.
         */
        virtual void Hide() = 0;

        /**
         * Intended for override on platform level. Each platform will use different way of updating the image.
         */
        virtual void Update() {}

        /**
         * Sets callback which is called after splash screen is opened.
         * @param $openCallback Callback to call after open.
         */
        virtual void setOnOpenCallback(const std::function<void()> &$openCallback);

        /**
         * Sets callback which is called after splash screen is closed.
         * @param $openCallback Callback to call after close.
         */
        virtual void setOnCloseCallback(const std::function<void()> &$closeCallback);

        /**
         * Adds progress label to splashscreen.
         * @param $settings Creates progress label based on these settings.
         */
        virtual void setProgressLabel(const Io::Oidis::Onion::Gui::Label::LabelBase::LabelSettings &$settings);

        /**
         * @param $settings Set progress label text based on this string.
         */
        virtual void setProgressLabelText(const string &$text);

        /**
         * @param $settings Adds status label created from $settings to splash screen.
         */
        virtual void setStatusLabel(const Io::Oidis::Onion::Gui::Label::LabelBase::LabelSettings &$settings);

        /**
         * @param $text Sets status label text.
         */
        virtual void setStatusLabelText(const string &$text);

        /**
         * Adds progress bar to splash screen.
         * @param $settings Setting to use when creating a progress bar.
         */
        virtual void setProgressBar(const Io::Oidis::Onion::Gui::ProgressBar::ProgressBarBase::ProgressBarSettings &$settings);

        /**
         * Update progress bar value.
         * @param $value Value of progress bar.
         */
        virtual void setProgressBarValue(int $value);

        virtual void invokeOpenCallback() const;

        virtual void invokeCloseCallback() const;

        virtual const SplashScreenSettings &getSettings() const;

        virtual void setSettings(const SplashScreenSettings &$settings);

        Gui::ProgressBar::ProgressBarBase *progressBar = nullptr;
        Gui::Image::ImageBase *bgImage = nullptr;
        Gui::Label::LabelBase *lbStatus = nullptr;
        Gui::Label::LabelBase *lbProgress = nullptr;

     protected:
        void checkSizeAndPosition(bool $finalValues, const std::function<void()> &$setToCenterCallback = nullptr);

     private:
        SplashScreenSettings settings = {};
        std::function<void()> openCallback = nullptr;
        std::function<void()> closeCallback = nullptr;

        void setLabel(Gui::Label::LabelBase *$label, const Io::Oidis::Onion::Gui::Label::LabelBase::LabelSettings &$settings);
    };
}

#endif  // IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENBASE_HPP_
