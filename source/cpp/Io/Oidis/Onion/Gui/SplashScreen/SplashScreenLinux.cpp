/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#if defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::SplashScreen {
    // this is a callback for expose-event, which draw all the elements
    void exposeEvent(GtkWidget *$widget, GdkEventExpose *$event, gpointer $data) {
        auto splashScreen = reinterpret_cast<SplashScreenLinux *>($data);

        Gui::ProgressBar::ProgressBarLinux::doDraw($widget, $event, splashScreen->progressBar);
        Gui::Label::LabelLinux::doDraw($widget, $event, splashScreen->lbStatus);
        Gui::Label::LabelLinux::doDraw($widget, $event, splashScreen->lbProgress);

        if (splashScreen->progressBar) {
            auto bgImage = dynamic_cast<Gui::ProgressBar::ProgressBarLinux *>(splashScreen->progressBar)->getBgImage();
            if (bgImage && bgImage->isVisible()) {
                Gui::Image::ImageLinux::doDraw($widget, $event, bgImage);
            }

            auto fgImage = dynamic_cast<Gui::ProgressBar::ProgressBarLinux *>(splashScreen->progressBar)->getFgImage();
            if (fgImage && fgImage->isVisible()) {
                fgImage->setScale(splashScreen->progressBar->getRatio());
                Gui::Image::ImageLinux::doDraw($widget, $event, fgImage);
            }
        }
    }

    SplashScreenLinux::SplashScreenLinux(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent,
                                         const SplashScreenBase::SplashScreenSettings &$settings)
            : SplashScreenBase($parent, $settings) {
        this->checkSizeAndPosition(false);

        this->window = gtk_window_new(GTK_WINDOW_POPUP);

        if (!this->window) {
            throw std::runtime_error{"Failed to allocate the gtk_widget window object"};
        }

        gtk_window_set_default_size(GTK_WINDOW(this->window), this->getWidth(), this->getHeight());
        gtk_window_set_decorated(GTK_WINDOW(this->window), FALSE);

        this->img = gtk_image_new_from_file($settings.imagePath.c_str());
        if (this->img == nullptr) {
            throw std::runtime_error{"Failed to allocate the gtk_widget img object"};
        }

        gtk_container_add(GTK_CONTAINER(this->window), this->img);
        gtk_widget_set_app_paintable(this->img, TRUE);
    }

    SplashScreenLinux::~SplashScreenLinux() {
        gtk_widget_destroy(this->img);
        gtk_widget_destroy(this->window);
    }

    void SplashScreenLinux::Show() {
        gtk_widget_show_all(this->window);
        this->Update();
        SplashScreenBase::invokeOpenCallback();
    }

    void SplashScreenLinux::Hide() {
        gtk_widget_hide_all(this->window);
        SplashScreenBase::invokeCloseCallback();
    }

    void SplashScreenLinux::Update() {
        if (!this->callbackAdded) {
            gtk_signal_connect_after(GTK_OBJECT(this->img), "expose-event",
                                     G_CALLBACK(exposeEvent),
                                     reinterpret_cast<gpointer>(this));
            this->callbackAdded = true;
        }
        gtk_window_move(GTK_WINDOW(this->window), this->getX(), this->getY());
        gtk_window_resize(GTK_WINDOW(this->window), this->getWidth(), this->getHeight());

        gtk_widget_queue_draw(this->window);

        // here Draw() functions are not called, because the drawing is called from the exposeEvent callback
        // which is called by gtk_widget_queue_draw(this->window)

        while (gtk_events_pending()) {
            gtk_main_iteration();
        }
    }

    void SplashScreenLinux::Create() {
        BaseGuiObject::Create();

        this->checkSizeAndPosition(true, [this]() {
            GdkScreen *screen = gdk_screen_get_default();
            if (screen) {
                gint primaryMonitor = gdk_screen_get_primary_monitor(screen);

                GdkRectangle monitorSize = {};
                gdk_screen_get_monitor_geometry(screen, primaryMonitor, &monitorSize);

                int tmpW = monitorSize.width / 2;
                tmpW -= this->getWidth() / 2;
                int tmpH = monitorSize.height / 2;
                tmpH -= this->getHeight() / 2;
                this->setPosition(tmpW, tmpH);
            } else {
                this->setPosition(0, 0);
            }
        });

        gtk_window_set_default_size(GTK_WINDOW(this->window), this->getWidth(), this->getHeight());

        if (this->window == nullptr) {
            throw std::runtime_error{"Failed to allocate the gtk_widget window object"};
        }

        gtk_container_remove(GTK_CONTAINER(this->window), this->img);

        this->img = gtk_image_new_from_file(this->getSettings().imagePath.c_str());
        if (this->img == nullptr) {
            throw std::runtime_error{"Failed to allocate the gtk_widget img object"};
        }

        gtk_container_add(GTK_CONTAINER(this->window), this->img);
        gtk_widget_set_app_paintable(this->img, TRUE);

        this->lbStatus->Create();
        this->lbProgress->Create();
    }
}

#endif  // LINUX_PLATFORM
