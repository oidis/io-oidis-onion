/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#ifdef UNICODE
#define OVERRIDE_UNICODE 1
#pragma push_macro("UNICODE")
#undef UNICODE
#endif

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::SplashScreen {
    bool workerCreated = false;
    bool disposeWorker = false;
    unsigned int mainThreadId;
    HANDLE guiEvent, doneEvent;
    const SplashScreenWin *instance;
    SplashScreenWin::Task currentTask = SplashScreenWin::Task::NONE;

    bool IsMainThread() {
        return GetCurrentThreadId() == mainThreadId;
    }

    SplashScreenWin::SplashScreenWin(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent, const SplashScreenSettings &$settings)
            : SplashScreenBase($parent, $settings) {
        disposeWorker = false;
        this->guiThread = new std::thread(SplashScreenWin::guiWorker);
        while (!workerCreated) { std::this_thread::sleep_for(std::chrono::milliseconds{1}); }
    }

    SplashScreenWin::~SplashScreenWin() {
        disposeWorker = true;
        if (this->guiThread != nullptr) {
            this->guiThread->join();
            this->guiThread = nullptr;
        }
        PostQuitMessage(0);
        DestroyWindow(this->window);
    }

    void SplashScreenWin::Show() {
        if (IsMainThread()) {
            if (this->window == nullptr) {
                throw std::runtime_error{"Failed to allocate the HWND object."};
            }

            this->bgImage->setVisible(true);
            this->update();
            SplashScreenBase::invokeOpenCallback();
        } else {
            DispatchOnMainThread(this, Task::SHOW);
        }
    }

    void SplashScreenWin::Hide() {
        if (IsMainThread()) {
            this->bgImage->setVisible(false);
            ShowWindow(this->window, SW_HIDE);
            SplashScreenBase::invokeCloseCallback();
        } else {
            DispatchOnMainThread(this, Task::HIDE);
        }
    }

    LRESULT CALLBACK SplashScreenWin::WndProc(HWND $hwnd, UINT $message, WPARAM $wParam, LPARAM $lParam) {
        auto *self = reinterpret_cast<SplashScreenWin *>(GetWindowLongPtr($hwnd, GWLP_USERDATA));
        if ((self == nullptr) || self->window != $hwnd) {
            return DefWindowProc($hwnd, $message, $wParam, $lParam);
        }

        switch ($message) {
            case WM_CLOSE: {
                PostQuitMessage(0);
                break;
            }
            case WM_DESTROY: {
                KillTimer(self->window, 0);
                SetWindowLongPtr(self->window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(nullptr));
                self->window = nullptr;
                break;
            }
            case WM_TIMER: {
                if ($wParam == 0) {
                    if (self->destroy) {
                        DestroyWindow(self->window);
                        PostQuitMessage(0);
                    } else {
                        self->update();
                    }
                }
                break;
            }
            default: {
                return DefWindowProc($hwnd, $message, $wParam, $lParam);
            }
        }

        return 0;
    }

    void SplashScreenWin::update() {
        ShowWindow(this->window, SW_SHOWNORMAL);

        HDC hdcScreen = GetDC(nullptr);
        HDC hdcMem = CreateCompatibleDC(hdcScreen);
        HBITMAP hBitmap = CreateCompatibleBitmap(hdcScreen, this->getWidth(), this->getHeight());
        auto hbmpOld = (HBITMAP)SelectObject(hdcMem, hBitmap);

        Gdiplus::Graphics *graphics = Gdiplus::Graphics::FromHDC(hdcMem);

        this->draw(*graphics);

        BLENDFUNCTION blend = {};
        blend.BlendOp = AC_SRC_OVER;
        blend.SourceConstantAlpha = 255;
        blend.AlphaFormat = AC_SRC_ALPHA;

        POINT ptZero = {};

        SIZE sizeSplash = {static_cast<LONG>(this->getWidth()), static_cast<LONG>(this->getHeight())};
        UpdateLayeredWindow(this->window, hdcScreen, nullptr, &sizeSplash,
                            hdcMem, &ptZero, RGB(0, 0, 0), &blend, ULW_OPAQUE);

        delete graphics;
        DeleteObject(hbmpOld);
        DeleteObject(hBitmap);
        SelectObject(hdcMem, hbmpOld);
        DeleteDC(hdcMem);
        ReleaseDC(nullptr, hdcScreen);
    }

    void SplashScreenWin::draw(Gdiplus::Graphics &$graphics) {
        $graphics.Clear(Gdiplus::Color::Black);
        if (this->bgImage != nullptr) {
            this->bgImage->Draw(&$graphics);
        }
        if (this->progressBar != nullptr) {
            this->progressBar->Draw(static_cast<void *>(&$graphics));
        }
        if (this->lbStatus != nullptr) {
            this->lbStatus->Draw(static_cast<void *>(&$graphics));
        }
        if (this->lbProgress != nullptr) {
            this->lbProgress->Draw(static_cast<void *>(&$graphics));
        }
    }

    void SplashScreenWin::Update() {
        if (IsMainThread()) {
            MoveWindow(this->window, this->getX(), this->getY(), this->getWidth(), this->getHeight(), TRUE);
            this->update();
        } else {
            DispatchOnMainThread(this, Task::UPDATE);
        }
    }

    void SplashScreenWin::Create() {
        BaseGuiObject::Create();

        this->checkSizeAndPosition(false, [this]() {
            RECT desktopSize;
            HWND desktop = GetDesktopWindow();
            GetWindowRect(desktop, &desktopSize);
            this->setPosition(desktopSize.right / 2 - this->getWidth() / 2, desktopSize.bottom / 2 - this->getHeight() / 2);
        });

        if (!IsMainThread()) {
            SplashScreenWin::DispatchOnMainThread(this, Task::CREATE);
        } else {
            HINSTANCE hinstance = GetModuleHandle(nullptr);

            const std::string windowTitle = "io-oidis-gui-splashScreen";
            const std::string windowClass = "io-oidis-gui-splashScreen-class" + std::to_string(GetCurrentThreadId());

            WNDCLASSEX wndclassex = {};
            wndclassex.cbSize = sizeof(WNDCLASSEX);
            wndclassex.style = CS_HREDRAW | CS_VREDRAW;
            wndclassex.lpfnWndProc = WndProc;
            wndclassex.cbClsExtra = 0;
            wndclassex.cbWndExtra = 0;
            wndclassex.hInstance = hinstance;
            wndclassex.hIcon = LoadIcon(hinstance, MAKEINTRESOURCE(0));
            wndclassex.hCursor = LoadCursor(nullptr, IDC_ARROW);
            wndclassex.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
            wndclassex.lpszMenuName = nullptr;
            wndclassex.lpszClassName = static_cast<LPCSTR>(windowClass.c_str());
            wndclassex.hIconSm = LoadIcon(hinstance, MAKEINTRESOURCE(0));

            RegisterClassEx(&wndclassex);

            this->window = CreateWindowEx(
                    WS_EX_LAYERED | WS_EX_TOPMOST,
                    static_cast<LPCSTR>(windowClass.c_str()),
                    static_cast<LPCSTR>(windowTitle.c_str()),
                    WS_POPUP,
                    this->getX(), this->getY(),
                    this->getWidth(), this->getHeight(),
                    nullptr,
                    nullptr,
                    hinstance,
                    nullptr);

            if (this->window == nullptr) {
                throw std::runtime_error("Failed to create window.");
            }

            SetWindowLongPtr(this->window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
            SetMenu(this->window, nullptr);

            ShowWindow(this->window, SW_HIDE);

            long style = GetWindowLong(this->window, GWL_EXSTYLE);
            style |= WS_EX_TOOLWINDOW;
            style &= ~(WS_EX_APPWINDOW);
            SetWindowLong(this->window, GWL_EXSTYLE, style);

            UpdateWindow(this->window);

            if (this->bgImage != nullptr) {
                this->bgImage->Create();
                this->bgImage->Load(this->getSettings().imagePath);
                this->bgImage->setSizeAndPosition(0, 0, this->getWidth(), this->getHeight());
            }

            if (this->lbProgress != nullptr) {
                this->lbProgress->Create();
            }

            if (this->lbStatus != nullptr) {
                this->lbStatus->Create();
            }
        }
    }

    void SplashScreenWin::DispatchOnMainThread(const SplashScreenWin *$instance, Task $task) {
        // send message to guiWorker and wait for finish if necessary
        SetEvent(guiEvent);
        instance = $instance;
        currentTask = $task;
        DWORD dwWaitResult = WaitForSingleObject(doneEvent, INFINITE);
        if (dwWaitResult != WAIT_OBJECT_0) {
            throw std::runtime_error("An error occurred in dispatch wait for finish. (" + std::to_string(dwWaitResult) + ")");
        }
    }

    void SplashScreenWin::guiWorker() {
        mainThreadId = GetCurrentThreadId();
        guiEvent = CreateEvent(nullptr, FALSE, FALSE, ("GUI_EVENT" + std::to_string(mainThreadId)).c_str());
        doneEvent = CreateEvent(nullptr, FALSE, FALSE, ("DONE_EVENT" + std::to_string(mainThreadId)).c_str());
        workerCreated = true;

        while (!disposeWorker) {
            DWORD dwWaitResult = WaitForSingleObject(guiEvent, 1);
            if (dwWaitResult == WAIT_OBJECT_0) {
                switch (currentTask) {
                    case CREATE:
                        const_cast<SplashScreenWin *>(instance)->Create();
                        break;
                    case SHOW:
                        const_cast<SplashScreenWin *>(instance)->Show();
                        break;
                    case HIDE:
                        const_cast<SplashScreenWin *>(instance)->Hide();
                        break;
                    case UPDATE:
                        const_cast<SplashScreenWin *>(instance)->Update();
                        break;
                }
                SetEvent(doneEvent);
            }
        }
        CloseHandle(guiEvent);
        CloseHandle(doneEvent);
        workerCreated = false;
    }
}

#ifdef OVERRIDE_UNICODE
#pragma pop_macro("UNICODE")
#endif

#endif  // WIN_PLATFORM
