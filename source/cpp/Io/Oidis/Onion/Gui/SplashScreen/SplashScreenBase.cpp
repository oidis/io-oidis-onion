/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::SplashScreen {
    using Io::Oidis::Onion::Gui::Primitives::BaseGuiObject;

    SplashScreenBase::SplashScreenBase(const BaseGuiObject *$parent, const SplashScreenSettings &$settings)
            : BaseGuiObject($parent) {
        this->settings = $settings;

        this->setPosition(this->settings.x, this->settings.y);
        this->setSize(this->settings.width, this->settings.height);

        this->bgImage = Image::ImageFactory::Create(this);
        this->progressBar = ProgressBar::ProgressBarFactory::Create(this);
        this->lbStatus = Label::LabelFactory::Create(this);
        this->lbProgress = Label::LabelFactory::Create(this);
    }

    SplashScreenBase::~SplashScreenBase() {
        delete this->bgImage;
        delete this->lbStatus;
        delete this->lbProgress;
        delete this->progressBar;
    }

    void SplashScreenBase::invokeOpenCallback() const {
        if (this->openCallback) {
            this->openCallback();
        }
    }

    void SplashScreenBase::invokeCloseCallback() const {
        if (this->closeCallback) {
            this->closeCallback();
        }
    }

    void SplashScreenBase::setOnOpenCallback(const std::function<void()> &$openCallback) {
        this->openCallback = $openCallback;
    }

    void SplashScreenBase::setOnCloseCallback(const std::function<void()> &$CloseCallback) {
        this->closeCallback = $CloseCallback;
    }

    void SplashScreenBase::setProgressLabel(const Io::Oidis::Onion::Gui::Label::LabelBase::LabelSettings &$settings) {
        this->setLabel(this->lbProgress, $settings);
    }

    void SplashScreenBase::setProgressLabelText(const string &$text) {
        if (this->lbProgress != nullptr) {
            this->lbProgress->setText($text);
        }
    }

    void SplashScreenBase::setStatusLabel(const Io::Oidis::Onion::Gui::Label::LabelBase::LabelSettings &$settings) {
        this->setLabel(this->lbStatus, $settings);
    }

    void SplashScreenBase::setStatusLabelText(const string &$text) {
        if (this->lbStatus != nullptr) {
            this->lbStatus->setText($text);
        }
    }

    void SplashScreenBase::setProgressBar(const Io::Oidis::Onion::Gui::ProgressBar::ProgressBarBase::ProgressBarSettings &$settings) {
        if (this->progressBar != nullptr) {
            this->progressBar->setSize($settings.width, $settings.height);
            this->progressBar->setPosition($settings.x, $settings.y);
            this->progressBar->setVisible($settings.visible);
            this->progressBar->setBackground($settings.background);
            this->progressBar->setForeground($settings.foreground);
            this->progressBar->setMarquee($settings.isMarquee);
        }
    }

    void SplashScreenBase::setProgressBarValue(int $value) {
        if (this->progressBar != nullptr) {
            this->progressBar->setValue($value);
        }
    }

    void SplashScreenBase::setLabel(Gui::Label::LabelBase *$label,
                                    const Io::Oidis::Onion::Gui::Label::LabelBase::LabelSettings &$settings) {
        if ($label != nullptr) {
            auto *font = const_cast<Io::Oidis::Onion::Gui::Structures::Font *>($label->getFont());
            font->setColor(Io::Oidis::Onion::Gui::Structures::ColorFactory::Create($settings.color));
            font->setSize($settings.size);
            font->setBold($settings.bold);
            if (!$settings.font.empty()) {
                font->setName($settings.font);
            }
            $label->setFont(font);
            $label->setJustify($settings.justify);
            $label->setSize($settings.width, $settings.height);
            $label->setPosition($settings.x, $settings.y);
            $label->setVisible($settings.visible);
        }
    }

    const SplashScreenBase::SplashScreenSettings &SplashScreenBase::getSettings() const {
        return this->settings;
    }

    void SplashScreenBase::setSettings(const SplashScreenBase::SplashScreenSettings &$settings) {
        this->settings = $settings;
        this->setPosition($settings.x, $settings.y);
        this->setSize($settings.width, $settings.height);
    }

    void SplashScreenBase::checkSizeAndPosition(bool $finalValues, const std::function<void()> &$setToCenterCallback) {
        if ($finalValues) {
            if (this->getWidth() <= 0 || this->getHeight() <= 0) {
                throw std::range_error("Invalid width or height of splashscreen.");
            }
        } else {
            if (this->getWidth() < 0 || this->getHeight() < 0) {
                throw std::range_error("Invalid width or height of splashscreen.");
            }
        }

        if (this->getX() < -1 || this->getY() < -1) {
            throw std::range_error("Invalid width or height of splashscreen.");
        }

        if (this->getX() == -1 || this->getY() == -1) {
            if ($setToCenterCallback) {
                $setToCenterCallback();
            }
        }
    }
}
