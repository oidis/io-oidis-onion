/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "../../../../../reference.hpp"

#include "SplashScreenMac.hpp"

#include <Cocoa/Cocoa.h>  // NOLINT

// @formatter:off
@interface SplashView : NSWindow {}

@property(nonatomic, assign) NSSize size;
@property(nonatomic, assign) NSPoint position;

- (void) Update;

- (void) AddView: (NSView*) $view;  // NOLINT(readability/casting)
- (void) Show;
- (void) Hide;
- (void) Close;
@end

@implementation SplashView

- (void) Update {
    if ([NSThread isMainThread]) {
        auto screen = [[NSScreen mainScreen] frame];
        auto y = screen.size.height - self.size.height - self.position.y + screen.origin.y;
        [self setFrame:NSMakeRect(self.position.x, y, self.size.width, self.size.height) display:YES];
    } else {
        [self performSelectorOnMainThread:@selector(Update) withObject:nil waitUntilDone:YES];
    }
}

- (void) AddView: (NSView*) $view {  // NOLINT(readability/casting)
    if ([NSThread isMainThread]) {
        [self.contentView addSubview:$view];
    } else {
        [self performSelectorOnMainThread:@selector(AddView:) withObject:$view waitUntilDone:YES];
    }
}

- (void) Show {
    if ([NSThread isMainThread]) {
        [self setIsVisible:YES];
        [self makeKeyAndOrderFront:NSApp];
    } else {
        [self performSelectorOnMainThread:@selector(Show) withObject:nil waitUntilDone:YES];
    }
}

- (void) Hide {
    if ([NSThread isMainThread]) {
        [self setIsVisible:NO];
    } else {
        [self performSelectorOnMainThread:@selector(Hide) withObject:nil waitUntilDone:YES];
    }
}

- (void) Close {
    if ([NSThread isMainThread]) {
        [self setFrame:NSMakeRect(0.0f, 0.0f, 0.0f, 0.0f) display:NO animate:NO];
    } else {
        [self performSelectorOnMainThread:@selector(Close) withObject:nil waitUntilDone:YES];
    }
}
@end
// @formatter:on

namespace Io::Oidis::Onion::Gui::SplashScreen {
    SplashScreenMac::SplashScreenMac(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent,
                                     const SplashScreenBase::SplashScreenSettings &$settings)
            : SplashScreenBase($parent, $settings) {
        this->isCreated = false;

        // @formatter:off
        this->window  = [[[SplashView alloc]
                          initWithContentRect:NSMakeRect(0, 0,
                                  this->getSettings().width, this->getSettings().height)
                          styleMask:NSWindowStyleMaskBorderless|NSWindowStyleMaskFullSizeContentView
                          backing:NSBackingStoreBuffered
                          defer:NO
                          screen:nil] autorelease];
        // @formatter:on

        if (this->window == nil) {
            throw std::runtime_error{"Failed to allocate the NSWindow object"};
        }
        // @formatter:off
        [this->window setHasShadow:YES];
        // todo: add switch for transparency
        // [this->window setBackgroundColor:[NSColor clearColor]];
        // @formatter:on

        this->window.level = NSStatusWindowLevel;
        this->window.styleMask = NSWindowStyleMaskFullSizeContentView;
        this->window.contentView.wantsLayer = true;
    }

    void SplashScreenMac::Create() {
        BaseGuiObject::Create();
        this->isCreated = false;

        // @formatter:off
        [this->window setPosition:NSMakePoint(this->getX(), this->getY())];
        [this->window setSize:NSMakeSize(this->getWidth(), this->getHeight())];
        [this->window Update];
        // @formatter:on

        this->checkSizeAndPosition(false, [&]() {
            this->coercePosition();
        });

        this->bgImage->Create();
        this->bgImage->Load(this->getSettings().imagePath);
        this->bgImage->setPosition(0, 0);
        this->bgImage->setSize(this->getWidth(), this->getHeight());
        // @formatter:off
        [this->window AddView:reinterpret_cast<NSView*>(this->bgImage->getNative())];
        // @formatter:on

        this->lbProgress->Create();
        // @formatter:off
        [this->window AddView:reinterpret_cast<NSView*>(this->lbProgress->getNative())];
        // @formatter:on

        this->lbStatus->Create();
        // @formatter:off
        [this->window AddView:reinterpret_cast<NSView*>(this->lbStatus->getNative())];
        // @formatter:on

        this->progressBar->Create();
        // @formatter:off
        [this->window AddView:reinterpret_cast<NSView*>(this->progressBar->getNative())];
        // @formatter:on

        this->isCreated = true;
    }

    SplashScreenMac::~SplashScreenMac() {
        // in order to quit message loop without the delegate, this hack is necessary
        // @formatter:off
        this->isCreated = false;
        [this->window Close];
        // @formatter:on
    }

    void SplashScreenMac::Show() {
        if (this->isCreated) {
            // @formatter:off
            [this->window Show];
            // @formatter:on
            this->bgImage->setVisible(true);

            SplashScreenMac::invokeOpenCallback();
        }
    }

    void SplashScreenMac::Hide() {
        if (this->isCreated) {
            // @formatter:off
            [this->window Hide];
            // @formatter:on

            this->bgImage->setVisible(false);

            SplashScreenMac::invokeCloseCallback();
        }
    }

    void SplashScreenMac::Update() {
        if (this->isCreated) {
            // @formatter:off
            [this->window Update];
            // @formatter:on
            this->bgImage->Draw(nullptr);
            this->lbProgress->Draw(nullptr);
            this->lbStatus->Draw(nullptr);
            this->progressBar->Draw(nullptr);
        }
    }

    void *SplashScreenMac::getNative() const {
        return this->window;
    }

    void SplashScreenMac::setSize(int $width, int $height) {
        BaseGuiObject::setSize($width, $height);
        // @formatter:off
        [this->window setSize: NSMakeSize($width, $height)];
        [this->window Update];
        // @formatter:on
        if (this->isCreated) {
            this->Update();
        }
    }

    void SplashScreenMac::setPosition(int $x, int $y) {
        BaseGuiObject::setPosition($x, $y);
        // @formatter:off
        [this->window setPosition: NSMakePoint($x, $y)];
        [this->window Update];
        // @formatter:on
        if (this->isCreated) {
            this->Update();
        }
    }

    void SplashScreenMac::setSizeAndPosition(int $x, int $y, int $width, int $height) {
        BaseGuiObject::setSizeAndPosition($x, $y, $width, $height);
        this->setPosition($x, $y);
        this->setSize($width, $height);
    }

    void SplashScreenMac::setSettings(const SplashScreenBase::SplashScreenSettings &$settings) {
        SplashScreenBase::setSettings($settings);

        this->checkSizeAndPosition(false, [&]() {
            this->coercePosition();
        });
    }

    void SplashScreenMac::coercePosition() {
        CGFloat xPos = this->getX();
        CGFloat yPos = this->getY();
        if (this->getX() < 0) {
            // @formatter:off
            xPos = NSWidth([[this->window screen] frame])/2 - NSWidth([this->window frame])/2;
            // @formatter:on
        }
        if (this->getY() < 0) {
            // @formatter:off
            yPos = NSHeight([[this->window screen] frame])/2 - NSHeight([this->window frame])/2;
            // @formatter:on
        }
        this->setPosition(xPos, yPos);
    }
}

#endif  // MAC_PLATFORM
