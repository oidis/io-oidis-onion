/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENWIN_HPP_
#define IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENWIN_HPP_

#if defined(WIN_PLATFORM) || defined(_WIN32)

#include <windows.h>    // NOLINT [build/include]
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4458)  // declaration of 'xxx' hides class member
#endif
#include <gdiplus.h>
#ifdef _MSC_VER
#pragma warning(pop)
#endif
#include <thread>

#include "SplashScreenBase.hpp"

namespace Io::Oidis::Onion::Gui::SplashScreen {
    class SplashScreenWin : public SplashScreenBase {
     public:
        explicit SplashScreenWin(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent, const SplashScreenSettings &$settings);

        ~SplashScreenWin();

        void Show() override;

        void Hide() override;

        void Update() override;

        void Create() override;

        enum Task {
            NONE,
            CREATE,
            SHOW,
            HIDE,
            UPDATE
        };

     private:
        static LRESULT CALLBACK WndProc(HWND $hwnd, UINT $message, WPARAM $wParam, LPARAM $lParam);

        void update();

        void draw(Gdiplus::Graphics &$graphics);

        static void guiWorker();

        static void DispatchOnMainThread(const SplashScreenWin *$instance, Task $task);

        HWND window = nullptr;
        bool destroy = false;
        std::thread *guiThread = nullptr;
    };
}

#endif  // defined(WIN_PLATFORM) || defined(_WIN32)

#endif  // IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENWIN_HPP_
