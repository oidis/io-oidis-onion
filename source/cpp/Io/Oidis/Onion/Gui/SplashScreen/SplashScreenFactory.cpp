/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::SplashScreen::SplashScreenFactory {

    SplashScreenBase *Create(const SplashScreenBase::SplashScreenSettings &$settings) {
#if defined(MAC_PLATFORM) || defined(__APPLE__)
        return new SplashScreenMac(nullptr, $settings);
#elif defined(LINUX_PLATFORM) || defined(__linux__)
#if defined(ONION_NO_GUI)
        return new SplashScreenCmd(nullptr, $settings);
#else
        return new SplashScreenLinux(nullptr, $settings);
#endif
#elif defined(WIN_PLATFORM) || defined(_WIN32)
        return new SplashScreenWin(nullptr, $settings);
#else
        return nullptr;
#endif
    }

}
