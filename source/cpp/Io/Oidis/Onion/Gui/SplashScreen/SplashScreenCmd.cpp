/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::SplashScreen {
    SplashScreenCmd::SplashScreenCmd(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent,
                                     const SplashScreenBase::SplashScreenSettings &$settings)
            : SplashScreenBase($parent, $settings) {
//        if ($settings.cmdMax != 0) {
//            this->cmdMax = $settings.cmdMax;
//        }
//
//        if (!$settings.splashScreenPlainText.empty()) {
//            this->splashScreenPlainText = $settings.splashScreenPlainText;
//        }
    }

    void SplashScreenCmd::Show() {
        if (!this->splashScreenPlainText.empty()) {
            std::cout << this->splashScreenPlainText;
        }
        SplashScreenBase::invokeOpenCallback();
    }

    void SplashScreenCmd::Hide() {
        SplashScreenBase::invokeCloseCallback();
    }

    void SplashScreenCmd::Update() {
        if (this->progressBar) {
            auto pbValue = static_cast<double>(this->progressBar->getValue());
            auto pbMax = static_cast<double>(this->progressBar->getMax());
            double ratio = (pbValue / pbMax);
            int val = static_cast<int>(this->cmdMax * ratio);
            std::string out;
            out += "Progress:\t\t";
            int i = 0;
            for (; i < val; i++) {
                out += "#";
            }
            for (; i < this->cmdMax; i++) {
                out += ".";
            }
            out += " " + std::to_string(static_cast<int>(ratio * 100));
            out += "%\n";
            std::cout << out;
        }
    }
}

#endif  // LINUX_PLATFORM
