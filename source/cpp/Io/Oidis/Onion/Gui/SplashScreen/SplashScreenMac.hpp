/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENMAC_HPP_
#define IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENMAC_HPP_

#if defined(MAC_PLATFORM) || defined(__APPLE__)

#include <objc/objc.h>

#include "SplashScreenBase.hpp"

@class SplashView;

namespace Io::Oidis::Onion::Gui::SplashScreen {
    class SplashScreenMac : public SplashScreenBase {
     public:
        explicit SplashScreenMac(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent,
                const SplashScreenBase::SplashScreenSettings &$settings);

        ~SplashScreenMac() override;

        void Show() override;

        void Hide() override;

        void Update() override;

        void *getNative() const override;

        void Create() override;

        void setSize(int $width, int $height) override;

        void setPosition(int $x, int $y) override;

        void setSizeAndPosition(int $x, int $y, int $width, int $height) override;

        void setSettings(const SplashScreenSettings &$settings) override;

     protected:
        virtual void coercePosition();

     private:
        SplashView *window = nil;
        bool isCreated = false;
    };
}

#endif  // defined(MAC_PLATFORM) || defined(__APPLE__)

#endif  // IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENMAC_HPP_
