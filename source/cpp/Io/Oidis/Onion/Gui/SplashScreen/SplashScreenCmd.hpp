/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENCMD_HPP_
#define IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENCMD_HPP_

#ifdef LINUX_PLATFORM

#include "SplashScreenBase.hpp"

namespace Io::Oidis::Onion::Gui::SplashScreen {
    class SplashScreenCmd : public SplashScreenBase {
     public:
        explicit SplashScreenCmd(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent,
                const SplashScreenBase::SplashScreenSettings &$settings);

        void Show() override;

        void Hide() override;

        void Update() override;

     private:
        bool callbackAdded = false;
        int cmdMax = 20;
        std::string splashScreenPlainText;
    };
}

#endif  // #ifdef LINUX_PLATFORM

#endif  // IO_OIDIS_ONION_GUI_SPLASHSCREEN_SPLASHSCREENCMD_HPP_
