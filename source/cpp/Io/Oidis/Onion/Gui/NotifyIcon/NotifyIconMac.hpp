/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONMAC_HPP_
#define IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONMAC_HPP_

#if defined(MAC_PLATFORM) || defined(__APPLE__)

#include "NotifyIconBase.hpp"

@class NSNotifyIcon;

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    class NotifyIconMac : public NotifyIconBase {
     public:
        NotifyIconMac();

        virtual ~NotifyIconMac();

        bool Create(std::shared_ptr<NotifyIconOptions> $options) override;

        bool Destroy() override;

     private:
        bool updateNotifyIcon(std::shared_ptr<NotifyIconOptions> $options) override;

        NSNotifyIcon *nativeIcon = nullptr;
    };
}

#endif  // defined(MAC_PLATFORM) || defined(__APPLE__)

#endif  // IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONMAC_HPP_
