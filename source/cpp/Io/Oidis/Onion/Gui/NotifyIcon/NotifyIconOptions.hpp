/* ********************************************************************************************************* *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONOPTIONS_HPP_
#define IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONOPTIONS_HPP_

#include <string>
#include <vector>
#include "NotifyIconContextMenu.hpp"
#include "NotifyBalloonIcon.hpp"

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    using std::string;

    class NotifyIconOptions {
     public:
        const string &getTip() const {
            return this->tip;
        }

        void setTip(const string &$tip) {
            this->tip = $tip;
        }

        const string &getIconPath() const {
            return this->iconPath;
        }

        void setIconPath(const string &$iconPath) {
            this->iconPath = $iconPath;
        }

        const NotifyIconContextMenu &getContextMenu() const {
            return this->contextMenu;
        }

        void setContextMenu(const NotifyIconContextMenu &$contextMenu) {
            this->contextMenu = $contextMenu;
        }

        void AddItem(const NotifyIconContextMenuItem &$item) {
            this->contextMenu.push_back($item);
        }

        void setBalloonIcon(std::shared_ptr<NotifyBalloonIcon> $balloonIcon) {
            this->balloonIcon = std::move($balloonIcon);
        }

        const std::shared_ptr<NotifyBalloonIcon> &getBalloonIcon() const {
            return this->balloonIcon;
        }

     private:
        std::string tip;
        std::string iconPath;
        NotifyIconContextMenu contextMenu;
        std::shared_ptr<NotifyBalloonIcon> balloonIcon = nullptr;
    };
}

#endif  // IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONOPTIONS_HPP_
