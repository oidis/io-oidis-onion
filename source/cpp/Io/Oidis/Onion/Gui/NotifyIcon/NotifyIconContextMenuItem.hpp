/* ********************************************************************************************************* *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONCONTEXTMENUITEM_HPP_
#define IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONCONTEXTMENUITEM_HPP_

#include <functional>
#include <string>

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    class NotifyIconContextMenuItem {
     public:
        using ClickedCallback = std::function<void(const std::string &id, const std::string &name)>;

        NotifyIconContextMenuItem(const std::string &$name, const std::string &$id, const ClickedCallback &$clickedCallback);

        NotifyIconContextMenuItem();

        void OnClick() const;

        const std::string &getName() const;

        void setName(const std::string &$name);

        const std::string &getId() const;

        void setId(const std::string &$label);

        int getPosition() const;

        void setPosition(int $position);

        void setCallback(const ClickedCallback &$callback);

     private:
        std::string name;
        std::string id;
        int position = 0;
        ClickedCallback clickedCallback = nullptr;
    };
}

#endif  // IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONCONTEXTMENUITEM_HPP_
