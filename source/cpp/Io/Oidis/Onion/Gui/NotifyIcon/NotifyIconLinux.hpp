/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONLINUX_HPP_
#define IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONLINUX_HPP_

#if (defined(LINUX_PLATFORM) || defined(__linux__))  && !defined(ONION_NO_GUI)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"

#include <libappindicator/app-indicator.h>

#pragma GCC diagnostic pop

#include <thread>
#include "NotifyIconBase.hpp"

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    class NotifyIconLinux : public NotifyIconBase {
     public:
        bool Create(std::shared_ptr<NotifyIconOptions> $options) override;

        bool Destroy() override;

     private:
        // This has to be static, because there's no real way to destroy the AppIndicator instance, so we're just re-using
        // and updating (if necessary) this particular instance.
        static AppIndicator *indicator;
        GtkWidget *contextMenu = nullptr;

        static void contextMenuItemClicked(GtkEntry *$menuItem, gpointer $item);

        bool updateNotifyIcon(std::shared_ptr<NotifyIconOptions> $options) override;
    };
}

#endif  // defined(LINUX_PLATFORM) || defined(__linux__)

#endif  // IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONLINUX_HPP_
