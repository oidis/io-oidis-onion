/* ********************************************************************************************************* *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONBASE_HPP_
#define IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONBASE_HPP_

#include "NotifyIconOptions.hpp"
#include "NotifyIconContextMenu.hpp"

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    /**
     * NotifyIcon class provides API to control status icon in system tray.
     */
    class NotifyIconBase {
     public:
        constexpr static size_t GetIndexModifier() {
            return 2000;
        }

        NotifyIconBase() = default;

        virtual ~NotifyIconBase() = default;

        NotifyIconBase(const NotifyIconBase &) = delete;

        NotifyIconBase &operator=(const NotifyIconBase &) = delete;

        NotifyIconBase(NotifyIconBase &&) = delete;

        NotifyIconBase &operator=(NotifyIconBase &&) = delete;

        /**
         * Initializes options and indicator instance.
         * @param $options Options of icon.
         * @return Return true if initialization was successful.
         */
        virtual bool Create(std::shared_ptr<NotifyIconOptions> $options) = 0;

        /**
         * Disables the icon. Calling Create() will make the icon re-appear.
         * @return Return true if successful.
         */
        virtual bool Destroy() = 0;

        /**
         * If not created, Create() the icon, else modify. $options will be the new settings for the icon.
         * @param $options New options for the icon.
         * @return Return true if successful.
         */
        virtual bool Modify(std::shared_ptr<NotifyIconOptions> $options);

     protected:
        std::shared_ptr<NotifyIconOptions> options = nullptr;

        bool created = false;

        void onCreate(bool $success) noexcept;

        void onDestroy(bool $success) noexcept;

        void onModify(bool $success) noexcept;

        virtual bool updateNotifyIcon(std::shared_ptr<NotifyIconOptions> $options) = 0;

        bool iconFileExist(const std::string &$path) const;
    };
}

#endif  // IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONBASE_HPP_
