/* ********************************************************************************************************* *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::NotifyIcon {

    const std::string &NotifyBalloonIcon::getTitle() const {
        return this->title;
    }

    const std::string &NotifyBalloonIcon::getMessage() const {
        return this->message;
    }

    const NotifyBalloonIcon::NotifyBalloonIconType &NotifyBalloonIcon::getType() const {
        return this->type;
    }

    const std::string &NotifyBalloonIcon::getUserIcon() const {
        return this->userIcon;
    }

    bool NotifyBalloonIcon::isNoSound() const {
        return false;
    }

    void NotifyBalloonIcon::setMessage(const string &$message) {
        this->message = $message;
    }

    void NotifyBalloonIcon::setType(const NotifyBalloonIcon::NotifyBalloonIconType &$type) {
        this->type = $type;
    }

    void NotifyBalloonIcon::setTitle(const std::string &$title) {
        this->title = $title;
    }

    void NotifyBalloonIcon::setUserIcon(const std::string &$userIcon) {
        this->userIcon = $userIcon;
    }
}
