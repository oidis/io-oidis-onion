/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#if defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    AppIndicator *NotifyIconLinux::indicator = nullptr;

    bool NotifyIconLinux::Create(std::shared_ptr<NotifyIconOptions> $options) {
        if (!this->created) {
            this->options = std::move($options);

            if (NotifyIconLinux::indicator == nullptr && this->options) {
                NotifyIconLinux::indicator = app_indicator_new("io-oidis-gui-icon",
                                                               this->options->getIconPath().c_str(),
                                                               AppIndicatorCategory::APP_INDICATOR_CATEGORY_APPLICATION_STATUS);
            }

            if (this->updateNotifyIcon(nullptr)) {
                this->onCreate(true);
                return true;
            }
        }
        return false;
    }

    bool NotifyIconLinux::Destroy() {
        if (this->created) {
            app_indicator_set_status(NotifyIconLinux::indicator, AppIndicatorStatus::APP_INDICATOR_STATUS_PASSIVE);

            this->onDestroy(true);

            return true;
        }
        return false;
    }

    void NotifyIconLinux::contextMenuItemClicked(GtkEntry *$menuItem, gpointer $item) {
        (void)$menuItem;
        auto item = reinterpret_cast<NotifyIconContextMenuItem *>($item);
        if (item) {
            item->OnClick();
        }
    }

    bool NotifyIconLinux::updateNotifyIcon(std::shared_ptr<NotifyIconOptions> $options) {
        if ($options) {
            this->options = std::move($options);
        }

        if (this->options) {
            app_indicator_set_title(NotifyIconLinux::indicator, this->options->getTip().c_str());
            app_indicator_set_icon_theme_path(NotifyIconLinux::indicator, this->options->getIconPath().c_str());

            if (this->contextMenu) {
                gtk_widget_hide_all(this->contextMenu);
                gtk_widget_destroy(this->contextMenu);
            }
            this->contextMenu = gtk_menu_new();

            for (int i = 0; i < static_cast<int>(this->options->getContextMenu().size()); i++) {
                GtkWidget *menuItem = gtk_image_menu_item_new_with_label(this->options->getContextMenu().at(i).getName().c_str());
                g_signal_connect(menuItem, "activate", GTK_SIGNAL_FUNC(NotifyIconLinux::contextMenuItemClicked),
                                 (gpointer)(&this->options->getContextMenu().at(i)));

                gtk_menu_shell_insert(GTK_MENU_SHELL(this->contextMenu), menuItem, i);
            }

            app_indicator_set_menu(NotifyIconLinux::indicator, GTK_MENU(this->contextMenu));

            gtk_widget_show_all(this->contextMenu);
        }

        if (NotifyIconLinux::indicator) {
            app_indicator_set_status(NotifyIconLinux::indicator, AppIndicatorStatus::APP_INDICATOR_STATUS_ACTIVE);
        }

        return true;
    }
}

#endif  // LINUX_PLATFORM
