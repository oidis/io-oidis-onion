/* ********************************************************************************************************* *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONWIN_HPP_
#define IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONWIN_HPP_

#ifdef WIN_PLATFORM

#include <iostream>

#include "windows.h"    // NOLINT(build/include)
#include "shellapi.h"   // NOLINT(build/include)
#include "strsafe.h"  // NOLINT(build/include)
#include "objbase.h"  // NOLINT(build/include)

#include "NotifyIconBase.hpp"

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    class NotifyIconWin : public NotifyIconBase {
     public:
        ~NotifyIconWin();

        bool Create(std::shared_ptr<NotifyIconOptions> $options) override;

        bool Destroy() override;

        bool Modify(std::shared_ptr<NotifyIconOptions> $options) override;

     private:
        static LRESULT CALLBACK WindowProcedure(HWND $hwnd, UINT $message, WPARAM $wParam, LPARAM $lParam);

        constexpr static int GetIndexModifier() { return 2000; }

        NOTIFYICONDATA iconData = {};
        HWND windowHandle = nullptr;
        HMENU hMenu = nullptr;

        bool createWindowForIcon();

        bool updateNotifyIcon(std::shared_ptr<NotifyIconOptions> $options) override;

        void loadIcon(HICON *$icon, const std::string &$path);
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONWIN_HPP_
