/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONFACTORY_HPP_
#define IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONFACTORY_HPP_

#include <memory>

namespace Io::Oidis::Onion::Gui::NotifyIcon::NotifyIconFactory {
    NotifyIconBase *Create();
}

#endif  // IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYICONFACTORY_HPP_
