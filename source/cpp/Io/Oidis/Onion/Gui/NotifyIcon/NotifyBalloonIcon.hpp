/* ********************************************************************************************************* *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYBALLOONICON_HPP_
#define IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYBALLOONICON_HPP_

#include <string>

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    class NotifyBalloonIcon {
     public:
        // TODO(nxa33118) use WUI_DECLARE macro from chromium
        enum NotifyBalloonIconType {
            NONE = 0,
            INFO,
            WARNING,
            ERROR_TYPE,     // using _TYPE here because of windows #define ERROR in headerfile
            USER
        };

        /**
         * @return Returns balloon title.
         */
        const std::string &getTitle() const;

        void setTitle(const std::string &$title);

        /**
         * @return Returns balloon message.
         */
        const std::string &getMessage() const;

        void setMessage(const std::string &$message);

        /**
         * @return Returns balloon icon type.
         */
        const NotifyBalloonIconType &getType() const;

        void setType(const NotifyBalloonIconType &$type);

        /**
         * @return Returns user icon path.
         */
        const std::string &getUserIcon() const;

        void setUserIcon(const std::string &$userIcon);

        /**
         * @return Returns true if balloon sound is disabled, false otherwise.
         */
        bool isNoSound() const;

        void setNoSound(bool $noSound) {
            this->noSound = $noSound;
        }

     private:
        std::string title = "";
        std::string message = "";
        NotifyBalloonIconType type = NotifyBalloonIconType::NONE;
        std::string userIcon = "";
        bool noSound = false;
    };
}

#endif  // IO_OIDIS_ONION_GUI_NOTIFYICON_NOTIFYBALLOONICON_HPP_
