/* ********************************************************************************************************* *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    bool NotifyIconBase::Modify(std::shared_ptr<NotifyIconOptions> $options) {
        if (!this->created) {
            return this->Create($options);
        } else {
            if (this->updateNotifyIcon($options)) {
                this->onModify(true);
                return true;
            }
        }
        return false;
    }

    void NotifyIconBase::onCreate(const bool $success) noexcept {
        if ($success) {
            this->created = true;
        }
    }

    void NotifyIconBase::onDestroy(const bool $success) noexcept {
        if ($success) {
            this->created = false;
        }
    }

    void NotifyIconBase::onModify(const bool $success) noexcept {
        (void)$success;
    }

    bool NotifyIconBase::iconFileExist(const std::string &$path) const {
        if ($path.empty()) {
            return false;
        } else {
            std::ifstream ifs{$path, std::ios_base::in};

            return ifs.is_open();
        }
    }
}
