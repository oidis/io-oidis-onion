/* ********************************************************************************************************* *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    NotifyIconContextMenuItem::NotifyIconContextMenuItem(const std::string &$name, const std::string &$id,
                                                         const ClickedCallback &$clickedCallback)
            : name($name),
              id($id),
              clickedCallback($clickedCallback) {}

    void NotifyIconContextMenuItem::OnClick() const {
        if (this->clickedCallback != nullptr) {
            this->clickedCallback(this->id, this->name);
        }
    }

    const std::string &NotifyIconContextMenuItem::getName() const {
        return this->name;
    }

    const std::string &NotifyIconContextMenuItem::getId() const {
        return this->id;
    }

    void NotifyIconContextMenuItem::setName(const std::string &$name) {
        this->name = $name;
    }

    void NotifyIconContextMenuItem::setId(const std::string &$id) {
        this->id = $id;
    }

    NotifyIconContextMenuItem::NotifyIconContextMenuItem() {}

    void NotifyIconContextMenuItem::setCallback(const NotifyIconContextMenuItem::ClickedCallback &$callback) {
        this->clickedCallback = $callback;
    }

    int NotifyIconContextMenuItem::getPosition() const {
        return this->position;
    }

    void NotifyIconContextMenuItem::setPosition(int $position) {
        this->position = $position;
    }
}
