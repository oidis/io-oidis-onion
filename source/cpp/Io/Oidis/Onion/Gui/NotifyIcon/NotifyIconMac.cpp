/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "NotifyIconMac.hpp"

#include <Cocoa/Cocoa.h>
#include <algorithm>

// @formatter:off

@interface ItemInfo:NSObject
    @property(nonatomic, retain) NSString *title;
    @property(nonatomic, retain) NSString *id;
    @property(nonatomic, assign) std::function<void()> callback;
@end

@implementation ItemInfo
@end

@interface NotifyContextMenuItem:NSMenuItem
- (id)initWithSettings:(ItemInfo *)$info;  // NOLINT(readability/casting)
@property(nonatomic, retain) ItemInfo *info;
@end

@implementation NotifyContextMenuItem
- (id)initWithSettings:(ItemInfo *)$info {  // NOLINT(readability/casting)
    if (self = [super initWithTitle:[$info title]
                      action:@selector(contextMenuItemClicked:)
                      keyEquivalent:@""]) {
        self.info = $info;
        self.target = self;
    }
    return self;
}

- (void)contextMenuItemClicked:(NotifyContextMenuItem *)$sender {  // NOLINT(readability/casting)
    if ($sender != nil && $sender.title != nil) {
        self.info.callback();
    }
}

- (void)dealloc {
    if (self.info != nil) {
        [self.info release];
        self.info = nil;
    }
    [super dealloc];
}
@end

@interface NSNotifyIcon : NSObject
@property(nonatomic, retain) NSMenu *menu;
@property(nonatomic, retain) NSStatusItem *statusIcon;
@property(nonatomic, retain) NSWindow *dummyWindow;
@property(nonatomic, retain) NSString *icon;
@property(nonatomic, retain) NSString *toolTip;

- (void)ConstructDummy;
- (void)Destroy;
- (void)UpdateMenu;
- (void)AddMenuItem:(ItemInfo *)$info;  // NOLINT(readability/casting)
@end

@implementation NSNotifyIcon
+ (NSImage *)ScaleImage:(NSImage *)$image size:(NSSize)$size {  // NOLINT(readability/casting)
    if ($image != nil) {
        NSImage *source = $image;

        if ([source isValid]) {
            NSImage * scaled = [[NSImage alloc] initWithSize:$size];
            [scaled lockFocus];
            [source setSize:$size];
            [NSGraphicsContext.currentContext setImageInterpolation:NSImageInterpolationHigh];
            [source drawAtPoint:NSZeroPoint fromRect:CGRectMake(0, 0, $size.width, $size.height)
                    operation:NSCompositingOperationCopy
                    fraction:1.0];
            [scaled unlockFocus];
            return scaled;
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

- (void)UpdateMenu {
    if ([NSThread isMainThread]) {
        [self.menu removeAllItems];
        [[[self statusIcon] button] setToolTip:self.toolTip];
        [[[self statusIcon] button] setImage:[NSNotifyIcon ScaleImage:
                [[NSImage alloc] initWithContentsOfFile:self.icon] size:NSMakeSize(16, 16)]];
    } else {
        [self performSelectorOnMainThread:@selector(UpdateMenu) withObject:nil waitUntilDone:YES];
    }
}

- (void)AddMenuItem:(ItemInfo *)$info {  // NOLINT(readability/casting)
    if ([NSThread isMainThread]) {
        NSMenuItem *menuItem = [[[NotifyContextMenuItem alloc] initWithSettings:$info] autorelease];
        [self.menu addItem:menuItem];
    } else {
        [self performSelectorOnMainThread:@selector(AddMenuItem:) withObject:$info waitUntilDone:YES];
    }
}

- (void)Destroy {
    if ([NSThread isMainThread]) {
        [NSStatusBar.systemStatusBar removeStatusItem:self.statusIcon];
    } else {
        [self performSelectorOnMainThread:@selector(Destroy) withObject:nil waitUntilDone:YES];
    }
}

- (void)DummyClose {
    if ([NSThread isMainThread]) {
        [self.dummyWindow setFrame:NSMakeRect(0.0f, 0.0f, 0.0f, 0.0f) display:NO animate:NO];
    } else {
        [self performSelectorOnMainThread:@selector(DummyClose) withObject:nil waitUntilDone:YES];
    }
}

- (void)ConstructDummy {
    if ([NSThread isMainThread]) {
         self.dummyWindow =  [[[NSWindow alloc]
                          initWithContentRect:NSMakeRect(0, 0, 10, 10)
                          styleMask:NSWindowStyleMaskBorderless|NSWindowStyleMaskFullSizeContentView
                          backing:NSBackingStoreBuffered
                          defer:NO
                          screen:nil] autorelease];
        self.dummyWindow.level = NSStatusWindowLevel;
        self.dummyWindow.styleMask = NSWindowStyleMaskFullSizeContentView;
        self.dummyWindow.contentView.wantsLayer = true;
    } else {
        [self performSelectorOnMainThread:@selector(ConstructDummy) withObject:nil waitUntilDone:YES];
    }
}

- (id)init {
    if (self = [super init]) {
        self.menu = [[NSMenu alloc] initWithTitle:@""];
        self.statusIcon = [NSStatusBar.systemStatusBar statusItemWithLength:NSSquareStatusItemLength];
        self.statusIcon.menu = self.menu;

        [self ConstructDummy];
    }

    return self;
}

- (void)dealloc {
    if (self.menu != nil) {
        [self.menu release];
        self.menu = nil;
    }

    if (self.statusIcon != nil) {
        [self.statusIcon release];
        self.statusIcon = nil;
    }
    [self DummyClose];
    [super dealloc];
}
@end

// @formatter:on

namespace Io::Oidis::Onion::Gui::NotifyIcon {

    NotifyIconMac::NotifyIconMac()
            : NotifyIconBase() {
        if (this->nativeIcon == nullptr) {
            // @formatter:off
            this->nativeIcon = [[NSNotifyIcon alloc] init];
                // @formatter:on
        }
    }

    bool NotifyIconMac::Create(std::shared_ptr<NotifyIconOptions> $options) {
        if (!this->created) {
            if (this->updateNotifyIcon($options)) {
                this->onCreate(true);
                return true;
            }
        }

        return false;
    }

    bool NotifyIconMac::Destroy() {
        // @formatter:off
        [[NSStatusBar systemStatusBar] removeStatusItem:[this->nativeIcon statusIcon]];
        // @formatter:on

        return true;
    }

    bool NotifyIconMac::updateNotifyIcon(std::shared_ptr<NotifyIconOptions> $options) {
        if ($options != nullptr) {
            // @formatter:off
            [this->nativeIcon setIcon:[NSString stringWithUTF8String:$options->getIconPath().c_str()]];
            [this->nativeIcon setToolTip:[NSString stringWithUTF8String:$options->getTip().c_str()]];
            [this->nativeIcon UpdateMenu];
            // @formatter:on

            for (const auto &i : $options->getContextMenu()) {
                // @formatter:off
                ItemInfo *itemInfo = [ItemInfo alloc];
                [itemInfo setId:[NSString stringWithUTF8String:i.getId().c_str()]];
                [itemInfo setTitle:[NSString stringWithUTF8String:i.getName().c_str()]];
                [itemInfo setCallback:[&](){
                            i.OnClick();
                        }];
                [this->nativeIcon  AddMenuItem:itemInfo];
                // @formatter:on
            }
            return true;
        } else {
            return false;
        }
    }

    NotifyIconMac::~NotifyIconMac() {
        // @formatter:off
        [this->nativeIcon Destroy];
        [this->nativeIcon DummyClose];
        // @formatter:on

        if (this->nativeIcon != nullptr) {
            // @formatter:off
            [this->nativeIcon release];
            // @formatter:on
            this->nativeIcon = nullptr;

            this->onDestroy(true);
        }
    }
}

#endif  // MAC_PLATFORM
