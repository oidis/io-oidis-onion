/* ********************************************************************************************************* *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#ifdef UNICODE
#define OVERRIDE_UNICODE 1
#pragma push_macro("UNICODE")
#undef UNICODE
#endif

#include "../../../../../reference.hpp"

using std::string;

namespace Io::Oidis::Onion::Gui::NotifyIcon {

    NotifyIconWin::~NotifyIconWin() {
        this->Destroy();
    }

    bool NotifyIconWin::Create(std::shared_ptr<NotifyIconOptions> $options) {
        if (!this->created) {
            this->options = std::move($options);

            if (this->createWindowForIcon()) {
                if (this->updateNotifyIcon($options)) {
                    if (Shell_NotifyIcon(NIM_ADD, &this->iconData) == TRUE) {
                        Shell_NotifyIcon(NIM_SETVERSION, &this->iconData);
                        this->onCreate(true);
                        return true;
                    } else {
                        this->onCreate(false);
                    }
                }
            }
        } else {
            std::cerr << "Only one notify icon is enabled for application at this time. Use modify or destroy.\n";
        }
        return false;
    }

    bool NotifyIconWin::createWindowForIcon() {
        if (this->created) {
            return true;
        }

        const std::string windowTitle = "io-oidis-gui-notifyIcon";
        const std::string windowClassName = "io-oidis-gui-notifyIcon-class";

        WNDCLASSEX winClass;
        std::memset(&winClass, 0, sizeof(WNDCLASSEX));
        winClass.cbSize = sizeof(WNDCLASSEX);
        winClass.style = CS_HREDRAW | CS_VREDRAW;
        winClass.lpfnWndProc = WindowProcedure;
        winClass.cbClsExtra = 0;
        winClass.cbWndExtra = 0;
        winClass.hInstance = nullptr;
        this->loadIcon(&winClass.hIcon, this->options->getIconPath());
        winClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
        winClass.hbrBackground = (HBRUSH)(CreateSolidBrush(RGB(255, 255, 255)));
        winClass.lpszMenuName = nullptr;
        winClass.lpszClassName = reinterpret_cast<LPCSTR>(windowClassName.c_str());
        this->loadIcon(&winClass.hIconSm, this->options->getIconPath());

        if (!RegisterClassEx(&winClass)) {
            DWORD err = GetLastError();
            if (err != ERROR_CLASS_ALREADY_EXISTS) {
                return false;
            }
        }

        this->windowHandle = CreateWindowEx(
                0,
                reinterpret_cast<LPCSTR>(windowClassName.c_str()),
                reinterpret_cast<LPCSTR>(windowTitle.c_str()),
                WS_POPUP,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                5,
                5,
                nullptr,
                nullptr,
                nullptr,
                nullptr);
        SetWindowLongPtr(this->windowHandle, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

        ShowWindow(this->windowHandle, 0);
        return true;
    }

    bool NotifyIconWin::Destroy() {
        if (this->created) {
            if (Shell_NotifyIcon(NIM_DELETE, &this->iconData)) {
                DestroyMenu(this->hMenu);
                DestroyWindow(this->windowHandle);
                PostQuitMessage(0);
                this->onDestroy(true);
                return true;
            } else {
                this->onDestroy(false);
            }
        }
        return false;
    }

    bool NotifyIconWin::Modify(std::shared_ptr<NotifyIconOptions> $options) {
        if (!this->created) {
            return this->Create($options);
        } else {
            if (this->updateNotifyIcon($options)) {
                if (Shell_NotifyIcon(NIM_MODIFY, &this->iconData)) {
                    this->onModify(true);
                    return true;
                } else {
                    this->onModify(false);
                }
            }
        }
        return false;
    }

    bool NotifyIconWin::updateNotifyIcon(std::shared_ptr<NotifyIconOptions> $options) {
        if ($options) {
            this->options = std::move($options);
        }

        this->iconData.cbSize = sizeof(NOTIFYICONDATA);
        this->iconData.hWnd = this->windowHandle;
        this->iconData.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE | NIF_SHOWTIP;
        this->iconData.uCallbackMessage = WM_USER;
        this->iconData.uVersion = NOTIFYICON_VERSION_4;

        if (!this->options) {
            return false;
        }

        this->loadIcon(&this->iconData.hIcon, this->options->getIconPath());

        if (this->hMenu != nullptr) {
            DestroyMenu(this->hMenu);
        }
        this->hMenu = CreatePopupMenu();

        if (this->hMenu != nullptr) {
            for (int i = 0; i < static_cast<int>(this->options->getContextMenu().size()); i++) {
                AppendMenu(this->hMenu, MF_STRING, i + GetIndexModifier(), this->options->getContextMenu()[i].getId().c_str());
            }
        }

        string tip = this->options->getTip();
#ifdef UNICODE
        wcscpy_s(this->iconData.szTip, ARRAYSIZE(this->iconData.szTip), std::wstring(tip.begin(), tip.end()).c_str());
#else
        strcpy_s(this->iconData.szTip, ARRAYSIZE(this->iconData.szTip), tip.c_str());
#endif

        if (this->options->getBalloonIcon()) {
            string infoTitle = this->options->getBalloonIcon()->getTitle();
            if (!infoTitle.empty()) {
                if (infoTitle.size() >= sizeof(this->iconData.szInfoTitle) / sizeof(WCHAR)) {
                    std::cerr << "Notify icon info text exceeds maximum chars (" << infoTitle.size() << "/"
                              << sizeof(this->iconData.szInfoTitle) / sizeof(WCHAR) << ").";
                    return false;
                } else {
#ifdef UNICODE
                    wcscpy_s(this->iconData.szInfoTitle, ARRAYSIZE(this->iconData.szInfoTitle),
                             std::wstring(infoTitle.begin(), infoTitle.end()).c_str());
#else
                    strcpy_s(this->iconData.szInfoTitle, ARRAYSIZE(this->iconData.szInfoTitle), infoTitle.c_str());
#endif
                    this->iconData.uFlags |= NIF_INFO;
                    this->iconData.dwInfoFlags = NIIF_USER | NIIF_LARGE_ICON;

                    string info = this->options->getBalloonIcon()->getMessage();
                    if (!info.empty()) {
                        if (info.size() >= sizeof(this->iconData.szInfo) / sizeof(WCHAR)) {
                            std::cerr << "Notify icon info text exceeds maximum chars (" << info.size() << "/"
                                      << sizeof(this->iconData.szInfo) / sizeof(WCHAR) << ").";
                            return false;
                        } else {
#ifdef UNICODE
                            wcscpy_s(this->iconData.szInfo, ARRAYSIZE(this->iconData.szInfo),
                                     std::wstring(info.begin(), info.end()).c_str());
#else
                            strcpy_s(this->iconData.szInfo, ARRAYSIZE(this->iconData.szInfo), info.c_str());
#endif
                        }
                    }

                    auto type = this->options->getBalloonIcon()->getType();
                    if (type == NotifyBalloonIcon::NotifyBalloonIconType::NONE) {
                        this->iconData.dwInfoFlags &= ~NIIF_ICON_MASK;
                        this->iconData.dwInfoFlags |= NIIF_NONE;
                    } else if (type == NotifyBalloonIcon::NotifyBalloonIconType::INFO) {
                        this->iconData.dwInfoFlags &= ~NIIF_ICON_MASK;
                        this->iconData.dwInfoFlags |= NIIF_INFO;
                    } else if (type == NotifyBalloonIcon::NotifyBalloonIconType::WARNING) {
                        this->iconData.dwInfoFlags &= ~NIIF_ICON_MASK;
                        this->iconData.dwInfoFlags |= NIIF_WARNING;
                    } else if (type == NotifyBalloonIcon::NotifyBalloonIconType::ERROR_TYPE) {
                        this->iconData.dwInfoFlags &= ~NIIF_ICON_MASK;
                        this->iconData.dwInfoFlags |= NIIF_ERROR;
                    } else if (type == NotifyBalloonIcon::NotifyBalloonIconType::USER) {
                        this->iconData.dwInfoFlags &= ~NIIF_ICON_MASK;
                        this->iconData.dwInfoFlags |= NIIF_USER;

                        loadIcon(&this->iconData.hBalloonIcon, this->options->getBalloonIcon()->getUserIcon());
                    }

                    if (this->options->getBalloonIcon()->isNoSound()) {
                        this->iconData.dwInfoFlags |= NIIF_NOSOUND;
                    } else {
                        this->iconData.dwInfoFlags &= ~NIIF_NOSOUND;
                    }
                }
            }
        }

        return true;
    }

    void NotifyIconWin::loadIcon(HICON *$icon, const std::string &$path) {
        if (($icon != nullptr) && this->iconFileExist($path)) {
            *$icon = (HICON)LoadImageA(nullptr, reinterpret_cast<LPCSTR>($path.c_str()), IMAGE_ICON, 0, 0,
                                       LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_DEFAULTCOLOR);
        } else {
            std::cerr << "Failed to load icon: " << $path << "\n";
        }
    }

    LRESULT NotifyIconWin::WindowProcedure(HWND $hwnd, UINT $message, WPARAM $wParam, LPARAM $lParam) {
        auto *self = reinterpret_cast<NotifyIconWin *>(GetWindowLongPtr($hwnd, GWLP_USERDATA));
        if ((self == nullptr) || self->windowHandle != $hwnd) {
            return DefWindowProc($hwnd, $message, $wParam, $lParam);
        }

        switch ($message) {
            case WM_USER: {
                if ($lParam == WM_RBUTTONDOWN) {
                    POINT curPoint;
                    GetCursorPos(&curPoint);
                    SetForegroundWindow(self->windowHandle);

                    SendMessage(self->windowHandle, WM_INITMENUPOPUP, (WPARAM)self->hMenu, 0);
                    UINT clicked = TrackPopupMenu(self->hMenu,
                                                  TPM_RETURNCMD | TPM_NONOTIFY | TPM_RETURNCMD,
                                                  curPoint.x, curPoint.y, 0, $hwnd, nullptr);
                    if (clicked != 0) {
                        SendMessage(self->windowHandle, WM_COMMAND, clicked, 0);
                    }
                }
                break;
            }

            case WM_COMMAND: {
                if (HIWORD($lParam) == 0) {
                    auto id = static_cast<size_t>(LOWORD($wParam)) - NotifyIconBase::GetIndexModifier();
                    if (id < self->options->getContextMenu().size()) {
                        self->options->getContextMenu()[id].OnClick();
                    }
                }
                break;
            }
        }

        return DefWindowProc($hwnd, $message, $wParam, $lParam);
    }
}

#ifdef OVERRIDE_UNICODE
#pragma pop_macro("UNICODE")
#endif

#endif  // WIN_PLATFORM
