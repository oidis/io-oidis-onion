/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_GUI_HPP_
#define IO_OIDIS_ONION_GUI_GUI_HPP_

#if __cplusplus < 201703L
#   ifdef __clang__
#       pragma clang diagnostic push
#       pragma clang diagnostic ignored "-Wc++17-extensions"
#   endif
#endif

#include <string>
#include <memory>
#include <functional>
#include <iomanip>

using std::string;

#include "Environment/Handler.hpp"
#include "SplashScreen/SplashScreenBase.hpp"
#include "SplashScreen/SplashScreenFactory.hpp"
#include "NotifyIcon/NotifyIconBase.hpp"
#include "NotifyIcon/NotifyIconFactory.hpp"
#include "NotifyIcon/NotifyIconOptions.hpp"
#include "NotifyIcon/NotifyIconContextMenu.hpp"
#include "NotifyIcon/NotifyIconContextMenuItem.hpp"
#include "NotifyIcon/NotifyBalloonIcon.hpp"
#include "TaskBar/TaskBarBase.hpp"
#include "TaskBar/TaskBarFactory.hpp"

#if __cplusplus < 201703L
#   ifdef __clang__
#       pragma clang diagnostic pop
#   endif
#endif

#endif  // IO_OIDIS_ONION_GUI_GUI_HPP_
