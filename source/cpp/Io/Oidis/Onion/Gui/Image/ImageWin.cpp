/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../../../../../reference.hpp"

using std::string;

namespace Io::Oidis::Onion::Gui::Image {

    ImageWin::ImageWin(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent)
            : ImageBase($parent) {}

    void ImageWin::draw(void *$gdiGraphics) {
        if (this->getIsGif()) {
            if (this->getTick() == -1 || this->getTick() <= (this->getLoopDelay() * this->getNativeTick())) {
                GUID guid = Gdiplus::FrameDimensionTime;
                this->nativeImage->SelectActiveFrame(&guid, static_cast<unsigned>(this->getFrameIndex()));
                this->setTick(static_cast<int>(reinterpret_cast<unsigned *>(this->propertyItem[0].value)[this->getFrameIndex()] * 10));
                this->setFrameIndex((this->getFrameIndex() + 1) % this->getFramesCnt());
                this->setLoopDelay(1);
            } else {
                this->setLoopDelay(this->getLoopDelay() + 1);
            }
        }

        Gdiplus::ImageAttributes attributes;
        Gdiplus::Rect dest(this->getX(), this->getY(), this->getWidth(), this->getHeight());

        auto graphics = static_cast<Gdiplus::Graphics *>($gdiGraphics);
        if ((graphics == nullptr) || (this->nativeImage == nullptr)) {
            return;
        }
        graphics->DrawImage(this->nativeImage, dest);
    }

    void ImageWin::Load(const string &$path) {
        const auto endsWith = [](const std::string &$str, const std::string &$suffix) -> bool {
            return (($str.size() >= $suffix.size()) &&
                    (0 == $str.compare($str.size() - $suffix.size(), $suffix.size(), $suffix)));
        };

        this->setIsGif(false);
        if ($path.empty()) {
            return;
        }
        this->nativeImage = new Gdiplus::Image(std::wstring($path.begin(), $path.end()).c_str());

        // check gif
        if (endsWith($path, ".gif")) {
            this->setIsGif(true);
            unsigned count = this->nativeImage->GetFrameDimensionsCount();
            auto *dimIds = new GUID[count];
            this->nativeImage->GetFrameDimensionsList(dimIds, count);
            WCHAR strGuid[39];
            StringFromGUID2(dimIds[0], strGuid, 39);
            this->setFramesCnt(this->nativeImage->GetFrameCount(&dimIds[0]));

            unsigned total = this->nativeImage->GetPropertyItemSize(PropertyTagFrameDelay);

            if (this->propertyItem != nullptr) {
                free(this->propertyItem);
            }
            this->propertyItem = (Gdiplus::PropertyItem *)malloc(total);
            this->nativeImage->GetPropertyItem(PropertyTagFrameDelay, total, propertyItem);

            delete[] dimIds;
        }
    }

    ImageWin::~ImageWin() {
        if (this->propertyItem != nullptr) {
            free(this->propertyItem);
        }
    }
}

#endif  // WIN_PLATFORM
