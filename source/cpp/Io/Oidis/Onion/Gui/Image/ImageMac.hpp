/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_IMAGE_IMAGEMAC_HPP_
#define IO_OIDIS_ONION_GUI_IMAGE_IMAGEMAC_HPP_

#ifdef MAC_PLATFORM

#include "ImageBase.hpp"

@class ImageView;

namespace Io::Oidis::Onion::Gui::Image {
    class ImageMac : public ImageBase {
     public:
        explicit ImageMac(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent);

        void Load(const string &$path) override;

        void* getNative() const override;

        void Create() override;

        void setSize(int $width, int $height) override;

        void setPosition(int $x, int $y) override;

        void setSizeAndPosition(int $x, int $y, int $width, int $height) override;

        void setVisible(bool $visible) override;

     private:
        void draw(void *$args) override;

        ImageView *nativeImage;
    };
}

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_ONION_GUI_IMAGE_IMAGEMAC_HPP_
