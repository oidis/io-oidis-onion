/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_IMAGE_IMAGEBASE_HPP_
#define IO_OIDIS_ONION_GUI_IMAGE_IMAGEBASE_HPP_

#include "../Primitives/BaseGuiObject.hpp"

namespace Io::Oidis::Onion::Gui::Image {
    class ImageBase : public Gui::Primitives::BaseGuiObject {
     public:
        /**
         * Constructs default image and parent.
         */
        explicit ImageBase(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent);

        /**
         * Load image from file path. Supports each formats supported by Windows Gdiplus::Image.
         * @param $path Specify image file path.
         */
        virtual void Load(const std::string &$path) = 0;

        /**
         * Release internal data.
         */
        virtual ~ImageBase() = default;

        bool getIsGif() const;

        void setIsGif(bool $isGif);

        int getFrameIndex() const;

        void setFrameIndex(int $frameIndex);

        int getFramesCnt() const;

        void setFramesCnt(int $framesCnt);

        int getTick() const;

        void setTick(int $tick);

        int getLoopDelay() const;

        void setLoopDelay(int $loopDelay);

        int getNativeTick() const;

        double getScale() const;

        void setScale(double $scale);

        /**
         * Draw function, calls private function with platform-dependent parameters.
         * @param $args Platform dependent parameters (gtkwidget on linux, gdigraphics on windows...).
         */
        void Draw(void *$args) {
            this->draw($args);
        }

     protected:
        virtual void draw(void *$args) = 0;

     private:
        bool isGif = false;
        int frameIndex = 0;
        int framesCnt = 1;
        int tick = -1;
        int loopDelay = 0;
        const int nativeTick = 10;  // ms
        double scale = 1;
    };
}

#endif  // IO_OIDIS_ONION_GUI_IMAGE_IMAGEBASE_HPP_
