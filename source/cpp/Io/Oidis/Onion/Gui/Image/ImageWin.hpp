/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_IMAGE_IMAGEWIN_HPP_
#define IO_OIDIS_ONION_GUI_IMAGE_IMAGEWIN_HPP_

#ifdef WIN_PLATFORM

#include <windows.h>
#include <objidl.h>
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4458)  // declaration of 'xxx' hides class member
#endif
#include <gdiplus.h>
#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <string>
#include <memory>

#include "ImageBase.hpp"

namespace Io::Oidis::Onion::Gui::Image {
    class ImageWin : public ImageBase {
     public:
        explicit ImageWin(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent);

        /**
         * Release internal data.
         */
        virtual ~ImageWin();

        void Load(const std::string &$path) override;

     private:
        Gdiplus::Image *nativeImage = nullptr;
        Gdiplus::PropertyItem *propertyItem = nullptr;

        void draw(void *$gdiGraphics) override;
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_ONION_GUI_IMAGE_IMAGEWIN_HPP_
