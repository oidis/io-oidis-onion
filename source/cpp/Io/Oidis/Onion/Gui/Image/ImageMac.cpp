/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include <Cocoa/Cocoa.h>

#include "../../../../../reference.hpp"

// @formatter:off
@interface ImageView : NSImageView {
    NSImage *image;
}

@property(atomic, assign) bool visible;

- (void) LoadImage:(NSString*) $path;  // NOLINT(readability/casting)

- (void) Update;

@end

@implementation ImageView

- (void) LoadImage:(NSString*) $path {  // NOLINT(readability/casting)
    image = [[NSImage alloc] initWithContentsOfFile:$path];
}

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

- (void)drawRect:(NSRect)rect {
    NSGraphicsContext* theContext = [NSGraphicsContext currentContext];
    [theContext saveGraphicsState];

    if (self.visible) {
        [image drawInRect:rect];
    }

    [theContext restoreGraphicsState];
}

- (void) Update {
    if ([NSThread isMainThread]) {
        [self setNeedsDisplay: YES];
    } else {
        [self performSelectorOnMainThread:@selector(Update) withObject:nil waitUntilDone:NO];
    }
}
@end
// @formatter:on

namespace Io::Oidis::Onion::Gui::Image {

    ImageMac::ImageMac(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent)
            : ImageBase($parent) {}

    void ImageMac::Load(const std::string &$path) {
        // @formatter:off
//        if (!$path.empty()) {
//            std::ifstream image;
//            image.open(this->settings.imagePath, std::ios_base::in);
//            if (!image.is_open()) {
//                throw std::runtime_error{"ImageBase at " + this->settings.imagePath + " does not exist"};
//            }
//        }
        [this->nativeImage LoadImage:[NSString stringWithUTF8String:$path.c_str()]];
        // @formatter:on
    }

    void ImageMac::draw(void *$args) {
        (void)$args;

        // @formatter:off
        [this->nativeImage Update];
        // @formatter:on
    }

    void *ImageMac::getNative() const {
        return this->nativeImage;
    }

    void ImageMac::Create() {
        BaseGuiObject::Create();

        // @formatter:off
        this->nativeImage = [[ImageView alloc] initWithFrame: reinterpret_cast<NSWindow*>(this->getParent()->getNative()).frame];
        // @formatter:on
        this->nativeImage.imageScaling = NSImageScaleNone;
    }

    void ImageMac::setSize(int $width, int $height) {
        BaseGuiObject::setSize($width, $height);
        // @formatter:off
        [this->nativeImage setFrameSize: NSMakeSize($width, $height)];
        [this->nativeImage Update];
        // @formatter:on
    }

    void ImageMac::setPosition(int $x, int $y) {
        BaseGuiObject::setPosition($x, $y);
        // @formatter:off
        [this->nativeImage setFrameOrigin: NSMakePoint($x, $y)];
        [this->nativeImage Update];
        // @formatter:on
    }

    void ImageMac::setSizeAndPosition(int $x, int $y, int $width, int $height) {
        BaseGuiObject::setSizeAndPosition($x, $y, $width, $height);
        this->setPosition($x, $y);
        this->setSize($width, $height);
    }

    void ImageMac::setVisible(bool $visible) {
        BaseGuiObject::setVisible($visible);
        // @formatter:off
        [this->nativeImage setVisible:$visible];
        [this->nativeImage Update];
        // @formatter:on
    }
}

#endif  // MAC_PLATFORM
