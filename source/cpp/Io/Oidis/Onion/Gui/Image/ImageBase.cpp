/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::Image {

    ImageBase::ImageBase(const Io::Oidis::Onion::Gui::Primitives::BaseGuiObject *$parent)
            : BaseGuiObject($parent) {}

    bool ImageBase::getIsGif() const {
        return this->isGif;
    }

    void ImageBase::setIsGif(bool $isGif) {
        this->isGif = $isGif;
    }

    int ImageBase::getFrameIndex() const {
        return frameIndex;
    }

    void ImageBase::setFrameIndex(int $frameIndex) {
        this->frameIndex = $frameIndex;
    }

    int ImageBase::getFramesCnt() const {
        return this->framesCnt;
    }

    void ImageBase::setFramesCnt(int $framesCnt) {
        this->framesCnt = $framesCnt;
    }

    int ImageBase::getTick() const {
        return this->tick;
    }

    void ImageBase::setTick(int $tick) {
        this->tick = $tick;
    }

    int ImageBase::getLoopDelay() const {
        return this->loopDelay;
    }

    void ImageBase::setLoopDelay(int $loopDelay) {
        this->loopDelay = $loopDelay;
    }

    int ImageBase::getNativeTick() const {
        return this->nativeTick;
    }

    double ImageBase::getScale() const {
        return this->scale;
    }

    void ImageBase::setScale(double $scale) {
        this->scale = $scale;
    }
}
