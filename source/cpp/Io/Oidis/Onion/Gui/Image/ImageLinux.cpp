/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#if defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#include "../../../../../reference.hpp"

using std::string;

namespace Io::Oidis::Onion::Gui::Image {

    gboolean ImageLinux::doDraw(GtkWidget *$widget, GdkEventExpose *$event, gpointer $data) {
        (void)$event;
        auto self = reinterpret_cast<ImageLinux *>($data);

        if (!self->isVisible()) {
            return TRUE;
        }

        cairo_t *cr = gdk_cairo_create(gtk_widget_get_window($widget));
        if (!cr) {
            return TRUE;
        }

        if (!self->image) {
            return TRUE;
        }

        cairo_scale(cr, self->getScale(), 1);

        cairo_set_source_surface(cr, self->image, self->getX(), self->getY());
        cairo_paint(cr);

        return TRUE;
    }

    void ImageLinux::Load(const std::string &$path) {
        (void)$path;
        this->image = cairo_image_surface_create_from_png($path.c_str());
    }

    void ImageLinux::draw(void *$args) {
        (void)$args;
    }

    ImageLinux::~ImageLinux() {
        cairo_surface_destroy(this->image);
    }

    void *ImageLinux::getNative() const {
        return static_cast<void *>(this->image);
    }
}

#endif  // LINUX_PLATFORM
