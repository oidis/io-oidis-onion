/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_GUI_IMAGE_IMAGELINUX_HPP_
#define IO_OIDIS_ONION_GUI_IMAGE_IMAGELINUX_HPP_

#if defined(LINUX_PLATFORM) && !defined(ONION_NO_GUI)

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"

#include <gtk/gtk.h>

#pragma GCC diagnostic pop

#include <string>
#include <memory>

#include "ImageBase.hpp"

namespace Io::Oidis::Onion::Gui::Image {
    class ImageLinux : public ImageBase {
     public:
        using ImageBase::ImageBase;

        static gboolean doDraw(GtkWidget *$widget, GdkEventExpose *$event, gpointer $data);

        ~ImageLinux() override;

        void Load(const string &$path) override;

        void *getNative() const override;

     private:
        void draw(void *$args) override;

        cairo_surface_t *image = nullptr;
    };
}

#endif  // LINUX_PLATFORM

#endif  // IO_OIDIS_ONION_GUI_IMAGE_IMAGELINUX_HPP_
