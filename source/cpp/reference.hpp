/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_HPP_  // NOLINT
#define IO_OIDIS_ONION_RESOURCEMANAGER_HPP_

#include <string>
#include <vector>

// global-using-start
using std::string;
// global-using-stop

// generated-code-start
#include "interfacesMap.hpp"
#include "Io/Oidis/Onion/Filesystem/Utils.hpp"
#include "Io/Oidis/Onion/Gui/Environment/Handler.hpp"
#include "Io/Oidis/Onion/Gui/Gui.hpp"
#include "Io/Oidis/Onion/Gui/Image/ImageBase.hpp"
#include "Io/Oidis/Onion/Gui/Image/ImageFactory.hpp"
#include "Io/Oidis/Onion/Gui/Image/ImageLinux.hpp"
#include "Io/Oidis/Onion/Gui/Image/ImageMac.hpp"
#include "Io/Oidis/Onion/Gui/Image/ImageWin.hpp"
#include "Io/Oidis/Onion/Gui/Label/LabelBase.hpp"
#include "Io/Oidis/Onion/Gui/Label/LabelFactory.hpp"
#include "Io/Oidis/Onion/Gui/Label/LabelLinux.hpp"
#include "Io/Oidis/Onion/Gui/Label/LabelMac.hpp"
#include "Io/Oidis/Onion/Gui/Label/LabelWin.hpp"
#include "Io/Oidis/Onion/Gui/NotifyIcon/NotifyBalloonIcon.hpp"
#include "Io/Oidis/Onion/Gui/NotifyIcon/NotifyIconBase.hpp"
#include "Io/Oidis/Onion/Gui/NotifyIcon/NotifyIconContextMenu.hpp"
#include "Io/Oidis/Onion/Gui/NotifyIcon/NotifyIconContextMenuItem.hpp"
#include "Io/Oidis/Onion/Gui/NotifyIcon/NotifyIconFactory.hpp"
#include "Io/Oidis/Onion/Gui/NotifyIcon/NotifyIconLinux.hpp"
#include "Io/Oidis/Onion/Gui/NotifyIcon/NotifyIconMac.hpp"
#include "Io/Oidis/Onion/Gui/NotifyIcon/NotifyIconOptions.hpp"
#include "Io/Oidis/Onion/Gui/NotifyIcon/NotifyIconWin.hpp"
#include "Io/Oidis/Onion/Gui/Primitives/BaseGuiObject.hpp"
#include "Io/Oidis/Onion/Gui/ProgressBar/ProgressBarBase.hpp"
#include "Io/Oidis/Onion/Gui/ProgressBar/ProgressBarFactory.hpp"
#include "Io/Oidis/Onion/Gui/ProgressBar/ProgressBarLinux.hpp"
#include "Io/Oidis/Onion/Gui/ProgressBar/ProgressBarMac.hpp"
#include "Io/Oidis/Onion/Gui/ProgressBar/ProgressBarWin.hpp"
#include "Io/Oidis/Onion/Gui/SplashScreen/SplashScreenBase.hpp"
#include "Io/Oidis/Onion/Gui/SplashScreen/SplashScreenCmd.hpp"
#include "Io/Oidis/Onion/Gui/SplashScreen/SplashScreenFactory.hpp"
#include "Io/Oidis/Onion/Gui/SplashScreen/SplashScreenLinux.hpp"
#include "Io/Oidis/Onion/Gui/SplashScreen/SplashScreenMac.hpp"
#include "Io/Oidis/Onion/Gui/SplashScreen/SplashScreenWin.hpp"
#include "Io/Oidis/Onion/Gui/Structures/ColorBase.hpp"
#include "Io/Oidis/Onion/Gui/Structures/ColorFactory.hpp"
#include "Io/Oidis/Onion/Gui/Structures/ColorLinux.hpp"
#include "Io/Oidis/Onion/Gui/Structures/ColorMac.hpp"
#include "Io/Oidis/Onion/Gui/Structures/ColorWin.hpp"
#include "Io/Oidis/Onion/Gui/Structures/Font.hpp"
#include "Io/Oidis/Onion/Gui/TaskBar/TaskBarBase.hpp"
#include "Io/Oidis/Onion/Gui/TaskBar/TaskBarFactory.hpp"
#include "Io/Oidis/Onion/Gui/TaskBar/TaskBarMac.hpp"
#include "Io/Oidis/Onion/Gui/TaskBar/TaskBarWin.hpp"
#include "Io/Oidis/Onion/ResourceManager/Core/Protocol.hpp"
#include "Io/Oidis/Onion/ResourceManager/Core/Resource.hpp"
#include "Io/Oidis/Onion/ResourceManager/Logger/Logger.hpp"
#include "Io/Oidis/Onion/ResourceManager/Logger/LoggerConsole.hpp"
#include "Io/Oidis/Onion/ResourceManager/Logger/LoggerDevNull.hpp"
#include "Io/Oidis/Onion/ResourceManager/Logger/LogManager.hpp"
#include "Io/Oidis/Onion/ResourceManager/Reader/ReaderSettings.hpp"
#include "Io/Oidis/Onion/ResourceManager/Reader/ResourceReader.hpp"
#include "Io/Oidis/Onion/ResourceManager/Utils/BinaryReader.hpp"
#include "Io/Oidis/Onion/ResourceManager/Utils/ByteUtils.hpp"
#include "Io/Oidis/Onion/ResourceManager/Utils/CollectionUtils.hpp"
#include "Io/Oidis/Onion/ResourceManager/Utils/FileReader.hpp"
#include "Io/Oidis/Onion/ResourceManager/Utils/FileUtils.hpp"
#include "Io/Oidis/Onion/ResourceManager/Utils/FileWriter.hpp"
#include "Io/Oidis/Onion/ResourceManager/Writer/ResourceWriter.hpp"
#include "Io/Oidis/Onion/ResourceManager/Writer/WriterSettings.hpp"
// generated-code-end

#include "Io/Oidis/Onion/ResourceManager/ResourceManager.hpp"

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_HPP_ // NOLINT
