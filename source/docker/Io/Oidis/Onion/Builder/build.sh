#!/usr/bin/env bash
# * ********************************************************************************************************* *
# *
# * Copyright 2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

wui install --solution=shared || exit 1

wui test unit --solution=shared || exit 1

wui eap --solution=shared || exit 1

profile="${ONION_PROFILE:-eap}"

if [[ $profile == "prod" ]]; then
  wui target-deploy:prod --solution=shared || exit 1
else
  wui target-deploy:eap --solution=shared || exit 1
  # optional deploy to dev instance too
  wui target-deploy:dev --solution=shared || exit 1
fi
