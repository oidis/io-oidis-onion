# io-oidis-onion

> Multi-platform library focused on utils for handling of simple UI and content of binary files

## Requirements

This application depends on the [WUI Builder](https://gitlab.com/oidis/io-oidis-builder).
See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

As an extension there are prepared several helper run profiles for CLion IDE. So you can open this project in CLion 
and use Build or Build-Linux presets (both are posix relevant and -Linux one is executed in docker). 

## Project tests

Unit tests are located in [test/unit/cpp/Io/Oidis/Onion](test/unit/cpp/Io/Oidis/Onion) directory.
You can execute them with command below from project root directory.
```shell
wui test unit --solution=shared
```

## Documentation

This project provides automatically generated documentation in [Doxygen](http://www.doxygen.org/index.html) 
from the C++ source by running the `wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2020.0.0
Refactored resource reader/writer API into ResourceManager class. Added performance unit test for ResourceManager. Increased effectivity of
resources writing by std::for_each replacement by native range based for. The writer part is not fully finished for WIN because performance 
is poor on WIN, so use of ResourceHacker for writing of huge amount of resources is still recommended.
### v2019.3.0
Added basic GUI support for Win, Linux, OS X and headless progress for Linux and ARM. This gui is capable to show simple window with 
configurable background image and size. Also labels and progressbar could be added to window and controlled by prepared methods. 
Updated project config to solutions. 
### v2019.1.0
Added full implementation for Linux, ARM and OS X. Added caching mechanism and resource update schema.
### v2019.0.0
Initial version for NodejsRE integration.

## License

This software is owned or controlled by Oidis.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Karel Burda, 
Copyright 2019 [NXP](http://nxp.com/)
Copyright 2019-2022 [Oidis](https://www.oidis.org/)
