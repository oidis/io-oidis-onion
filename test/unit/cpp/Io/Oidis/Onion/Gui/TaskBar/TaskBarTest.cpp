/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <type_traits>

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::TaskBar {
#if !defined(ONION_NO_GUI)

    TEST(TaskBarTest, DISABLED_StaticAssertions) {
        static_assert((std::is_copy_constructible<TaskBarBase>::value == false) ||
                      (std::is_copy_assignable<TaskBarBase>::value == false),
                      "TaskBar should not be copy-able");
        static_assert((std::is_move_constructible<TaskBarBase>::value == false) ||
                      (std::is_move_assignable<TaskBarBase>::value == false),
                      "TaskBar should not be move-able");
    }

    TEST(TaskBarTest, DISABLED_Basic) {
#ifdef LINUX_PLATFORM
        EXPECT_EQ(TaskBarFactory::Create(nullptr), nullptr);
#else
        const auto taskBar = TaskBarFactory::Create(nullptr);
        ASSERT_NE(taskBar, nullptr);

        taskBar->setProgressValue(20, 100);
        taskBar->setProgressValue(0, 100);
        taskBar->setProgressValue(100, 100);
        EXPECT_THROW(taskBar->setProgressValue(101, 100), std::logic_error);
        EXPECT_THROW(taskBar->setProgressValue(-50, 100), std::logic_error);

        taskBar->setProgressState(TaskBarBase::TaskbarProgressState::NORMAL);
        taskBar->setProgressState(TaskBarBase::TaskbarProgressState::ERROR_STATE);
        taskBar->setProgressState(TaskBarBase::TaskbarProgressState::INDETERMINATE);
        taskBar->setProgressState(TaskBarBase::TaskbarProgressState::NOPROGRESS);
        taskBar->setProgressState(TaskBarBase::TaskbarProgressState::PAUSED);

        // deliberately multiple times
        for (unsigned char i = 0; i < 32; ++i) {
            taskBar->Clear();
        }
#endif
    }

#endif
}
