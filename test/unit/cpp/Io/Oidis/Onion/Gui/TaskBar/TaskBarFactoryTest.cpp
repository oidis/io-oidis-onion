/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::TaskBar {
    TEST(TaskBarFactoryTest, DISABLED_Factory) {
#ifdef MAC_PLATFORM
        ASSERT_NE(TaskBarFactory::Create(nullptr), nullptr);
#elif LINUX_PLATFORM
        ASSERT_EQ(TaskBarFactory::Create(nullptr), nullptr);
#elif WIN_PLATFORM
        ASSERT_NE(TaskBarFactory::Create(nullptr), nullptr);
#endif
    }
}
