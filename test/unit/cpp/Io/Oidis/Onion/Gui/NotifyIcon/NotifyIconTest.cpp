/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <type_traits>
#include <thread>
#include <future>

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::NotifyIcon {
#if !defined(ONION_NO_GUI)

    TEST(NotifyIconTest, DISABLED_StaticAssertions) {
        static_assert((std::is_copy_constructible<NotifyIconBase>::value == false) ||
                      (std::is_copy_assignable<NotifyIconBase>::value == false),
                      "NotifyIcon should not be copy-able");
        static_assert((std::is_move_constructible<NotifyIconBase>::value == false) ||
                      (std::is_move_assignable<NotifyIconBase>::value == false),
                      "NotifyIcon should not be move-able");
    }

    TEST(NotifyIconTest, DISABLED_Basic) {
        Environment::Handler::Init();

        std::shared_ptr<NotifyIconBase> notifyIcon = nullptr;
        notifyIcon.reset(NotifyIconFactory::Create());

        ASSERT_NE(notifyIcon, nullptr);

        auto options = std::make_shared<NotifyIconOptions>();
        options->setTip("Basic tip");
        options->setIconPath("test/resource/data/Io/Oidis/Onion/Gui/icon.ico");

        int infoCallbackCalled = -1;
        int exitCallbackCalled = -1;

        NotifyIconContextMenuItem infoItem{"info", "OnNotifyIconContextItemSelected",
                                           [&](const std::string &$id, const std::string &$name) {
                                               ASSERT_EQ($id, "OnNotifyIconContextItemSelected");
                                               ASSERT_EQ($name, "info");
                                               infoCallbackCalled++;
                                           }};
        NotifyIconContextMenuItem exitItem{"exit", "OnNotifyIconContextItemSelected",
                                           [&](const std::string &$id, const std::string &$name) {
                                               ASSERT_EQ($id, "OnNotifyIconContextItemSelected");
                                               ASSERT_EQ($name, "exit");
                                               exitCallbackCalled++;
                                           }};

        options->setContextMenu({});
        options->AddItem(infoItem);
        options->AddItem(exitItem);

        ASSERT_TRUE(notifyIcon->Create(options));
        Environment::Handler::Start([&]() -> int {
            auto future = std::async(std::launch::async, [&]() {
                // deliberately multiple times
                ASSERT_EQ(infoCallbackCalled, -1);
                for (unsigned char i = 0; i < 8; ++i) {
                    infoItem.OnClick();
                }
                ASSERT_EQ(infoCallbackCalled, 7);
                ASSERT_EQ(exitCallbackCalled, -1);

                // deliberately multiple times
                for (unsigned char i = 0; i < 8; ++i) {
                    exitItem.OnClick();
                }

                std::this_thread::sleep_for(std::chrono::seconds(4));

                ASSERT_EQ(exitCallbackCalled, 7);

                // deliberately multiple times
                for (unsigned char i = 0; i < 8; ++i) {
                    notifyIcon->Destroy();
                }

                Environment::Handler::Stop();
                notifyIcon.reset();
            });
            return 0;
        });
    }

    TEST(NotifyIconTest, DISABLED_Modify) {
        Environment::Handler::Init();

        NotifyIconBase *notifyIcon = NotifyIconFactory::Create();

        ASSERT_NE(notifyIcon, nullptr);

        auto options = std::make_shared<NotifyIconOptions>();
        options->setTip("Modify tip");
        options->setIconPath("test/resource/data/Io/Oidis/Onion/Gui/icon.ico");

        int item1CallbackCalled = -1;
        int item2CallbackCalled = -1;
        int item3CallbackCalled = -1;

        NotifyIconContextMenuItem item1{"item1", "OnNotifyIconContextItemSelected",
                                        [&](const std::string &$id, const std::string &$name) {
                                            ASSERT_EQ($id, "OnNotifyIconContextItemSelected");
                                            ASSERT_EQ($name, "item1");
                                            item1CallbackCalled++;
                                        }};
        NotifyIconContextMenuItem item2{"item2", "OnNotifyIconContextItemSelected",
                                        [&](const std::string &$id, const std::string &$name) {
                                            ASSERT_EQ($id, "OnNotifyIconContextItemSelected");
                                            ASSERT_EQ($name, "item2");
                                            item2CallbackCalled++;
                                        }};
        NotifyIconContextMenuItem item3{"item3", "OnNotifyIconContextItemSelected",
                                        [&](const std::string &$id, const std::string &$name) {
                                            ASSERT_EQ($id, "OnNotifyIconContextItemSelected");
                                            ASSERT_EQ($name, "item3");
                                            item3CallbackCalled++;
                                        }};

        options->setContextMenu({});
        options->AddItem(item1);
        options->AddItem(item2);

        Environment::Handler::Start([&]() -> int {
            auto future = std::async(std::launch::async, [&]() {
                ASSERT_TRUE(notifyIcon->Create(options));
                // deliberately multiple times
                ASSERT_EQ(item1CallbackCalled, -1);
                for (unsigned char i = 0; i < 8; ++i) {
                    item1.OnClick();
                }
                ASSERT_EQ(item1CallbackCalled, 7);
                ASSERT_EQ(item2CallbackCalled, -1);

                // deliberately multiple times
                for (unsigned char i = 0; i < 8; ++i) {
                    item2.OnClick();
                }

                std::this_thread::sleep_for(std::chrono::seconds(2));

                ASSERT_EQ(item2CallbackCalled, 7);
                ASSERT_EQ(item3CallbackCalled, -1);
                options->AddItem(item3);
                options->setTip("xxofjdslsflkjd");
                notifyIcon->Modify(options);

                for (int i = 0; i < 6; i++) {
                    item3.OnClick();
                }
                std::this_thread::sleep_for(std::chrono::seconds(2));
                ASSERT_EQ(item3CallbackCalled, 5);

                // deliberately multiple times
                for (unsigned char i = 0; i < 8; ++i) {
                    notifyIcon->Destroy();
                }

                Environment::Handler::Stop();
                delete notifyIcon;
            });
            return 0;
        });
    }

    TEST(NotifyIconTest, DISABLED_Setters) {
        Environment::Handler::Init();

        std::shared_ptr<NotifyIconBase> notifyIcon = nullptr;
        notifyIcon.reset(NotifyIconFactory::Create());

        ASSERT_NE(notifyIcon, nullptr);

        auto options = std::make_shared<NotifyIconOptions>();
        options->setTip("Basic tip");
        options->setIconPath("test/resource/data/Io/Oidis/Onion/Gui/icon.ico");

        bool infoCallbackCalled = false;
        bool exitCallbackCalled = false;

        NotifyIconContextMenuItem infoItem;
        infoItem.setName("info");
        infoItem.setId("OnNotifyIconContextItemSelected");
        infoItem.setCallback([&](const std::string &$id, const std::string &$name) {
            ASSERT_EQ($id, "OnNotifyIconContextItemSelected");
            ASSERT_EQ($name, "info");
            infoCallbackCalled = true;
        });

        NotifyIconContextMenuItem exitItem;
        exitItem.setName("exit");
        exitItem.setId("OnNotifyIconContextItemSelected");
        exitItem.setCallback([&](const std::string &$id, const std::string &$name) {
            ASSERT_EQ($id, "OnNotifyIconContextItemSelected");
            ASSERT_EQ($name, "exit");
            exitCallbackCalled = true;
        });

        options->setContextMenu({});
        options->AddItem(infoItem);
        options->AddItem(exitItem);

        Environment::Handler::Start([&]() -> int {
            auto future = std::async(std::launch::async, [&]() {
                ASSERT_TRUE(notifyIcon->Create(options));
                // deliberately multiple times
                for (unsigned char i = 0; i < 8; ++i) {
                    infoItem.OnClick();
                }

                ASSERT_TRUE(infoCallbackCalled);
                ASSERT_FALSE(exitCallbackCalled);

                options->setContextMenu({});
                notifyIcon->Modify(options);

                // deliberately multiple times
                for (unsigned char i = 0; i < 8; ++i) {
                    exitItem.OnClick();
                }
                ASSERT_TRUE(exitCallbackCalled);

                std::this_thread::sleep_for(std::chrono::seconds(2));

                // deliberately multiple times
                for (unsigned char i = 0; i < 8; ++i) {
                    notifyIcon->Destroy();
                }

                Environment::Handler::Stop();
                notifyIcon.reset();
            });
            return 0;
        });
    }

    TEST(NotifyIconTest, DISABLED_OptionsOutOfScope) {
        Environment::Handler::Init();

        std::shared_ptr<NotifyIconBase> notifyIcon = nullptr;
        notifyIcon.reset(NotifyIconFactory::Create());

        ASSERT_NE(notifyIcon, nullptr);

        bool cbCalled = false;

        Environment::Handler::Start([&]() -> int {
            auto future = std::async(std::launch::async, [&]() {
                NotifyIconContextMenuItem infoItem;

                {
                    auto options = std::make_shared<NotifyIconOptions>();
                    options->setTip("Basic tip");
                    options->setIconPath("test/resource/data/Io/Oidis/Onion/Gui/icon.ico");

                    infoItem.setName("info");
                    infoItem.setId("OnNotifyIconContextItemSelected");
                    infoItem.setCallback([&](const std::string &$id, const std::string &$name) {
                        ASSERT_EQ($id, "OnNotifyIconContextItemSelected");
                        ASSERT_EQ($name, "info");
                        cbCalled = true;
                    });

                    options->AddItem(infoItem);

                    ASSERT_TRUE(notifyIcon->Create(options));
                }

                infoItem.OnClick();
                ASSERT_TRUE(cbCalled);

                notifyIcon->Modify(nullptr);
                infoItem.OnClick();

                auto options = std::make_shared<NotifyIconOptions>();
                notifyIcon->Modify(options);
                infoItem.OnClick();

                // deliberately multiple times
                for (unsigned char i = 0; i < 8; ++i) {
                    notifyIcon->Destroy();
                }

                Environment::Handler::Stop();
                notifyIcon.reset();
            });
            return 0;
        });
    }

    TEST(NotifyIconTest, DISABLED_WindowsBalloon) {
#ifndef WIN_PLATFORM
        return;
#else
        Environment::Handler::Init();

        std::shared_ptr<NotifyIconBase> notifyIcon = nullptr;
        notifyIcon.reset(NotifyIconFactory::Create());

        ASSERT_NE(notifyIcon, nullptr);

        bool cbCalled = false;

        Environment::Handler::Start([&]() -> int {
            auto future = std::async(std::launch::async, [&]() {
                NotifyIconContextMenuItem infoItem;

                auto options = std::make_shared<NotifyIconOptions>();

                options->setTip("Basic tip");
                options->setIconPath("test/resource/data/Io/Oidis/Onion/Gui/icon.ico");

                {
                    infoItem.setName("info");
                    infoItem.setId("OnNotifyIconContextItemSelected");
                    infoItem.setCallback([&](const std::string &$id, const std::string &$name) {
                        ASSERT_EQ($id, "OnNotifyIconContextItemSelected");
                        ASSERT_EQ($name, "info");
                        cbCalled = true;
                    });

                    options->AddItem(infoItem);
                }

                {
                    auto balloon = std::make_shared<NotifyBalloonIcon>();
                    balloon->setMessage("test ballon message");
                    balloon->setTitle("test ballon title");
                    balloon->setType(NotifyBalloonIcon::NotifyBalloonIconType::INFO);
                    balloon->setNoSound(false);

                    options->setBalloonIcon(balloon);
                }

                ASSERT_TRUE(notifyIcon->Create(options));

                infoItem.OnClick();
                ASSERT_TRUE(cbCalled);

                notifyIcon->Modify(nullptr);
                infoItem.OnClick();

                notifyIcon->Destroy();

                Environment::Handler::Stop();
                notifyIcon.reset();
            });
            return 0;
        });
#endif
    }

#endif
}
