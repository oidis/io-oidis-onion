/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::NotifyIcon {
    TEST(NotifyIconFactoryTest, DISABLED_Factory) {
#ifdef MAC_PLATFORM
        ASSERT_NE(NotifyIconFactory::Create(), nullptr);
#elif LINUX_PLATFORM
#if !defined(ONION_NO_GUI)
        ASSERT_NE(NotifyIconFactory::Create(), nullptr);
#else
        ASSERT_EQ(NotifyIconFactory::Create(), nullptr);
#endif
#elif WIN_PLATFORM
        ASSERT_NE(NotifyIconFactory::Create(), nullptr);
#endif
    }
}
