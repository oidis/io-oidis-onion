/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <type_traits>
#include <thread>
#include <future>

#include "../../../../../reference.hpp"

namespace Io::Oidis::Onion::Gui::SplashScreen {
#if !defined(ONION_NO_GUI)
    int speedFactor = 50;  // use higher value to slow-down test

    TEST(SplashScreenTest, DISABLED_StaticAssertions) {
        static_assert((std::is_copy_constructible<SplashScreenBase>::value == false) ||
                      (std::is_copy_assignable<SplashScreenBase>::value == false),
                      "SplashScreen should not be copy-able");
        static_assert((std::is_move_constructible<SplashScreenBase>::value == false) ||
                      (std::is_move_assignable<SplashScreenBase>::value == false),
                      "SplashScreen should not be move-able");
    }

    TEST(SplashScreenTest, DISABLED_InvalidCreate) {
        Environment::Handler::Init();
        ASSERT_THROW(SplashScreenFactory::Create({0, 0, 1024, -1, ""})->Create(), std::range_error);
        ASSERT_THROW(SplashScreenFactory::Create({0, 0, -1, 1024, ""})->Create(), std::range_error);
        ASSERT_THROW(SplashScreenFactory::Create({0, -2, 1024, 1024, ""})->Create(), std::range_error);
        ASSERT_THROW(SplashScreenFactory::Create({-2, 0, 1024, 1024, ""})->Create(), std::range_error);
        ASSERT_THROW(SplashScreenFactory::Create({-2, -2, 1024, 1024, ""})->Create(), std::range_error);
        Environment::Handler::Stop();
    }

    TEST(SplashScreenTest, DISABLED_DummyClose) {
        Environment::Handler::Init();
        SplashScreenBase *splashScreen = SplashScreenFactory::Create({0, 0, 1, 1, ""});
        int ex = Environment::Handler::Start([&]() -> int {
            splashScreen->Create();
            std::this_thread::sleep_for(std::chrono::milliseconds{500});
            Environment::Handler::Stop();
            delete splashScreen;
            return 0;
        });

        ASSERT_EQ(ex, 0);
    }

    TEST(SplashScreenTest, DISABLED_Basic) {
        Environment::Handler::Init();

        SplashScreenBase::SplashScreenSettings settings = {0, 0, 456, 312,
                                                           "test/resource/data/Io/Oidis/Onion/Gui/Background.png"};
        SplashScreenBase *splashScreen = SplashScreenFactory::Create(settings);

        ASSERT_NE(splashScreen, nullptr);
        Environment::Handler::Start([&]() -> int {
            splashScreen->Create();
            auto fcnWrap = [&]() {
                int testCounterOpen = 0;
                int testCounterClose = 0;

                splashScreen->setOnOpenCallback([&testCounterOpen]() {
                    std::this_thread::sleep_for(std::chrono::milliseconds{2000});
                    testCounterOpen++;
                });
                splashScreen->setOnCloseCallback([&testCounterClose]() {
                    std::this_thread::sleep_for(std::chrono::milliseconds{500});
                    testCounterClose++;
                });

                const int reps = 2;
                for (int i = 0; i < reps; i++) {
                    splashScreen->Show();
                    splashScreen->Hide();
                }

                ASSERT_EQ(testCounterClose, testCounterOpen);
                ASSERT_EQ(testCounterClose, reps);
                Environment::Handler::Stop();
                delete splashScreen;
            };
            fcnWrap();
            return 0;
        });
    }

    TEST(SplashScreenTest, DISABLED_ProgressBar_Color) {
        Environment::Handler::Init();
        SplashScreenBase::SplashScreenSettings settings = {100, 100, 456, 312,
                                                           "test/resource/data/Io/Oidis/Onion/Gui/BackgroundTransparent.png"};
        SplashScreenBase *splashScreen = SplashScreenFactory::Create(settings);
        ASSERT_NE(splashScreen, nullptr);

        Environment::Handler::Start([&]() -> int {
            splashScreen->Create();
            splashScreen->setProgressBar({2,
                                          312 - 15 - 5,
                                          456 - 4,
                                          15, "#ffff00", "#00ffff", true, false});
            auto future = std::async(std::launch::async, [&]() {
                splashScreen->setOnOpenCallback([&splashScreen]() {
                    std::this_thread::sleep_for(std::chrono::milliseconds{speedFactor});
                    for (int i = 1; i <= 100; i++) {
                        splashScreen->setProgressBarValue(i);
                        splashScreen->Update();
                        std::this_thread::sleep_for(std::chrono::milliseconds{speedFactor});
                    }
                });

                splashScreen->Show();
                splashScreen->Hide();
                std::this_thread::sleep_for(std::chrono::milliseconds{500});
                splashScreen->Show();

                Environment::Handler::Stop();
                delete splashScreen;
            });
            return 0;
        });
    }

    TEST(SplashScreenTest, DISABLED_ProgressBar_Image) {
        Environment::Handler::Init();
        // show in center of screen
        SplashScreenBase::SplashScreenSettings settings = {-1, -1, 456, 312,
                                                           "test/resource/data/Io/Oidis/Onion/Gui/Background.png"};
        SplashScreenBase *splashScreen = SplashScreenFactory::Create({});
        ASSERT_NE(splashScreen, nullptr);

        Environment::Handler::Start([&]() -> int {
            splashScreen->setSettings(settings);
            splashScreen->Create();
            splashScreen->setProgressBar({2,
                                          312 - 15 - 5,
                                          456 - 4,
                                          15,
                                          "test/resource/data/Io/Oidis/Onion/Gui/bar_fill.png",
                                          "test/resource/data/Io/Oidis/Onion/Gui/bar_border.png",
                                          true, false});
            auto future = std::async(std::launch::async, [&]() {
                splashScreen->setOnOpenCallback([&splashScreen]() {
                    std::this_thread::sleep_for(std::chrono::milliseconds{speedFactor});
                    for (int i = 1; i <= 100; i++) {
                        splashScreen->setProgressBarValue(i);

                        splashScreen->Update();
                        std::this_thread::sleep_for(std::chrono::milliseconds{speedFactor});
                    }
                    splashScreen->Hide();
                });

                splashScreen->Show();

                Environment::Handler::Stop();
                delete splashScreen;
            });
            return 0;
        });
    }

    TEST(SplashScreenTest, DISABLED_Labels) {
        Environment::Handler::Init();
        SplashScreenBase::SplashScreenSettings settings = {-1, -1, 456, 312,
                                                           "test/resource/data/Io/Oidis/Onion/Gui/Background.png"};
        SplashScreenBase *splashScreen = SplashScreenFactory::Create({});
        ASSERT_NE(splashScreen, nullptr);

        Environment::Handler::Start([&]() -> int {
            splashScreen->setSettings(settings);
            splashScreen->Create();
            splashScreen->setProgressLabel(
                    {
                            50, 40, 150, 40,
                            "", 33,
                            Io::Oidis::Onion::Gui::Label::LabelBase::TextJustify::LEFT, "#ff0000", true, false});

            splashScreen->setStatusLabel(
                    {
                            150, 80, 150, 40,
                            "", 33,
                            Io::Oidis::Onion::Gui::Label::LabelBase::TextJustify::CENTER, "#0000ff", true, false});

            auto future = std::async(std::launch::async, [&]() {
                splashScreen->setOnOpenCallback([&splashScreen]() {
                    std::this_thread::sleep_for(std::chrono::milliseconds{speedFactor});
                    for (int i = 1; i <= 100; i++) {
                        splashScreen->setProgressLabelText(std::to_string(25 * i));
                        splashScreen->setStatusLabelText(std::to_string(2 * i));
                        splashScreen->Update();
                        std::this_thread::sleep_for(std::chrono::milliseconds{speedFactor});
                    }
                    splashScreen->Hide();
                });

                splashScreen->Show();

                Environment::Handler::Stop();
                delete splashScreen;
            });
            return 0;
        });
    }

    TEST(SplashScreenTest, DISABLED_Move) {
        Environment::Handler::Init();
        SplashScreenBase::SplashScreenSettings settings = {0, 0, 456, 312,
                                                           "test/resource/data/Io/Oidis/Onion/Gui/BackgroundTransparent.png"};
        SplashScreenBase *splashScreen = SplashScreenFactory::Create(settings);
        ASSERT_NE(splashScreen, nullptr);

        Environment::Handler::Start([&]() -> int {
            splashScreen->Create();
            splashScreen->setProgressBar({2,
                                          312 - 15 - 5,
                                          456 - 4,
                                          15,
                                          "test/resource/data/Io/Oidis/Onion/Gui/bar_fill.png",
                                          "test/resource/data/Io/Oidis/Onion/Gui/bar_border.png",
                                          true, false});
            auto future = std::async(std::launch::async, [&]() {
                splashScreen->setOnOpenCallback([&splashScreen]() {
                    std::this_thread::sleep_for(std::chrono::milliseconds{speedFactor});
                    for (int i = 1; i <= 100; i++) {
                        splashScreen->setProgressBarValue(i);
                        splashScreen->setPosition(i * 2, i * 2);
                        splashScreen->setSize((i % 20) + 456, (i % 20) + 312);

                        splashScreen->Update();
                        std::this_thread::sleep_for(std::chrono::milliseconds{speedFactor});
                    }
                    splashScreen->Hide();
                });

                splashScreen->Show();

                Environment::Handler::Stop();
                delete splashScreen;
            });
            return 0;
        });
    }

    TEST(SplashScreenTest, DISABLED_DoubleCreate) {
        Environment::Handler::Init();
        SplashScreenBase::SplashScreenSettings settings = {-1, -1, 456, 312,
                                                           "test/resource/data/Io/Oidis/Onion/Gui/BackgroundTransparent.png"};
        SplashScreenBase *splashScreen = SplashScreenFactory::Create(settings);
        ASSERT_NE(splashScreen, nullptr);

        Environment::Handler::Start([&]() -> int {
            splashScreen->Create();
            splashScreen->Create();
            splashScreen->setProgressBar({2,
                                          312 - 15 - 5,
                                          456 - 4,
                                          15,
                                          "test/resource/data/Io/Oidis/Onion/Gui/bar_fill.png",
                                          "test/resource/data/Io/Oidis/Onion/Gui/bar_border.png",
                                          true, false});
            auto future = std::async(std::launch::async, [&]() {
                splashScreen->setOnOpenCallback([&]() {
                    std::this_thread::sleep_for(std::chrono::milliseconds{speedFactor});
                    for (int i = 1; i <= 100; i++) {
                        splashScreen->setProgressBarValue(i);

                        splashScreen->Update();
                        std::this_thread::sleep_for(std::chrono::milliseconds{speedFactor});
                    }
                    splashScreen->Hide();
                });

                splashScreen->Show();

                Environment::Handler::Stop();
                delete splashScreen;
            });
            return 0;
        });
    }

#endif
}
