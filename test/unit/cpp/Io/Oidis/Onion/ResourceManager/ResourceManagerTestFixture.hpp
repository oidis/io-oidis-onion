/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEMANAGER_TEST_RESOURCEMANAGERTESTFIXTURE_HPP_  // NOLINT
#define IO_OIDIS_ONION_RESOURCEMANAGER_TEST_RESOURCEMANAGERTESTFIXTURE_HPP_

#ifdef WIN_PLATFORM

#include <Windows.h>

#endif

#include <cstdio>
#include <memory>
#include <fstream>
#include <vector>

#include "../../../../reference.hpp"

namespace Io::Oidis::Onion::ResourceManager {
    class ResourceManagerTest : public ::testing::Test {
     protected:
        const size_t numberOfResources = 256;
#ifndef WIN_PLATFORM
        const char *smallBinaryPath = "small_binary";
        const char *bigBinaryPath = "big_binary";
#else
        const char *smallBinaryPath = "small_binary.exe";
        const char *bigBinaryPath = "big_binary.exe";
#endif

        const char *longTextualResource = "test/resource/data/Io/Oidis/Onion/ResourceManager/textual_lf.txt";
        Core::Resource::ResourceData longTextualResourceContent = {0};

        const char *shortTextualResource = "test/resource/data/Io/Oidis/Onion/ResourceManager/textual_short.txt";
        Core::Resource::ResourceData shortTextualResourceContent = {0};

        const char *binaryResource = "test/resource/data/Io/Oidis/Onion/ResourceManager/dummy.bin";
        Core::Resource::ResourceData binaryResourceContent = {0};

     private:
        void SetUp() override {
            // to enable console logs: Logger::LogManager::setLogger(Logger::LoggerConsole{});
            Logger::LogManager::setLogger(Logger::LoggerDevNull{});

            this->createBinaries();

            this->readTestResources();
        }

        void TearDown() override {
            this->checkBinaries();

            std::remove(ResourceManagerTest::smallBinaryPath);
            std::remove(ResourceManagerTest::bigBinaryPath);
        }

        void createBinaries() {
#ifdef WIN_PLATFORM
            if (CopyFile("test/resource/data/Io/Oidis/Onion/ResourceManager/small_binary_x64.exe",
                    ResourceManagerTest::smallBinaryPath, FALSE) != TRUE) {
                throw std::runtime_error("Failed to copy test resource file.");
            }
            if (CopyFile("test/resource/data/Io/Oidis/Onion/ResourceManager/big_binary_x64.exe",
                    ResourceManagerTest::bigBinaryPath, FALSE) != TRUE) {
                throw std::runtime_error("Failed to copy test resource file.");
            }
#else
            this->createBinary(ResourceManagerTest::smallBinaryPath, 1);
            this->createBinary(ResourceManagerTest::bigBinaryPath, 1024 * 1024);
#endif
        }

        void createBinary(const std::string &$filePath, const size_t $size) {
            std::ofstream binaryStream{$filePath, std::ios::out | std::ios::binary};

            if (!binaryStream.is_open()) {
                throw std::runtime_error{"Failed to open " + $filePath + " for writing"};
            }

            std::vector<char> bytesToWrite($size, 1);
            if (!binaryStream.write(&bytesToWrite[0], bytesToWrite.size())) {
                throw std::runtime_error{"Failed to write " + std::to_string($size) + " bytes"};
            }
        }

        void checkBinaries() {
            this->checkBinary(ResourceManagerTest::smallBinaryPath, 1);
            this->checkBinary(ResourceManagerTest::bigBinaryPath, 1024 * 1024);
        }

        void checkBinary(const std::string &$filePath, const size_t $bytesToRead) {
#ifndef WIN_PLATFORM
            std::ifstream binaryStream{$filePath, std::ios::in | std::ios::binary};

            if (!binaryStream.is_open()) {
                throw std::runtime_error{"Failed to open " + $filePath + " for reading"};
            }

            std::vector<char> bytesToRead($bytesToRead, 0);
            if (!binaryStream.read(&bytesToRead[0], bytesToRead.size())) {
                throw std::runtime_error{"Failed to read " + std::to_string($bytesToRead) + " bytes"};
            }

            for (size_t i = 0; i < bytesToRead.size(); ++i) {
                if (bytesToRead[i] != 1) {
                    throw std::runtime_error{"Binary at path " + $filePath + " has incorrect content on byte " + std::to_string(i)};
                }
            }
#endif
        }

        void readTestResources() {
            std::ifstream textualResourceStream{longTextualResource, std::ios::in | std::ios::binary};
            std::ifstream shortTextualResourceStream{shortTextualResource, std::ios::in | std::ios::binary};
            std::ifstream binaryResourceStream{binaryResource, std::ios::in | std::ios::binary};

            if (!textualResourceStream.is_open()) {
                throw std::runtime_error{"Failed to read textual resource"};
            }
            if (!shortTextualResourceStream.is_open()) {
                throw std::runtime_error{"Failed to read textual resource"};
            }
            if (!binaryResourceStream.is_open()) {
                throw std::runtime_error{"Failed to read binary resource"};
            }

            ResourceManagerTest::longTextualResourceContent = decltype(ResourceManagerTest::longTextualResourceContent)
                    {(std::istreambuf_iterator<char>(textualResourceStream)), std::istreambuf_iterator<char>()};

            ResourceManagerTest::shortTextualResourceContent = decltype(ResourceManagerTest::shortTextualResourceContent)
                    {(std::istreambuf_iterator<char>(shortTextualResourceStream)), std::istreambuf_iterator<char>()};

            ResourceManagerTest::binaryResourceContent = decltype(ResourceManagerTest::binaryResourceContent)
                    {(std::istreambuf_iterator<char>(binaryResourceStream)), std::istreambuf_iterator<char>()};
        }
    };
}

#endif  // IO_OIDIS_ONION_RESOURCEMANAGER_TEST_RESOURCEMANAGERTESTFIXTURE_HPP_  NOLINT
