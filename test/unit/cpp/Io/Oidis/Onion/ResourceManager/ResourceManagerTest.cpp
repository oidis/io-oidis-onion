/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <algorithm>
#include <regex>
#include "../../../../reference.hpp"

#include "ResourceManagerTestFixture.hpp"

testing::AssertionResult assertData(const char *$actualExpr, const char *$expectedExpr,
                                    const Io::Oidis::Onion::ResourceManager::Core::Resource::ResourceData &$actual,
                                    const Io::Oidis::Onion::ResourceManager::Core::Resource::ResourceData &$expected) {
    testing::Message msg;
    string addonMessage;
    if ($actual.size() == $expected.size()) {
        std::vector<std::size_t> wrongItems{};
        for (std::size_t i = 0; i < $actual.size(); i++) {
            if ($actual[i] != $expected[i]) {
                if (wrongItems.empty()) {
                    addonMessage = std::to_string(i);
                } else {
                    addonMessage += ", " + std::to_string(i);
                }
                wrongItems.emplace_back(i);
            }
        }
        if (wrongItems.empty()) {
            return testing::AssertionSuccess();
        }
        addonMessage = "Wrong items: [" + addonMessage + "]";
    } else {
        addonMessage = "Size not match";
    }
    auto printArray = [](const Io::Oidis::Onion::ResourceManager::Core::Resource::ResourceData &$data) -> string {
        std::ostringstream oss;
        oss << "[";
        for (std::size_t i = 0; i < $data.size(); i++) {
            oss << std::hex << static_cast<int>($data[i]);
            if (i < $data.size() - 1) {
                oss << ", ";
            }
        }
        oss << "]";
        return oss.str();
    };
    msg << $actualExpr << std::endl <<
        "Which is: " << printArray($actual) << std::endl <<
        $expectedExpr << std::endl <<
        "Which is: " << printArray($expected);
    if (!addonMessage.empty()) {
        msg << std::endl <<
            "info: " << addonMessage;
    }
    return testing::AssertionFailure(msg);
}

#define ASSERT_EQ_DATA($actual, $expected) ASSERT_PRED_FORMAT2(assertData, $actual, $expected)

namespace Io::Oidis::Onion::ResourceManager {
    using Internal::StopWatch;
    using Io::Oidis::Onion::ResourceManager::Core::Resource;
    using Io::Oidis::Onion::ResourceManager::Core::ResourceMap;

    TEST_F(ResourceManagerTest, SmallBinary) {
        ResourceManager manager(ResourceManagerTest::smallBinaryPath);
        manager.Write({{"dummy1", Resource{ResourceManagerTest::longTextualResource}}});
        manager.Write({{"dummy2", Resource{ResourceManagerTest::binaryResource, Resource::CompressionType::ZIP}}});

        auto dummy1 = manager.Read("dummy1", "");
        ASSERT_EQ(dummy1.getName(), "");
        ASSERT_EQ(dummy1.getCompression(), Resource::CompressionType::NONE);
        ASSERT_TRUE(dummy1.getContentPath().empty());
        ASSERT_EQ_DATA(dummy1.getData(), ResourceManagerTest::longTextualResourceContent);

        auto dummy2 = manager.Read("dummy2", "");
        ASSERT_EQ(dummy2.getName(), "");
        ASSERT_EQ(dummy2.getCompression(), Resource::CompressionType::ZIP);
        ASSERT_TRUE(dummy2.getContentPath().empty());
        ASSERT_EQ_DATA(dummy2.getData(), ResourceManagerTest::binaryResourceContent);

        manager.Write(
                {{"dummy2", Resource{ResourceManagerTest::longTextualResource, Resource::CompressionType::NONE}}});

        dummy1 = manager.Read("dummy1", "", true);
        ASSERT_EQ(dummy1.getName(), "");
        ASSERT_EQ(dummy1.getCompression(), Resource::CompressionType::NONE);
        ASSERT_TRUE(dummy1.getContentPath().empty());
        ASSERT_EQ_DATA(dummy1.getData(), ResourceManagerTest::longTextualResourceContent);

        dummy2 = manager.Read("dummy2", "");
        ASSERT_EQ(dummy2.getName(), "");
        ASSERT_EQ(dummy2.getCompression(), Resource::CompressionType::NONE);
        ASSERT_TRUE(dummy2.getContentPath().empty());
        ASSERT_EQ_DATA(dummy2.getData(), ResourceManagerTest::longTextualResourceContent);
    }

#ifndef WIN_PLATFORM

    TEST_F(ResourceManagerTest, BigBinary) {
        ResourceManager manager(ResourceManagerTest::bigBinaryPath);
        manager.Write({{"foo", Resource{ResourceManagerTest::longTextualResource, Resource::CompressionType::ZIP}}});

        auto foo = manager.Read("foo", "");
        ASSERT_EQ(foo.getName(), "");
        ASSERT_EQ(foo.getCompression(), Resource::CompressionType::ZIP);
        ASSERT_TRUE(foo.getContentPath().empty());
        ASSERT_EQ_DATA(foo.getData(), ResourceManagerTest::longTextualResourceContent);

        ResourceMap resourcesToWrite;
        resourcesToWrite.reserve(ResourceManagerTest::numberOfResources);
        for (size_t i = 0; i < ResourceManagerTest::numberOfResources; ++i) {
            resourcesToWrite.emplace(std::make_pair("textual-" + std::to_string(i),
                                                    Resource{ResourceManagerTest::longTextualResource,
                                                             Resource::CompressionType::ZIP}));
            resourcesToWrite.emplace(std::make_pair("binary-" + std::to_string(i),
                                                    Resource{ResourceManagerTest::binaryResource}));
        }
        manager.Write(std::move(resourcesToWrite));

        manager.Write({{"dummy1", Resource{ResourceManagerTest::longTextualResource}}});

        auto data = manager.Read("textual-0", "", true);
        ASSERT_EQ(data.getName(), "");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::ZIP);
        ASSERT_TRUE(data.getContentPath().empty());
        ASSERT_EQ_DATA(data.getData(), ResourceManagerTest::longTextualResourceContent);

        data = manager.Read("textual-254", "");
        ASSERT_EQ(data.getName(), "");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::ZIP);
        ASSERT_TRUE(data.getContentPath().empty());
        ASSERT_EQ_DATA(data.getData(), ResourceManagerTest::longTextualResourceContent);

        data = manager.Read("binary-255", "");
        ASSERT_EQ(data.getName(), "");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);
        ASSERT_TRUE(data.getContentPath().empty());
        ASSERT_EQ_DATA(data.getData(), ResourceManagerTest::binaryResourceContent);

        data = manager.Read("dummy1", "");
        ASSERT_EQ(data.getName(), "");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);
        ASSERT_TRUE(data.getContentPath().empty());
        ASSERT_EQ_DATA(data.getData(), ResourceManagerTest::longTextualResourceContent);

        ResourceMap resourcesToUpdate = {
                {"binary-255", Resource{ResourceManagerTest::longTextualResource, Resource::CompressionType::ZIP}},
                {"boo",        Resource{ResourceManagerTest::longTextualResource, Resource::CompressionType::ZIP}}};
        manager.Write(std::move(resourcesToUpdate));

        data = manager.Read("binary-255", "", true);
        ASSERT_EQ(data.getName(), "");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::ZIP);
        ASSERT_TRUE(data.getContentPath().empty());
        ASSERT_EQ_DATA(data.getData(), ResourceManagerTest::longTextualResourceContent);

        data = manager.Read("boo", "");
        ASSERT_EQ(data.getName(), "");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::ZIP);
        ASSERT_TRUE(data.getContentPath().empty());
        ASSERT_EQ_DATA(data.getData(), ResourceManagerTest::longTextualResourceContent);
    }

    TEST_F(ResourceManagerTest, Performance) {
        const int shortTextCount = 30000;
        const int longTextCount = 5000;
        const int binaryCount = 1000;

        ResourceManager manager(ResourceManagerTest::bigBinaryPath);

        ResourceMap resourcesToWrite;
        StopWatch s;

        for (int i = 0; i < shortTextCount; i++) {
            resourcesToWrite.emplace(std::make_pair("PERFDATA/short-textual-" + std::to_string(i),
                                                    Resource{ResourceManagerTest::shortTextualResource,
                                                             Resource::CompressionType::ZIP}));
        }
        for (int i = 0; i < longTextCount; i++) {
            resourcesToWrite.emplace(std::make_pair("PERFDATA/long-textual-" + std::to_string(i),
                                                    Resource{ResourceManagerTest::longTextualResource,
                                                             Resource::CompressionType::ZIP}));
        }
        for (int i = 0; i < binaryCount; i++) {
            resourcesToWrite.emplace(std::make_pair("PERFDATA/binary-" + std::to_string(i),
                                                    Resource{ResourceManagerTest::binaryResource}));
        }

        s.Start();
        manager.Write(std::move(resourcesToWrite));
        s.Stop();
        auto writeElapsed = s.getElapsed();

        s.Start();
        std::ifstream appStream{ResourceManagerTest::bigBinaryPath, std::ios::in | std::ios::binary};
        std::vector<char> content = {(std::istreambuf_iterator<char>(appStream)), std::istreambuf_iterator<char>()};
        s.Stop();
        auto appReadElapsed = s.getElapsed();

        s.Start();
        manager.ReaderInit();
        s.Stop();
        auto readInitElapsed = s.getElapsed();
        s.Start();
        for (int i = 0; i < shortTextCount; i++) {
            auto data = manager.Read("PERFDATA", "short-textual-" + std::to_string(i));

            ASSERT_EQ(data.getName(), "");
            ASSERT_EQ(data.getCompression(), Resource::CompressionType::ZIP);
            ASSERT_TRUE(data.getContentPath().empty());
            ASSERT_EQ_DATA(data.getData(), ResourceManagerTest::shortTextualResourceContent);
        }
        s.Stop();
        auto readShortElapsed = s.getElapsed();
        s.Start();
        for (int i = 0; i < longTextCount; i++) {
            auto data = manager.Read("PERFDATA", "long-textual-" + std::to_string(i));

            ASSERT_EQ(data.getName(), "");
            ASSERT_EQ(data.getCompression(), Resource::CompressionType::ZIP);
            ASSERT_TRUE(data.getContentPath().empty());
            ASSERT_EQ_DATA(data.getData(), ResourceManagerTest::longTextualResourceContent);
        }
        s.Stop();
        auto readLongElapsed = s.getElapsed();
        s.Start();
        for (int i = 0; i < binaryCount; i++) {
            auto data = manager.Read("PERFDATA", "binary-" + std::to_string(i));

            ASSERT_EQ(data.getName(), "");
            ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);
            ASSERT_TRUE(data.getContentPath().empty());
            ASSERT_EQ_DATA(data.getData(), ResourceManagerTest::binaryResourceContent);
        }
        s.Stop();
        long readBinaryElapsed = s.getElapsed();

        auto readElapsed = readInitElapsed + readShortElapsed + readLongElapsed + readBinaryElapsed;
//        std::cout <<
//                  "app:        " << appReadElapsed << " us" << std::endl <<
//                  "write:      " << writeElapsed << " us" << std::endl <<
//                  "read-init   " << readInitElapsed << " us" << std::endl <<
//                  "read-short: " << readShortElapsed << " us" << std::endl <<
//                  "read-long:  " << readLongElapsed << " us" << std::endl <<
//                  "read-bin:   " << readBinaryElapsed << " us" << std::endl <<
//                  "read-sum:   " << readElapsed << " us" << std::endl;

        ASSERT_LE(readElapsed, appReadElapsed * 3);
        ASSERT_LE(writeElapsed, appReadElapsed * 2);
    }

#endif

    TEST_F(ResourceManagerTest, ResourcesCache) {
        ResourceManager manager(ResourceManagerTest::bigBinaryPath);

        manager.Write({
                              {"Foo/Bar",  Resource{ResourceManagerTest::longTextualResource}},
                              {"Foo/Bar1", Resource{ResourceManagerTest::longTextualResource}},
                              {"Foo/Bar2", Resource{ResourceManagerTest::longTextualResource}}
                      });
        auto data = manager.Read("Foo", "Bar");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);
        data = manager.Read("Foo", "Bar1");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);
        data = manager.Read("Foo", "Bar2");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);

        manager.Write({{"Foo/Bar", Resource{ResourceManagerTest::binaryResource, Resource::CompressionType::ZIP}}});

        data = manager.Read("Foo", "Bar");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);
        data = manager.Read("Foo", "Bar1");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);
        data = manager.Read("Foo", "Bar2");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);

        data = manager.Read("Foo", "Bar", true);
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::ZIP);
        data = manager.Read("Foo", "Bar1");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);
        data = manager.Read("Foo", "Bar2");
        ASSERT_EQ(data.getCompression(), Resource::CompressionType::NONE);
    }

    TEST_F(ResourceManagerTest, HasEmbeddedResourcesTests) {
        const auto resourceWriter = std::make_unique<Writer::ResourceWriter>(ResourceManagerTest::bigBinaryPath);
        resourceWriter->Write({{"CONFIG/PACKAGE_MAP", Resource{ResourceManagerTest::longTextualResource}}});

        Reader::ResourceReader resourceReader{ResourceManagerTest::bigBinaryPath};
        ASSERT_TRUE(resourceReader.HasEmbeddedResources());
    }

    TEST_F(ResourceManagerTest, DISABLED_TryRealAssembly) {
        Reader::ReaderSettings settings{"/workspace/oidis/io-oidis-localhost/build/releases/app-mac-nodejs/target/WuiLocalhost"};
        Reader::ResourceReader resourceReader{std::move(settings)};
        resourceReader.Initialize();
        auto all = Reader::ResourceReader::getCachedResources();
        auto resource = resourceReader.Read("CONFIG", "PACKAGE_MAP");
        auto data = resource.getData();
        char *buf = (&data[0]);
        if (buf != nullptr) {
//            string str(buf,data.size());
//            std::regex regex(R"(\"\/snapshot.*\")");
//            auto words_begin = std::sregex_iterator(str.begin(), str.end(), regex);
//            for (std::sregex_iterator i = words_begin; i != std::sregex_iterator(); ++i) {
//                std::smatch match = *i;
//                std::string match_str = match.str();
//                    std::cout << match_str << std::endl;
//            }
            std::cout << buf << std::endl;
        } else {
            ASSERT_TRUE(false) << "FAILED";
        }
    }
}
