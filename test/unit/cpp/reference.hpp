/* ********************************************************************************************************* *
 *
 * Copyright 2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_ONION_RESOURCEWRITER_TEST_REFERENCE_HPP_  // NOLINT
#define IO_OIDIS_ONION_RESOURCEWRITER_TEST_REFERENCE_HPP_

#include <gtest/gtest.h>

#include "../../../source/cpp/reference.hpp"
#include "Io/Oidis/Onion/StopWatch.hpp"

#endif  // IO_OIDIS_ONION_RESOURCEWRITER_TEST_REFERENCE_HPP_  NOLINT
