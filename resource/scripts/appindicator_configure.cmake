# * ********************************************************************************************************* *
# *
# * Copyright 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(APPINDICATOR_CONFIGURE app_target test_target test_lib_target path version attributes)
    message(STATUS "Running APPINDICATOR_CONFIGURE with (\n"
            "\tapp_target: ${app_target},\n"
            "\ttest_target: ${test_target},\n"
            "\tattributes: ${attributes},\n"
            "\tversion: ${version}\n).")

    PARSE_ARGS("${attributes}")

    set(_BASE_PATH ${path})
    if (NOT IS_ABSOLUTE ${_BASE_PATH})
        set(_BASE_PATH ${CMAKE_SOURCE_DIR}/${_BASE_PATH})
    endif ()

    # run "apt-get install pkg-config libappindicator-dev libcanberra-gtk-module" before
    find_package(PkgConfig REQUIRED)
    pkg_check_modules(appindicator REQUIRED appindicator-0.1)
    pkg_check_modules(GTK2 REQUIRED gtk+-2.0>=2.0.0)
    pkg_check_modules(X11 REQUIRED x11)

    target_include_directories(${app_target} PUBLIC ${appindicator_INCLUDE_DIRS})
    target_link_libraries(${app_target} PUBLIC ${GTK2_LIBRARIES} ${X11_LIBRARIES} ${appindicator_LIBRARIES})

endmacro()
